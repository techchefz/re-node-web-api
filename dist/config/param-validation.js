'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _body;

var _joi = require('joi');

var _joi2 = _interopRequireDefault(_joi);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

exports.default = {
  // ================================================= USER =============================================
  //POST /node/api/users/verifyOtp
  verifyOtp: {
    body: {
      phoneNo: _joi2.default.string().required(),
      otp: _joi2.default.number().required()
    }
  },

  //POST /node/api/users/changePassword
  changePassword: {
    body: {
      OldPassword: _joi2.default.string().required(),
      newPassword: _joi2.default.string().required()
    }
  },

  //POST /node/api/users/forgot
  forgotPassword: {
    body: {
      phoneNo: _joi2.default.string().required()
    }
  },

  //POST /node/api/users/reset-password-phone
  forgotReset: {
    body: {
      phoneNo: _joi2.default.string().required(),
      password: _joi2.default.string().required()
    }
  },

  //POST /node/api/users/userInterests
  userInterest: {
    body: {
      userId: _joi2.default.string().required(),
      interests: _joi2.default.array().required()
    }
  },

  //POST /node/api/users/updateprofileimage
  updateprofileimage: {
    body: {
      userId: _joi2.default.string().required(),
      image: _joi2.default.string().required()
    }
  },

  //POST /node/api/users/updatecoverimage
  updatecoverimage: {
    body: {
      userId: _joi2.default.string().required(),
      image: _joi2.default.string().required()
    }
  },

  //POST /node/api/users/resetPassword
  resetPassword: {
    body: {
      password: _joi2.default.string().required()
    }
  },

  //POST /node/api/users/deletecoverimage
  deletecoverimage: {
    body: {
      userId: _joi2.default.string().required()
    }
  },

  //POST /node/api/users/deleteprofileimage
  deleteprofileimage: {
    body: {
      userId: _joi2.default.string().required()
    }
  },

  //GET /node/api/users/suggestedTripStories
  suggestedTripStories: {
    query: {
      jsonString: _joi2.default.string().required()
    }
  },

  //GET /node/api/users/reviewForUser
  reviewForUser: {
    query: {
      jsonString: _joi2.default.string().required()
    }
  },

  //GET /node/api/users/
  getUserDetails: {
    query: {
      jsonString: _joi2.default.string().required()
    }
  },

  //POST /node/api/users/updateProfile
  updateProfile: {
    body: {
      userId: _joi2.default.string().required()
    }
  },

  // POST /node/api/users/register
  createUser: {
    body: {
      email: _joi2.default.string().required(),
      password: _joi2.default.string().required(),
      phoneNo: _joi2.default.string().required(),
      fname: _joi2.default.string().required(),
      lname: _joi2.default.string().required(),
      gender: _joi2.default.string().required(),
      city: _joi2.default.string().required(),
      dob: _joi2.default.string().required(),
      moto: _joi2.default.string().required()

    }
  },
  // ================================================= USER =============================================

  //POST /node/api/booktestride/create
  createTestRide: {
    body: (_body = {
      fName: _joi2.default.string().required,
      lName: _joi2.default.string().required,
      email: _joi2.default.string().required,
      bikeName: _joi2.default.string().required,
      countryName: _joi2.default.string().required,
      stateName: _joi2.default.string().required,
      cityName: _joi2.default.string().required,
      dealerName: _joi2.default.string().required,
      Date: _joi2.default.string().required,
      mobile: _joi2.default.number().required
    }, _defineProperty(_body, 'Date', _joi2.default.string().required), _defineProperty(_body, 'dealerCode', _joi2.default.string().required), _defineProperty(_body, 'buyPlanDate', _joi2.default.string().required), _defineProperty(_body, 'time', _joi2.default.string().required), _body)
  },
  // POST /api/auth/login
  login: {
    body: {
      email: _joi2.default.string().required(),
      password: _joi2.default.string().required(),
      userType: _joi2.default.string().required()
    }
  },

  // POST /api/bikes/register
  createBike: {
    body: {
      bikeModel: _joi2.default.string().required(),
      bikeName: _joi2.default.string().required(),
      bikeCategory: _joi2.default.string().required()
    }
  },

  // POST /api/stories/create
  createTripStory: {
    body: {}
  },

  // POST /api/stories/all
  getStories: {
    body: {
      limit: _joi2.default.string().required()
    }
  },

  // POST /api/stories/
  getStory: {
    body: {}
  },

  // POST /api/rides/create
  createRide: {
    body: {
      rideName: _joi2.default.string().required(),
      startLatitude: _joi2.default.number().required(),
      startLongitude: _joi2.default.number().required(),
      destination: _joi2.default.string().required(),
      destinationlatitude: _joi2.default.number().required(),
      destinationlongitude: _joi2.default.number().required(),
      //  durationInDays: Joi.number().required(),
      startDate: _joi2.default.string().required(),
      endDate: _joi2.default.string().required(),
      terrain: _joi2.default.string().required(),
      userId: _joi2.default.string().required(),
      startTimeHours: _joi2.default.string().required(),
      startTimeMins: _joi2.default.string().required(),
      startTimeZone: _joi2.default.string().required(),
      totalDistance: _joi2.default.number().required(),
      fname: _joi2.default.string().required(),
      lname: _joi2.default.string().required(),
      gender: _joi2.default.string().required(),
      email: _joi2.default.string().required(),
      // isRoyalEnfieldOwner: Joi.string().required(),
      // dob: Joi.string().required(),
      // city: Joi.string().required(),
      // bikeOwned: Joi.string().required(),
      rideCategory: _joi2.default.string().required()
    }
  },

  // POST /api/rides/getridesaroundme
  getridesaroundme: {
    body: {
      latitude: _joi2.default.number().required(),
      longitude: _joi2.default.number().required()
    }
  },

  // POST /api/rides/all
  getRides: {
    body: {
      limit: _joi2.default.string().required()
    }
  },

  // POST /api/rides/
  getRide: {
    body: {
      rideId: _joi2.default.string().required()
    }
  },

  sendOtp: {
    body: {
      phoneNo: _joi2.default.string().required()
    }
  },

  getBikeDetail: {
    body: {
      phoneNo: _joi2.default.string().required()
    }
  },

  findState: {
    body: {
      state: _joi2.default.string().required()
    }
  },

  findCity: {
    body: {
      city: _joi2.default.string().required()
    }
  }
};
module.exports = exports['default'];
//# sourceMappingURL=param-validation.js.map
