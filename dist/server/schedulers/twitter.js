'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _tweetit = require('../controllers/tweetit');

var _twitterfeeds = require('../models/twitterfeeds');

var _twitterfeeds2 = _interopRequireDefault(_twitterfeeds);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Twit = require('twitter');
var util = require('util');

var mongoose = require('mongoose');


function twitterscheduler() {

	setInterval(twitterservice, 1000 * 30);
	function twitterservice() {
		_twitterfeeds2.default.distinct('HashTag').then(function (doc) {
			_twitterfeeds2.default.remove().then(function (docs) {
				for (var i = 0; i < doc.length; i++) {
					(0, _tweetit.tweetit)(doc[i], 10);
				}
			});
		});
	}
	// 	Twitterfeed.find().then((doc) => {

	// 		var hashTag = doc[0].HashTag;
	// 		if (doc.length != 0) {
	// 			Twitterfeed.remove().then((docs) => {					
	// 			});
	// 		}

	// 		tweetit(doc[0].HashTag,doc.length);

	// 	});
	// }
}
exports.default = { twitterscheduler: twitterscheduler };
module.exports = exports['default'];
//# sourceMappingURL=twitter.js.map
