'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var dealersingleinterfaceSchema = new _mongoose2.default.Schema({
    BranchCode: { type: String, default: Number },
    StoreManagerDesignation: { type: String, default: Number },
    Country: { type: String, default: Number },
    StoreManagerPhoneNo: { type: String, default: Number },
    StoreManagerEmailID: { type: String, default: Number },
    StoreEmailID: { type: String, default: Number },
    WeeklyOff: { type: String, default: Number },
    BusinessHours: { type: String, default: Number },
    Latitude: { type: String, default: Number },
    Longitude: { type: String, default: Number },
    StoreManagerName: { type: String, default: Number },
    DealerMobileNo: { type: String, default: Number },
    AlternateStoreNo: { type: String, default: Number },
    MainPhoneNo: { type: String, default: Number },
    GooglePlaceID: { type: String, default: Number },
    Status: { type: String, default: Number }
});

exports.default = _mongoose2.default.model('singleintefacedealer', dealersingleinterfaceSchema);
module.exports = exports['default'];
//# sourceMappingURL=dealerSingleInteface.js.map
