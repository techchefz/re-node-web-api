'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Bike Schema
 */

var Schema = _mongoose2.default.Schema;
var BikeSchema = new _mongoose2.default.Schema({
    bikeName: { type: String, default: null },
    familyCode: { type: Number, default: null },
    model: [],
    familyName: { type: String, default: null },
    status: { type: String, default: null },
    ReviewId: [{ type: Schema.Types.ObjectId, ref: 'Reviews', default: null }]
});

exports.default = _mongoose2.default.model('Bike', BikeSchema);
module.exports = exports['default'];
//# sourceMappingURL=bike.js.map
