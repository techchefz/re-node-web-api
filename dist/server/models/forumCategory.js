'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Forums Schema
 */

var Schema = _mongoose2.default.Schema;

var ForumCategorySchema = new _mongoose2.default.Schema({
    categoryName: { type: String, Default: null, unique: true },
    categorySubHeading: { type: String, Default: null },
    totalTopicsCount: { type: Number, Default: 0 },
    totalViewsCount: { type: Number, Default: 0 },
    totalRepliesCount: { type: Number, Default: 0 },
    subscribedUsers: [{
        userId: { type: Schema.Types.ObjectId, ref: 'User', default: null },
        email: { type: String, Default: null }
    }]
});

exports.default = _mongoose2.default.model('ForumCategory', ForumCategorySchema);
module.exports = exports['default'];
//# sourceMappingURL=forumCategory.js.map
