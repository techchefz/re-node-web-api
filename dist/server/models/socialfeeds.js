'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var socialfeedsschema = new _mongoose2.default.Schema({

	HashTag: { type: String },

	ListOfTags: [],
	UserName: {
		type: String

	},
	UserId: {
		type: String

	}, Metatag: [],
	Text: {
		type: String
	},

	SocialFeedImage: {
		type: String
	},

	Likes: {
		type: Number

	},

	ReTweets: {
		type: Number

	},
	SocialLinks: {
		type: String
	},
	SocialSource: {
		type: String
	},
	isApproved: {
		type: Boolean,
		default: true
	},
	isBrandPost: {
		type: Boolean,
		default: true
	},
	moderationReference: { type: String }
});

exports.default = _mongoose2.default.model('socialfeeds', socialfeedsschema);
module.exports = exports['default'];
//# sourceMappingURL=socialfeeds.js.map
