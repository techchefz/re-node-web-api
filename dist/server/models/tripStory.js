'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _httpStatus = require('http-status');

var _httpStatus2 = _interopRequireDefault(_httpStatus);

var _APIError = require('../helpers/APIError');

var _APIError2 = _interopRequireDefault(_APIError);

var _comments = require('./comments');

var _comments2 = _interopRequireDefault(_comments);

var _replies = require('./replies');

var _replies2 = _interopRequireDefault(_replies);

var _user = require('./user');

var _user2 = _interopRequireDefault(_user);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Trip-Story Schema
 */
var moment = require('moment');
var Schema = _mongoose2.default.Schema;
var TripStorySchema = new _mongoose2.default.Schema({
    storyTitle: { type: String, default: null, required: true },
    storySummary: { type: String, default: null },
    storyBody: { type: String, default: null, required: true },
    postedOn: { type: String, default: moment().format("D MMM YYYY") },
    postedBy: { type: Schema.Types.ObjectId, ref: 'User', default: null },
    coverImage: { type: String, default: null },
    locale: {
        country: { type: String, required: true },
        language: { type: String, required: true }
    },
    storyUrl: { type: String, default: null },
    comment: [{ type: _mongoose2.default.Schema.Types.ObjectId, ref: 'comments', default: null }],
    videoThumbnail: [{ type: String, default: null }],
    tripStoryImages: [{
        srcPath: { type: String, default: "https://content3.jdmagicbox.com/comp/ernakulam/i4/0484px484.x484.170825114532.p1i4/catalogue/tripstory-kakkanad-west-ernakulam-tour-operators-42bsn0q.jpg" }
    }],
    categoryName: { type: String, default: null },
    storyTags: [{ type: String, default: null }]
});

/**
 * Statics
 */

TripStorySchema.statics = {
    getStories: function getStories(skip, limit) {
        return this.find().skip(skip).limit(limit).execAsync().then(function (stories) {
            if (stories) {
                return stories;
            }
            var err = new _APIError2.default('Error Retrieving stories!', _httpStatus2.default.NOT_FOUND);
            return Promise.reject(err);
        });
    },
    getStory: function getStory(id) {
        return this.find({ _id: id }).execAsync().then(function (story) {
            if (story) {
                return story;
            }
            var err = new _APIError2.default('No Such story Exists !', _httpStatus2.default.NOT_FOUND);
            return Promise.reject(err);
        });
    }
};

exports.default = _mongoose2.default.model('TripStory', TripStorySchema);
module.exports = exports['default'];
//# sourceMappingURL=tripStory.js.map
