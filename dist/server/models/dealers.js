'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _moment = require('moment');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var dealerDetailsSchema = new _mongoose2.default.Schema({
    BranchCode: { type: String, default: Number },
    AddressLine1: { type: String, default: String },
    AddressLine2: { type: String, default: String },
    AddressLine3: { type: String, default: String },
    Pincode: { type: Number, default: String },
    City: { type: String, default: null, trim: true },
    State: { type: String, default: null, trim: true },
    Country: { type: String, defaul: null, trim: true },
    MainPhoneNo: { type: String, default: null, trim: true },
    StoreEmailId: { type: String, default: null, trim: true },
    WeeklyOff: { type: String, default: null, trim: true },
    StoreManagerName: { type: String, default: null, trim: true },
    BusinessHours: { type: String, default: null, trim: true },
    Longitude: { type: Number, default: null, trim: true },
    Latitude: { type: Number, default: null, trim: true },
    GooglePlaceID: { type: String, default: null, trim: true },
    DealerID: { type: String, default: null, trim: true },
    BranchName: { type: String, default: null, trim: true },
    CityID: { type: String, default: null, trim: true },
    StateID: { type: String, default: null, trim: true },
    BranchTypeIdenitifier: { type: String, default: null, trim: true },
    DealerPrincipalName: { type: String, default: null, trim: true },
    Status: { type: String, default: null, trim: true },
    Dealer_Source: { type: String, default: null, trim: true },
    Cover_image: { type: String, default: null, trim: true },
    Cover_image_mobile: { type: String, default: null, trim: true },
    Thumbnail_Image: { type: String, default: null, trim: true },
    PrimaryImage: { type: String, default: null, trim: true },
    PrimaryImage_mobile: { type: String, default: null, trim: true },
    DealerImage: { type: String, default: null, trim: true },
    DealerName: { type: String, default: null, trim: true },
    DealerOneLiner: { type: String, default: null, trim: true },
    DealerDescription: { type: String, default: null, trim: true },
    Gallery: { type: String, default: null, trim: true },
    rideOutId: [{ type: _mongoose2.default.Schema.Types.ObjectId, ref: 'ride', defaul: null }],
    DealerUrl: { type: String, default: null, trim: true }
});

// var dealerDetailsSchema = new mongoose.Schema({
//     locale: {
//         country: { type: String, defaul: 'In', trim: true },
//         language: { type: String, defaul: 'en', trim: true }
//     },
//     Status: { type: String, default: null, trim: true },
//     GooglePlaceID: { type: String, default: null, trim: true },
//     MainPhoneNo: { type: String, default: null, trim: true },
//     AlternateStoreNo: { type: String, default: null, trim: true },
//     DealerMobileNo: { type: String, default: null, trim: true },
//     StoreManagerName: { type: String, default: null, trim: true },
//     Longitude: { type: String, default: null, trim: true },
//     Latitude: { type: String, default: null, trim: true },
//     BusinessHours: { type: String, default: null, trim: true },
//     WeeklyOff: { type: String, default: null, trim: true },
//     StoreEmailID: { type: String, default: null, trim: true },
//     StoreManagerEmailID: { type: String, default: null, trim: true },
//     StoreManagerPhoneNo: { type: String, default: null, trim: true },
//     Country: { type: String, default: null, trim: true },
//     StoreManagerDesignation: { type: String, default: null, trim: true },
//     BranchCode: { type: String, default: null, trim: true },
//     Dealer_Source: { type: String, default: null, trim: true },
//     StatusCode: { type: String, default: null, trim: true },
//     DealerPrincipalName: { type: String, default: null, trim: true },
//     GearRMContactCode: { type: String, default: null, trim: true },
//     GearASMContactCode: { type: String, default: null, trim: true },
//     TPMContactCode: { type: String, default: null, trim: true },
//     RSMContactCode: { type: String, default: null, trim: true },
//     ZMContactCode: { type: String, default: null, trim: true },
//     RMContactCode: { type: String, default: null, trim: true },
//     TSMContactCode: { type: String, default: null, trim: true },
//     ASMContactCode: { type: String, default: null, trim: true },
//     CityType: { type: String, default: null, trim: true },
//     EndTime: { type: String, default: null, trim: true },
//     StartTime: { type: String, default: null, trim: true },
//     IsActive: { type: String, default: null, trim: true },
//     IsSalesApplicable: { type: String, default: null, trim: true },
//     IsServiceApplicable: { type: String, default: null, trim: true },
//     BranchTypeIdentifier: { type: String, default: null, trim: true },
//     Actual_Pincode: { type: String, default: null, trim: true },
//     StateID: { type: String, default: null, trim: true },
//     CityID: { type: String, default: null, trim: true },
//     Actual_State: { type: String, default: null, trim: true },
//     Actual_City: { type: String, default: null, trim: true },
//     Actual_Address3: { type: String, default: null, trim: true },
//     Actual_Address2: { type: String, default: null, trim: true },
//     Actual_Address1: { type: String, default: null, trim: true },
//     BranchName: { type: String, default: null, trim: true },
//     BranchID: { type: String, default: null, trim: true },
//     DealerName: { type: String, default: null, trim: true },
//     DealerID: { type: String, default: null, trim: true }
// });

exports.default = _mongoose2.default.model('dealer', dealerDetailsSchema);
module.exports = exports['default'];
//# sourceMappingURL=dealers.js.map
