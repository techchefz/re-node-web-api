'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var twitterFeedSchema = new _mongoose2.default.Schema({
	HashTag: {

		type: String,
		required: true,
		minlength: 1
	},

	ListOfTags: [{
		type: String
	}],

	UserName: {
		type: String

	},
	SocialFeedImage: {
		type: String

	},
	Text: {
		type: String
	},
	Likes: {
		type: Number

	},

	Retweets: {
		type: Number

	},

	SocialLink: {
		type: String
	},
	isApproved: {
		type: Boolean,
		default: true
	},
	moderationReference: { type: String }
});

exports.default = _mongoose2.default.model('Twitterfeed', twitterFeedSchema);
module.exports = exports['default'];
//# sourceMappingURL=twitterfeeds.js.map
