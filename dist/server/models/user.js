'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _bluebird = require('bluebird');

var _bluebird2 = _interopRequireDefault(_bluebird);

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _httpStatus = require('http-status');

var _httpStatus2 = _interopRequireDefault(_httpStatus);

var _APIError = require('../helpers/APIError');

var _APIError2 = _interopRequireDefault(_APIError);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//import bcrypt from 'bcrypt';
var crypto = require("crypto");
var md5 = require('js-md5');

var securityKey = 'manju22feet';

md5 = function md5(text) {
  return crypto.createHash('md5').update(text).digest();
};

/**
 * User Schema
 */
_mongoose2.default.Promise = global.Promise;
var Schema = _mongoose2.default.Schema;
var UserSchema = new _mongoose2.default.Schema({
  locale: {
    country: { type: String, required: true },
    language: { type: String, required: true }
  },
  previousUserId: { type: Number, default: null },
  fname: { type: String, default: null },
  lname: { type: String, default: null },
  gender: { type: String, default: null },
  email: { type: String, required: true, unique: true },
  phoneNo: { type: String, default: null, unique: true },
  password: { type: String, required: true, select: true },
  bikename: { type: String, default: null },
  city: { type: String, default: null },
  aboutMe: { type: String, default: null },
  profilePicture: {
    type: String,
    default: '/node/assets/User/ProfileImage/profile_image_dummy.jpg' //Default profile image to be added.
  },
  coverImage: {
    type: String,
    default: '/node/assets/User/coverImage/cover-image.jpg' //Default profile image to be added.
  },
  dob: { type: String, default: '12/8/1993' },
  addressInfo: {
    address: { type: String, default: null },
    city: { type: String, default: null },
    state: { type: String, default: null },
    country: { type: String, default: null },
    pinCode: { type: String, default: null }
  },
  ownBike: { type: String, default: null },
  reviewCreated: [{ type: Schema.Types.ObjectId, ref: 'Reviews' }],
  mobileVerified: { type: Boolean, default: false },
  emailVerified: { type: Boolean, default: false },
  otp: { type: Number, default: null },
  emailToken: { type: Number, default: null },
  isRoyalEnfieldOwner: { type: String, default: false },
  bikeOwned: [{ type: Schema.Types.ObjectId, ref: 'Bike', default: null }],
  ownedBikeInfo: [{
    bikeId: { type: Schema.Types.ObjectId, ref: 'Bike', default: null },
    dmsFamilyId: { type: String, default: null },
    type: { type: String, default: null },

    regNo: { type: String, default: null },
    RC_ownerName: { type: String, default: null },
    vehicleNo: { type: String, default: null },
    regDate: { type: Date, default: '1/1/2018' }
  }],

  ridesCreated: [{ type: Schema.Types.ObjectId, ref: 'Ride', default: null }],

  tripStoriesCreated: [{ type: Schema.Types.ObjectId, ref: 'TripStory', default: null }],

  ridesJoined: [{ type: Schema.Types.ObjectId, ref: 'Ride', default: null }],
  discussionJoined: [{ type: Schema.Types.ObjectId, ref: 'Forum', default: null }],
  favouriteQuote: {
    type: String, default: null
  },
  favouriteRideExperience: {
    type: String, default: null
  },
  socialNetworkUrls: {
    facebook: { type: String, default: null },
    googlePlus: { type: String, default: null },
    instagram: { type: String, default: null },
    twitter: { type: String, default: null }
  },
  userType: { type: String, default: 'user' },
  loginStatus: { type: Boolean, default: false },
  jwtAccessToken: { type: String, default: null },
  listofTags: [{ userTags: { type: String, default: null } }],
  userInterest: { type: String, default: null },
  userUrl: { type: String, default: null }
});

/**
 * converts the string value of the password to some hashed value
 * - pre-save hooks
 * - validations
 * - virtuals
 */
// eslint-disable-next-line
UserSchema.pre("save", function userSchemaPre(next) {
  var user = this;

  if (this.isModified('password') || this.isNew) {

    // eslint-disable-next-line

    var encrypt = function encrypt(text, secretKey) {
      secretKey = md5(secretKey);
      secretKey = Buffer.concat([secretKey, secretKey.slice(0, 8)]);
      var cipher = crypto.createCipheriv('des-ede3', secretKey, '');
      var encrypted = cipher.update(text, 'utf8', 'base64');
      return encrypted + cipher.final('base64');
    };
    var encrypted = encrypt(user.password, securityKey);
    user.password = encrypted;
    next();
  } else {
    return next();
  }
});

/**
 * comapare the stored hashed value of the password with the given value of the password
 * @param pw - password whose value has to be compare
 * @param cb - callback function
 */
UserSchema.methods.comparePassword = function comparePassword(pw, cb) {
  var user = this;
  // eslint-disable-next-line
  // pw is the incoming password 
  // user.password is the old password
  var isMatch;
  var encrypt = function encrypt(text, secretKey) {
    secretKey = md5(secretKey);
    secretKey = Buffer.concat([secretKey, secretKey.slice(0, 8)]);
    var cipher = crypto.createCipheriv('des-ede3', secretKey, '');
    var encrypted = cipher.update(text, 'utf8', 'base64');
    return encrypted + cipher.final('base64');
  };
  var encrypted = encrypt(pw, securityKey);

  if (encrypted === user.password) {
    isMatch = true;
  } else {
    isMatch = false;
  }

  cb(null, isMatch);
};

/**
 * Statics
 */
UserSchema.statics = {
  /**
     * Get user
     * @param {ObjectId} id - The objectId of user.
     * @returns {Promise<User, APIError>}
     */
  get: function get(id) {
    return this.findById(id).execAsync().then(function (user) {
      if (user) {
        return user;
      }
      var err = new _APIError2.default('No such user exists!', _httpStatus2.default.NOT_FOUND);
      return _bluebird2.default.reject(err);
    });
  },

  /**
     * List users in descending order of 'createdAt' timestamp.
     * @param {number} skip - Number of users to be skipped.
     * @param {number} limit - Limit number of users to be returned.
     * @returns {Promise<User[]>}
     */
  list: function list() {
    var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
        _ref$skip = _ref.skip,
        skip = _ref$skip === undefined ? 0 : _ref$skip,
        _ref$limit = _ref.limit,
        limit = _ref$limit === undefined ? 20 : _ref$limit;

    return this.find({ $or: [{ userType: 'user' }, { userType: 'admin' }] }).sort({ _id: -1 }).select('-__v').skip(skip).limit(limit).execAsync();
  }
};
/**
 * @typedef User
 */
exports.default = _mongoose2.default.model('User', UserSchema);
module.exports = exports['default'];
//# sourceMappingURL=user.js.map
