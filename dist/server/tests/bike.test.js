'use strict';

var _supertestAsPromised = require('supertest-as-promised');

var _supertestAsPromised2 = _interopRequireDefault(_supertestAsPromised);

var _httpStatus = require('http-status');

var _httpStatus2 = _interopRequireDefault(_httpStatus);

var _chai = require('chai');

var _chai2 = _interopRequireDefault(_chai);

var _index = require('../../index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_chai2.default.config.includeStack = true; /* eslint-disable */


describe('## Bike APIs', function () {
    var bike = {
        bikeModel: 'Himalyan',
        bikeName: 'Himalyan',
        bikeCategory: 'Himalyan'
    };
    var bike2 = {
        bikeModel: 'Himalyan',
        bikeName: 'Himalyan',
        bikeCategory: 'Himalyan'
    };

    describe('# POST /api/bikes/register', function (done) {
        it('should create a new bike', function (done) {
            (0, _supertestAsPromised2.default)(_index2.default).post('/api/bikes/register').send(bike).expect(_httpStatus2.default.OK).then(function (res) {
                (0, _chai.expect)(res.body.success).to.equal(true);
                (0, _chai.expect)(res.body.data).to.have.all.keys('bike');
                done();
            });
        });
    });

    describe('# POST /api/users/register', function () {
        it('should not create new bike with same bike name and category', function (done) {
            (0, _supertestAsPromised2.default)(_index2.default).post('/api/bikes/register').send(bike2).expect(_httpStatus2.default.OK).then(function (res) {
                (0, _chai.expect)(res.body.success).to.equal(false);
                (0, _chai.expect)(res.body.message).to.equal('bike already exist');
                done();
            });
        });
    });
});
//# sourceMappingURL=bike.test.js.map
