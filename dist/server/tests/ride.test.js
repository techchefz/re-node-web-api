'use strict';

var _supertestAsPromised = require('supertest-as-promised');

var _supertestAsPromised2 = _interopRequireDefault(_supertestAsPromised);

var _httpStatus = require('http-status');

var _httpStatus2 = _interopRequireDefault(_httpStatus);

var _chai = require('chai');

var _chai2 = _interopRequireDefault(_chai);

var _index = require('../../index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_chai2.default.config.includeStack = true; /* eslint-disable */


describe('## Ride APIs', function () {
    var ride = {
        rideTitle: "Trip to Ladakh !",
        rideDescription: "Steep ride down the hills",
        rideStartDate: "1/1/2018",
        rideEndDate: "1/1/2018",
        totalDistance: "99",
        rideStartTime: "11:00am",
        rideOrigin: "New Delhi",
        rideDestination: "Ladakh",
        createrName: "Deepak Chourasiya",
        createrEmail: "deepu@chourasiya.com",
        createrPhoneNo: "1722149211",
        rideURL: "https://www.illeh.com/",
        createrId: "5a585bc036d5800618f3b67c",
        createdOn: "2014-08-31T18:30:00Z",
        rideType: "member-ride"
    };

    describe('# POST /api/rides/create', function (done) {
        it('should create a new ride', function (done) {
            (0, _supertestAsPromised2.default)(_index2.default).post('/api/rides/create').send(ride).expect(_httpStatus2.default.OK).then(function (res) {
                (0, _chai.expect)(res.body.success).to.equal(true);
                (0, _chai.expect)(res.body.data).to.have.all.keys('ride');
                done();
            });
        });
    });
});
//# sourceMappingURL=ride.test.js.map
