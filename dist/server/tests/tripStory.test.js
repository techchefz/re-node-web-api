'use strict';

var _supertestAsPromised = require('supertest-as-promised');

var _supertestAsPromised2 = _interopRequireDefault(_supertestAsPromised);

var _httpStatus = require('http-status');

var _httpStatus2 = _interopRequireDefault(_httpStatus);

var _chai = require('chai');

var _chai2 = _interopRequireDefault(_chai);

var _index = require('../../index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_chai2.default.config.includeStack = true; /* eslint-disable */


describe('## Trip Story APIs', function () {
    var story = {
        storyTitle: "Second ride ever to goa for a weekend !",
        storyBody: "It was a regular friday, just like all other regular 9 to 5 corporate workers i was in a day shift in my office but for me the day was full with excitement and thrilling feeling because after the work i was ready to go to the paradise GOA to meet my friend i have never met before personally and to explore the places around.",
        postedOn: "2013-08-31T18:30:00Z",
        postedBy: "5a585bc036d5800618f3b67c",
        tripImages: ["http://stat.overdrive.in/wp-content/uploads/2016/12/2016-Royal-Enfield-Himalayan-first-ride-review.jpg"],
        storyUrl: "http://google.com",
        comments: [{
            commentId: "1234654654",
            commentedOn: "2013-08-31T18:30:00Z",
            commentedBy: "5a585bc036d5800618f3b67c",
            commentBody: "hi",
            replies: [{
                repliedOn: "2013-08-31T18:30:00Z",
                repliedBy: "5a585bc036d5800618f3b67c",
                replyBody: "hey"
            }]
        }]
    };

    var story1 = {
        storyTitle: "Second ride ever to goa for a weekend !",
        storyBody: "It was a regular friday, just like all other regular 9 to 5 corporate workers i was in a day shift in my office but for me the day was full with excitement and thrilling feeling because after the work i was ready to go to the paradise GOA to meet my friend i have never met before personally and to explore the places around.",
        postedOn: "2013-08-31T18:30:00Z",
        postedBy: "5a585bc036d5800618f3b67c"
    };

    var story2 = {
        storyTitle: "Third ride ever to goa for a weekend !",
        storyBody: "It was a regular friday, just like all other regular 9 to 5 corporate workers i was in a day shift in my office but for me the day was full with excitement and thrilling feeling because after the work i was ready to go to the paradise GOA to meet my friend i have never met before personally and to explore the places around.",
        postedOn: "2014-08-31T18:30:00Z",
        postedBy: "5a585bc036d5800618f3b67c"
    };

    describe('# POST /api/stories/create', function (done) {
        it('should create a new story', function (done) {
            (0, _supertestAsPromised2.default)(_index2.default).post('/api/stories/create').send(story).expect(_httpStatus2.default.OK).then(function (res) {
                (0, _chai.expect)(res.body.success).to.equal(true);
                (0, _chai.expect)(res.body.data).to.have.all.keys('story');
                done();
            });
        });
    });

    describe('# POST /api/stories/create', function (done) {
        it('should create a new story2', function (done) {
            (0, _supertestAsPromised2.default)(_index2.default).post('/api/stories/create').send(story1).expect(_httpStatus2.default.OK).then(function (res) {
                (0, _chai.expect)(res.body.success).to.equal(true);
                (0, _chai.expect)(res.body.data).to.have.all.keys('story');
                done();
            });
        });
    });

    describe('# POST /api/stories/create', function (done) {
        it('should create a new story3', function (done) {
            (0, _supertestAsPromised2.default)(_index2.default).post('/api/stories/create').send(story2).expect(_httpStatus2.default.OK).then(function (res) {
                (0, _chai.expect)(res.body.success).to.equal(true);
                (0, _chai.expect)(res.body.data).to.have.all.keys('story');
                done();
            });
        });
    });

    var storyId = {
        _id: "5a5c7bc7d87c0c2a4069b397"
    };
    describe('# POST /api/stories', function () {
        it('should get the story details', function (done) {
            (0, _supertestAsPromised2.default)(_index2.default).post('/api/stories').send(storyId).expect(_httpStatus2.default.OK).then(function (res) {
                (0, _chai.expect)(res.body.success).to.equal(true);
                (0, _chai.expect)(res.body.message).to.equal("story retrieved successfully");
                done();
            });
        });
    });

    var constraints = {
        limit: "1",
        skip: "2"
    };

    describe('# POST /api/stories/all', function (done) {
        it('should return list of stories', function (done) {
            (0, _supertestAsPromised2.default)(_index2.default).post('/api/stories/all').send(constraints).expect(_httpStatus2.default.OK).then(function (res) {
                (0, _chai.expect)(res.body.success).to.equal(true);
                (0, _chai.expect)(res.body.message).to.equal('stories retrieved successfully');
                done();
            });
        });
    });
});
//# sourceMappingURL=tripStory.test.js.map
