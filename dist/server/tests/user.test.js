'use strict';

var _supertestAsPromised = require('supertest-as-promised');

var _supertestAsPromised2 = _interopRequireDefault(_supertestAsPromised);

var _httpStatus = require('http-status');

var _httpStatus2 = _interopRequireDefault(_httpStatus);

var _chai = require('chai');

var _chai2 = _interopRequireDefault(_chai);

var _index = require('../../index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_chai2.default.config.includeStack = true; /* eslint-disable */


describe('## User(Public) APIs', function () {
  var user1 = {
    email: 'user1@techchefz.com',
    password: 'password',
    userType: 'user',
    fname: 'Kunal',
    lname: 'Bhardwaj',
    phoneNo: '9599914762'
  };

  var user2 = {
    email: 'user1@techchefz.com',
    password: 'password',
    userType: 'user',
    fname: 'Rahul',
    lname: 'Kumar',
    phoneNo: '9599914762'
  };

  var jwtAccessToken = null;
  describe('# POST /api/users/register', function () {
    it('should create a new user', function (done) {
      (0, _supertestAsPromised2.default)(_index2.default).post('/api/users/register').send(user1).expect(_httpStatus2.default.OK).then(function (res) {
        (0, _chai.expect)(res.body.success).to.equal(true);
        (0, _chai.expect)(res.body.data).to.have.all.keys('user', 'jwtAccessToken');
        user1 = res.body.data.user;
        jwtAccessToken = res.body.data.jwtAccessToken;
        done();
      });
    });
  });

  describe('# POST /api/users/register', function () {
    it('should not create new user with same email', function (done) {
      (0, _supertestAsPromised2.default)(_index2.default).post('/api/users/register').send(user2).expect(_httpStatus2.default.OK).then(function (res) {
        (0, _chai.expect)(res.body.success).to.equal(false);
        (0, _chai.expect)(res.body.message).to.equal('user already exist');
        done();
      });
    });
  });

  describe('# get /api/users', function () {
    it('should get the user details', function (done) {
      (0, _supertestAsPromised2.default)(_index2.default).get('/api/users').set('Authorization', jwtAccessToken).expect(_httpStatus2.default.OK).then(function (res) {
        (0, _chai.expect)(res.body.success).to.equal(true);
        (0, _chai.expect)(res.body.data.fname).to.equal(user1.fname);
        done();
      });
    });
  });

  describe('# Error handling POST /api/users/register', function () {
    it('should throw parameter validation error', function (done) {
      delete user2.phoneNo;
      (0, _supertestAsPromised2.default)(_index2.default).post('/api/users/register').send(user2).expect(_httpStatus2.default.BAD_REQUEST).then(function (res) {
        (0, _chai.expect)(res.body.success).to.equal(false);
        done();
      });
    });
  });

  describe('# Error handling get /api/users', function () {
    it('should get UNAUTHORIZED error as no token provided', function (done) {
      (0, _supertestAsPromised2.default)(_index2.default).get('/api/users').expect(_httpStatus2.default.UNAUTHORIZED).then(function (res) {
        (0, _chai.expect)(res.body.success).to.equal(false);
        done();
      });
    });
  });
});
//# sourceMappingURL=user.test.js.map
