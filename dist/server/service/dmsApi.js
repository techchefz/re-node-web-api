'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _queryString = require('query-string');

var _queryString2 = _interopRequireDefault(_queryString);

var _request = require('request');

var _request2 = _interopRequireDefault(_request);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function bookTestRide(testRideData) {
    var testRideDatavar = {
        'Name': testRideData.fName + testRideData.lName,
        'mobile': testRideData.mobile,
        'EMail': testRideData.email,
        'Country': 'india',
        'State': testRideData.stateName,
        'City': testRideData.cityName,
        'Motorcycle': "Classic 350",
        'Dealer_Name': testRideData.dealerName,
        'When_do_plan_to_buy': testRideData.buyPlanDate,
        'Web_link': 'https://royalenfield.com',
        'Tocken': 'ayz4EbIZLT403KVQ7dffhJ1Ffzv0yiSui1i1VXmAssesLjYODiy5dHIdfd2W11Q9QH6oHTuTWyIi7t5GsfGYk4qWisQdAP1X',
        'Dealer_Code': testRideData.dealerCode,
        'UTM': '22feet',
        'Test_Ride_Date': testRideData.Date,
        'Test_Ride_Time': testRideData.time
    };

    var axiosConfig = {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    };

    var testRideOptions = {
        url: 'http://crc.cequitycti.com/RoyalEnfield/service.asmx/Organic',
        method: 'POST',
        headers: axiosConfig.headers,
        body: _queryString2.default.stringify(testRideDatavar)
    };

    var hello = (0, _request2.default)(testRideOptions, function (error, response, body) {
        if (!error && response.statusCode == 200) {

            var result = response.statusCode;
        } else {
            reject(err);
        }
    });
}

exports.default = {
    bookTestRide: bookTestRide
};
module.exports = exports['default'];
//# sourceMappingURL=dmsApi.js.map
