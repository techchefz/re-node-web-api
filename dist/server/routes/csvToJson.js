'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _expressValidation = require('express-validation');

var _expressValidation2 = _interopRequireDefault(_expressValidation);

var _paramValidation = require('../../config/param-validation');

var _paramValidation2 = _interopRequireDefault(_paramValidation);

var _csvToJson = require('../controllers/csvToJson');

var _csvToJson2 = _interopRequireDefault(_csvToJson);

var _dealersCsvToJson = require('../controllers/dealersCsvToJson');

var _dealersCsvToJson2 = _interopRequireDefault(_dealersCsvToJson);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();

router.route('/connect').get(_csvToJson2.default.connectSftpforbikescsv);

router.route('/bikecsvToJson').get(_csvToJson2.default.bikeCsvToJson);

router.route('/dealerxlstojson').get(_csvToJson2.default.dealerDataToDB);

router.route('/getBranchMasterDealers').get(_dealersCsvToJson2.default.getBranchMasterFromSftp);
router.route('/getSingleInterfaceDealers').get(_dealersCsvToJson2.default.getSingleInterfaceFromSftp);

router.route('/getAllDealers').get(_dealersCsvToJson2.default.mergeDealerData);

router.route('/rideOutCsvToJson').get(_csvToJson2.default.rideOutCsvToJson);

router.route('/mapDealersToRodeOuts').get(_csvToJson2.default.mapDealersToRodeOuts);

exports.default = router;
module.exports = exports['default'];
//# sourceMappingURL=csvToJson.js.map
