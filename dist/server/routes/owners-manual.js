'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _expressValidation = require('express-validation');

var _expressValidation2 = _interopRequireDefault(_expressValidation);

var _paramValidation = require('../../config/param-validation');

var _paramValidation2 = _interopRequireDefault(_paramValidation);

var _ownersManual = require('../controllers/owners-manual');

var _ownersManual2 = _interopRequireDefault(_ownersManual);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();

router.route('/create').post(_ownersManual2.default.create);

exports.default = router;
module.exports = exports['default'];
//# sourceMappingURL=owners-manual.js.map
