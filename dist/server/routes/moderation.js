'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _expressValidation = require('express-validation');

var _expressValidation2 = _interopRequireDefault(_expressValidation);

var _httpStatus = require('http-status');

var _httpStatus2 = _interopRequireDefault(_httpStatus);

var _passport = require('passport');

var _passport2 = _interopRequireDefault(_passport);

var _APIError = require('../helpers/APIError');

var _APIError2 = _interopRequireDefault(_APIError);

var _env = require('../../config/env');

var _env2 = _interopRequireDefault(_env);

var _paramValidation = require('../../config/param-validation');

var _paramValidation2 = _interopRequireDefault(_paramValidation);

var _moderation = require('../controllers/moderation');

var _moderation2 = _interopRequireDefault(_moderation);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();

router.route('/twitterdata')
/** GET /api/moderation/twitterdata - Get twitterdata */
.get(_moderation2.default.getTwitterData);

router.route('/moderatetwitterdata')
/** GET /api/moderation/moderatetwitterdata - Moderate twitterdata */
.post(_moderation2.default.moderateTwitterData);

exports.default = router;
module.exports = exports['default'];
//# sourceMappingURL=moderation.js.map
