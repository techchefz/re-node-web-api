'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _expressValidation = require('express-validation');

var _expressValidation2 = _interopRequireDefault(_expressValidation);

var _paramValidation = require('../../config/param-validation');

var _paramValidation2 = _interopRequireDefault(_paramValidation);

var _forums = require('../controllers/forums');

var _forums2 = _interopRequireDefault(_forums);

var _forumCategory = require('../controllers/forumCategory');

var _forumCategory2 = _interopRequireDefault(_forumCategory);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();

router.route('/forum-detail').post(_forums2.default.getFourmDetail);

router.route('/create-forum-post').post(_forums2.default.create);

router.route('/increment-topic-view').post(_forums2.default.incrementTopicViews);

router.route('/all').post(_forums2.default.getForumPosts);

router.route('/create-category').post(_forumCategory2.default.create);

router.route('/category-details').post(_forumCategory2.default.getCategoryDetials);

router.route('/subscribe').post(_forumCategory2.default.subscribeToForumCategory);

exports.default = router;
module.exports = exports['default'];
//# sourceMappingURL=forums.js.map
