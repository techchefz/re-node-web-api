'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _expressValidation = require('express-validation');

var _expressValidation2 = _interopRequireDefault(_expressValidation);

var _paramValidation = require('../../config/param-validation');

var _paramValidation2 = _interopRequireDefault(_paramValidation);

var _ride = require('../controllers/ride');

var _ride2 = _interopRequireDefault(_ride);

var _multer = require('multer');

var _multer2 = _interopRequireDefault(_multer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();

var storage = _multer2.default.diskStorage({
  destination: function destination(req, file, cb) {
    cb(null, './node/assets/Rides');
  },
  filename: function filename(req, file, cb) {
    var imageName = Date.now() + file.originalname;
    cb(null, imageName);
    var imagePath = './node/assets/Rides' + imageName;
  }
});

var uploadObj = (0, _multer2.default)({ storage: storage });
/** POST /api/rides/create - create new Ride and return corresponding ride object */
router.route('/create').post(uploadObj.array('rideImages', 10), (0, _expressValidation2.default)(_paramValidation2.default.createRide), _ride2.default.create);

router.route('/all').post((0, _expressValidation2.default)(_paramValidation2.default.getRides), _ride2.default.getRides);

router.route('/getridesaroundme').post((0, _expressValidation2.default)(_paramValidation2.default.getridesaroundme), _ride2.default.getRidesAroundMe);

router.route('/')
/** POST /api/rides/ - Get ride */
.post((0, _expressValidation2.default)(_paramValidation2.default.getRide), _ride2.default.getRide)

/** PUT /api/rides/ - Update ride */
.put(_ride2.default.update)

/** DELETE /api/rides/ - Delete ride */
.delete(_ride2.default.remove).get(_ride2.default.getRide);

exports.default = router;
module.exports = exports['default'];
//# sourceMappingURL=ride.js.map
