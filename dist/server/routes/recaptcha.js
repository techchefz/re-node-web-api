'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _expressValidation = require('express-validation');

var _expressValidation2 = _interopRequireDefault(_expressValidation);

var _passport = require('passport');

var _passport2 = _interopRequireDefault(_passport);

var _APIError = require('../helpers/APIError');

var _APIError2 = _interopRequireDefault(_APIError);

var _env = require('../../config/env');

var _env2 = _interopRequireDefault(_env);

var _paramValidation = require('../../config/param-validation');

var _paramValidation2 = _interopRequireDefault(_paramValidation);

var _recaptcha = require('../controllers/recaptcha');

var _recaptcha2 = _interopRequireDefault(_recaptcha);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();

// POST /api/recaptcha/store
router.route('/store').post(_recaptcha2.default.recaptchastore).get(_recaptcha2.default.recaptchastore);

router.route('/verify').post(_recaptcha2.default.recaptchaverify).get(_recaptcha2.default.recaptchaverify);

exports.default = router;
module.exports = exports['default'];
//# sourceMappingURL=recaptcha.js.map
