'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _solr = require('../controllers/solr');

var _solr2 = _interopRequireDefault(_solr);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();

router.route('/').get(_solr2.default.searchSolr).post(_solr2.default.searchSolrCategory);

router.route('/searchride').post(_solr2.default.searchRide);

router.route('/testSms').post(_solr2.default.sendSms);

router.route('/tripStorySearchCount').post(_solr2.default.tripStorySearchCount);

router.route('/tripStorySearch').post(_solr2.default.tripStorySearch);

router.route('/searchTripOnClick').post(_solr2.default.tripStorySearchOnClick);

router.route('/forumTopicSearchOnClick').post(_solr2.default.forumTopicSearchOnClick);

router.route('/forumTopicSearch').post(_solr2.default.forumTopicSearch);

router.route('/newsSearch').post(_solr2.default.NewsSearch);

exports.default = router;
module.exports = exports['default'];
//# sourceMappingURL=search.js.map
