'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _dmsApi = require('../service/dmsApi');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();

/** POST /api/dealers/register - create new Dealer and return corresponding dealer object and token */
router.route('/book-test-ride').post(_dmsApi.bookTestRide);
exports.default = router;
module.exports = exports['default'];
//# sourceMappingURL=dms.js.map
