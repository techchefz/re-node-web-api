'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _uploadAsset = require('../controllers/uploadAsset');

var _uploadAsset2 = _interopRequireDefault(_uploadAsset);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _circularJsonEs = require('circular-json-es6');

var _circularJsonEs2 = _interopRequireDefault(_circularJsonEs);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();
router.route('/upload').post(_uploadAsset2.default.upload);

router.route('/fetch').post(_uploadAsset2.default.fetch);

exports.default = router;
module.exports = exports['default'];
//# sourceMappingURL=uploadAsset.js.map
