'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _syncSampleData = require('../controllers/syncSampleData');

var _syncSampleData2 = _interopRequireDefault(_syncSampleData);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();

// POST /api/social/twitter
router.route('/city-states').post(_syncSampleData2.default.syncCityStates);

router.route('/dealers').post(_syncSampleData2.default.syncDealers);

exports.default = router;
module.exports = exports['default'];
//# sourceMappingURL=syncSampleData.js.map
