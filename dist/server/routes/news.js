'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _news = require('../controllers/news');

var _news2 = _interopRequireDefault(_news);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();

router.route('/create').post(_news2.default.create);

router.route('/getNews')
/** POST /api/stories/ - Get story */
.post(_news2.default.getNews);

router.route('/').put(_news2.default.update);

exports.default = router;
module.exports = exports['default'];
//# sourceMappingURL=news.js.map
