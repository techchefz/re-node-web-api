'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _locobuzzfeeds = require('../controllers/locobuzzfeeds');

var _locobuzzfeeds2 = _interopRequireDefault(_locobuzzfeeds);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();

router.route('/').post(_locobuzzfeeds2.default.insert);

exports.default = router;
module.exports = exports['default'];
//# sourceMappingURL=locobuzzfeeds.js.map
