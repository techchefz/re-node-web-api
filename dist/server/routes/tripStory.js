'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _expressValidation = require('express-validation');

var _expressValidation2 = _interopRequireDefault(_expressValidation);

var _paramValidation = require('../../config/param-validation');

var _paramValidation2 = _interopRequireDefault(_paramValidation);

var _tripStory = require('../controllers/tripStory');

var _tripStory2 = _interopRequireDefault(_tripStory);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();

router.route('/getthestory').post(_tripStory2.default.getmystory);
/** POST /api/stories/create - create new Trip Story and return corresponding story object */
router.route('/create').post(_tripStory2.default.create);

router.route('/all').post((0, _expressValidation2.default)(_paramValidation2.default.getStories), _tripStory2.default.getStories);

router.route('/getstory')
/** POST /api/stories/ - Get story */
.post(_tripStory2.default.getStory).get(_tripStory2.default.getStory)

/** PUT /api/stories/ - Update Story */
.put(_tripStory2.default.update)

/** DELETE /api/stories/ - Delete Story */
.delete(_tripStory2.default.remove);

exports.default = router;
module.exports = exports['default'];
//# sourceMappingURL=tripStory.js.map
