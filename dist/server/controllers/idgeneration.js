'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _bike = require('../models/bike');

var _bike2 = _interopRequireDefault(_bike);

var _news = require('../models/news');

var _news2 = _interopRequireDefault(_news);

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function generateId(req, res, next) {

    var category = req.body.requestContentJSON;
    if (category == 'news') {
        var news = new _news2.default({});
        news.save().then(function (savedNews) {
            var obj = { pageId: savedNews._id };
            res.send({ pageId: savedNews._id.toString() });
        }).catch(function (error) {
            console.log(error);
        });
    } else {
        var arr = category.split('_');
        var bike = (0, _bike2.default)({
            bikeName: arr[1]
        });
        bike.save().then(function (savedBike) {
            res.send({ pageId: arr[0] + '_' + savedBike._id });
        });
    }
}
exports.default = { generateId: generateId };
module.exports = exports['default'];
//# sourceMappingURL=idgeneration.js.map
