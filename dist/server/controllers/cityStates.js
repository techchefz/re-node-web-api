'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _citiesStates = require('../models/citiesStates');

var _citiesStates2 = _interopRequireDefault(_citiesStates);

var _dealers = require('../models/dealers');

var _dealers2 = _interopRequireDefault(_dealers);

var _request = require('request');

var _request2 = _interopRequireDefault(_request);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var headers = {
    'Content-Type': 'text/plain'
};

var fetchCityStatesData = {
    "ResourceIdentifier": 206,
    "EntityType": "9021",
    "EntityID": 45,
    "CardType": 34,
    RequestKey: "rgfvrgkjwkdjsddcawdwhcb"
};

var fetchCityStatesString = JSON.stringify(fetchCityStatesData);

var fetchCityStatesOptions = {
    url: 'http://refineb2cdvp.excelloncloud.com/exapis//api/portalentity',
    method: 'POST',
    headers: headers,
    body: fetchCityStatesString
};

function fetchCityStates(req, res, next) {

    (0, _request2.default)(fetchCityStatesOptions, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            // Print out the response body
            res.send(body);
            var cityStatesData = JSON.parse(body);
            _citiesStates2.default.insertMany(cityStatesData, function (insertedDoc) {
                res.send(insertedDoc);
            });
        } else {
            console.log(error);
        }
    });
}

function getcity(req, res, next) {
    _citiesStates2.default.find({ StateName: req.body.stateName }).then(function (foundCity) {
        var citiesArr = [];
        foundCity.map(function (a) {
            citiesArr.push({ cityName: a.CityName, cityId: a.CityId });
        });
        res.send(citiesArr);
    }).catch(function (err) {
        res.send(err);
    });
}

function getState(req, res, next) {
    _citiesStates2.default.distinct('StateName').then(function (foundState) {
        res.send(foundState);
    });
}

function getDealers(req, res, next) {
    _dealers2.default.find({ City: req.body.city }).then(function (foundDealer) {
        var dealersArr = [];
        foundDealer.map(function (dealer) {
            var dealerObj = {
                "DealerName": dealer.DealerName,
                "BranchName": dealer.BranchName,
                "address": dealer.AddressLine1 + " " + dealer.AddressLine2 + " " + dealer.AddressLine3,
                "dealerID": dealer.DealerID
            };
            dealersArr.push(dealerObj);
        });
        res.send(dealersArr);
    }).catch(function (err) {
        return res.send(err);
    });
}
exports.default = {
    getState: getState,
    getcity: getcity,
    getDealers: getDealers,
    fetchCityStates: fetchCityStates
};
module.exports = exports['default'];
//# sourceMappingURL=cityStates.js.map
