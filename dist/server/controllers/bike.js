'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _bike2 = require('../models/bike');

var _bike3 = _interopRequireDefault(_bike2);

var _user = require('../models/user');

var _user2 = _interopRequireDefault(_user);

var _async = require('asyncawait/async');

var _async2 = _interopRequireDefault(_async);

var _await = require('asyncawait/await');

var _await2 = _interopRequireDefault(_await);

var _bikeCsv = require('../models/bikeCsv');

var _bikeCsv2 = _interopRequireDefault(_bikeCsv);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function get(req, res) {
    return res.send({ success: true, message: 'bike found', data: req.bike });
}

function create(req, res, next) {
    _bike3.default.findOneAsync({
        $and: [{ bikeName: req.body.bikeName }, { bikeCategory: req.body.bikeCategory }]
    }).then(function (foundBike) {
        if (foundBike !== null) {
            var returnObj = {
                success: true,
                message: '',
                data: {}
            };
            returnObj.message = 'bike already exist';
            returnObj.success = false;
            return res.send(returnObj);
        } else {
            var _bike = new _bike3.default({
                bikeModel: req.body.bikeModel,
                bikeName: req.body.bikeName,
                bikeCategory: req.body.bikeCategory
            });
            _bike.saveAsync().then(function (savedBike) {
                var returnObj = {
                    success: true,
                    message: '',
                    data: {}
                };
                returnObj.data.bike = savedBike;
                returnObj.message = 'bike created successfully';
                res.send(returnObj);
            }).error(function (e) {
                return next(e);
            });
        }
    });
}

function register(req, res, next) {
    res.send("inside bike");
    var bikeId = req.body.obj.motorcycleId.split('_')[1];
    var familyCode = req.body.obj.motorcycleFamilyCode;
    var modelCodes = req.body.obj.motorcycleModelCodes;
    var modelArr = [];
    var responseObj = {};
    var familyName;
    var status;
    for (var i = 0; i < modelCodes.length; i++) {
        _bikeCsv2.default.findOne({ bikeModelCode: modelCodes[i] }).then(function (foundCsvBike) {
            modelArr.push({
                bikeModelId: JSON.parse(foundCsvBike.bikeModelCode),
                bikeModelName: foundCsvBike.bikeName
            });
        }).catch(function (err) {
            console.log(err);
        });
    }
    _bikeCsv2.default.findOne({ familyCode: familyCode }).then(function (foundCsvBike) {
        familyName = foundCsvBike.familyName;
        status = foundCsvBike.bikeStatus;
    });

    _bike3.default.findOne({ _id: bikeId }).then(function (foundBike) {
        foundBike.familyCode = JSON.parse(familyCode);
        foundBike.status = status;
        foundBike.model = modelArr;
        foundBike.familyName = familyName;
        foundBike.save().then(function (savedBike) {
            responseObj.success = true;
            responseObj.message = "Bike created successfully";
            responseObj.data = savedBike;
        }).catch(function (err) {
            responseObj.success = false;
            responseObj.message = "error creating Bike";
            responseObj.data = err;
        });
    });
}

function update(req, res, next) {
    //code to update bike details goes here ..
    _bike3.default.findOne({ _id: req.body._id }).then(function (foundBike) {
        foundBike.bikeModel = req.body.bikeModel ? req.body.bikeModel : foundBike.bikeModel, foundBike.bikeName = req.body.bikeName ? req.body.bikeName : foundBike.bikeName, foundBike.bikeCategory = req.body.bikeCategory ? req.body.bikeCategory : foundBike.bikeCategory;
    });
    bike.saveAsync().then(function (updatedBike) {
        var returnObj = {
            success: true,
            message: '',
            data: {}
        };
        returnObj.data.bike = updatedBike;
        returnObj.message = 'bike updated successfully';
        res.send(returnObj);
    }).error(function (e) {
        return next(e);
    });
}

function remove(req, res, next) {
    // code to remove bike goes here..
    _bike3.default.remove({ bikeName: req.body.bikeName }).then(function (removedBike) {
        var returnObj = {
            success: true,
            message: '',
            data: {}
        };
        returnObj.data.bike = removedBike;
        returnObj.message = 'bike removed successfully';
        res.send(returnObj);
    }).error(function (e) {
        return next(e);
    });
}

function addMotorcycle(req, res, next) {
    var returnObj = {
        success: true,
        message: '',
        data: {}
    };

    var bikeId = req.body.bikeId.split("_")[1];
    var userId = req.body.userId;
    _user2.default.findOne({ _id: userId }).then(function (foundUser) {
        foundUser.ownedBikeInfo.push({ bikeId: bikeId, vehicleNo: req.body.vehicleNo });
        foundUser.save().then(function (savedUser) {
            returnObj.message = "bike added Successfully";
            res.status(200).send(returnObj);
        }).catch(function (err) {
            returnObj.success = false;
            returnObj.message = "error adding motorcycle";
            res.status(501).send(returnObj);
        });
    });
};

function getMotorcycleModelCodes(req, res) {
    var obj1 = {};
    _bikeCsv2.default.find().then(function (doc) {
        for (var i = 0; i < doc.length; i++) {
            obj1[doc[i].bikeModelCode] = doc[i].bikeName;
        }
        res.send(obj1);
    }).catch(function (err) {
        console.log(err);
    });
}

function getMotorcycleFamilyCodes(req, res) {
    var obj1 = {};

    _bikeCsv2.default.distinct("familyCode").then(function (distinct) {
        var foo = (0, _async2.default)(function () {
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = distinct[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var item = _step.value;

                    var arr = (0, _await2.default)(savingFunction(item));
                    (0, _await2.default)(matchingFunction(arr));
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }

            res.send(obj1);
        });
        foo();
    });

    var savingFunction = function savingFunction(distinct) {
        return new Promise(function (resolve, reject) {
            _bikeCsv2.default.aggregate([{ $match: { familyCode: distinct } }]).then(function (match) {
                resolve(match);
            }).catch(function (err) {
                console.log(err);
            });
        });
    };
    var matchingFunction = function matchingFunction(match) {
        return new Promise(function (resolve, reject) {
            var fun = (0, _async2.default)(function () {
                var _iteratorNormalCompletion2 = true;
                var _didIteratorError2 = false;
                var _iteratorError2 = undefined;

                try {
                    var _loop = function _loop() {
                        var item = _step2.value;

                        (0, _await2.default)(function () {
                            return new Promise(function (resolve, reject) {
                                obj1[item.familyCode] = item.familyName;
                                resolve(obj1);
                            });
                        });
                    };

                    for (var _iterator2 = match[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                        _loop();
                    }
                } catch (err) {
                    _didIteratorError2 = true;
                    _iteratorError2 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion2 && _iterator2.return) {
                            _iterator2.return();
                        }
                    } finally {
                        if (_didIteratorError2) {
                            throw _iteratorError2;
                        }
                    }
                }
            });
            fun();
            resolve(obj1);
        });
    };
}

exports.default = {
    get: get,
    create: create,
    update: update,
    remove: remove,
    addMotorcycle: addMotorcycle,
    getMotorcycleModelCodes: getMotorcycleModelCodes,
    getMotorcycleFamilyCodes: getMotorcycleFamilyCodes,
    register: register
};
module.exports = exports['default'];
//# sourceMappingURL=bike.js.map
