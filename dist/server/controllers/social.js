"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _twitter = require("twitter");

var _twitter2 = _interopRequireDefault(_twitter);

var _util = require("util");

var _util2 = _interopRequireDefault(_util);

var _request = require("request");

var _request2 = _interopRequireDefault(_request);

var _sortJsonArray = require("sort-json-array");

var _sortJsonArray2 = _interopRequireDefault(_sortJsonArray);

var _express = require("express");

var _express2 = _interopRequireDefault(_express);

var _axios = require("axios");

var _axios2 = _interopRequireDefault(_axios);

var _circularJsonEs = require("circular-json-es6");

var _circularJsonEs2 = _interopRequireDefault(_circularJsonEs);

var _socialfeeds = require("../models/socialfeeds");

var _socialfeeds2 = _interopRequireDefault(_socialfeeds);

var _env = require("../../config/env");

var _env2 = _interopRequireDefault(_env);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var asyncc = require('asyncawait/async');
var awaitt = require('asyncawait/await');


function twitterdata(req, res, next) {
  var arr = [];
  var hashtag = req.body.hashtag;
  _socialfeeds2.default.find().then(function (docs) {
    var foo = asyncc(function () {
      var _iteratorNormalCompletion = true;
      var _didIteratorError = false;
      var _iteratorError = undefined;

      try {
        for (var _iterator = docs[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
          var item = _step.value;

          if (item.ListOfTags != null) {
            for (var i = 0; i < item.ListOfTags.length; i++) {
              if (item.ListOfTags[i].toLowerCase() == hashtag.toLowerCase()) arr.push(awaitt(savingFunction(item)));
            }
          }
        }
      } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion && _iterator.return) {
            _iterator.return();
          }
        } finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }

      res.send(arr);
    });
    foo();
  }).catch(function (err) {
    res.status(500).send({ msg: "error" });
  });
}

var savingFunction = function savingFunction(item) {
  return new Promise(function (resolve, reject) {
    resolve(item);
  });
};

function dealerReviews(req, res, next) {
  var placeid = req.body.obj.googlePlaceId;
  var googlekey = _env2.default.googlereviewkey;
  var verifyUrl = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + placeid + "&key=" + googlekey;
  (0, _request2.default)(verifyUrl, function (err, response, body) {
    var stringbody = JSON.parse(body);
    var avReview = 0;
    for (var i = 0; i < stringbody.result.reviews.length; i++) {

      avReview = avReview + stringbody.result.reviews[i].rating;
    }
    var finalReview = avReview / stringbody.result.reviews.length;
    var reviewPercentage = finalReview / 5;
    var finalPercentage = reviewPercentage * 100;
    var location = stringbody.result.url;
    var obj = {
      ratingPercentage: finalPercentage,
      rating: finalReview,
      directionUrl: location
    };
    res.send(obj);
  });
}

function fetch(req, res, next) {
  var placeid = 'ChIJ7bGcKGThDDkRHLFd_LKUAC4';
  var googlekey = _env2.default.googlereviewkey;
  var placeId = req.body.obj.googlePlaceId;
  var verifyUrl = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + placeid + "&key=" + googlekey;

  (0, _request2.default)(verifyUrl, function (err, response, body) {

    body = JSON.parse(body);
    var Address = body.result.formatted_address;
    var phonenumber = body.result.formatted_phone_number;
    var name = body.result.name;
    var location = body.result.url;
    var ratings = body.result.rating;
    var website = body.result.website;
    var c = body.result.reviews;
    var d = (0, _sortJsonArray2.default)(c, 'rating', 'des');
    var reviewername = d[0].author_name;
    var authorurl = d[0].author_url;
    var profilephoto = d[0].profile_photo_url;
    var authorating = d[0].rating;
    var text = d[0].text;
    var timedescription = d[0].relative_time_description;
    var lat = body.result.geometry.location.lat;
    var lng = body.result.geometry.location.lng;

    var obj = {
      reviewText: text,
      reviewRating: authorating,
      reviewByUser: {
        firstName: reviewername,
        address: { addressAsText: Address },
        profilePicture: { srcPath: profilephoto }
      },
      reviewDateText: timedescription,
      reviewRatingPercentage: authorating * 20
    };

    res.send(obj);
  });
}

function googleplusdata(req, res, next) {
  var apkey = "AIzaSyA3LipyK7I_HaCEgW4jZl59dTlY0-gJvTA";
  var GUrl = "https://www.googleapis.com/plus/v1/activities?query=Royal+Enfield&maxResults=20&orderBy=recent&key=" + apkey;
  _axios2.default.get(GUrl).then(function (response) {
    var clone = _circularJsonEs2.default.parse(_circularJsonEs2.default.stringify(response));
    res.send(clone.data);
  }).catch(function (e) {
    console.log(e);
  });
}

function getWeather(req, res, next) {
  var lat = req.query.lat;
  var lng = req.query.lng;
  _axios2.default.get("https://api.openweathermap.org/data/2.5/weather?lat=" + lat + "&lon=" + lng + "&appid=cbf1b58e3415fa3b236b9e52d76e1bc4").then(function (data) {
    res.send(data.data);
  }).catch(function (err) {
    console.log(err);
  });
}

exports.default = {
  twitterdata: twitterdata,
  fetch: fetch,
  googleplusdata: googleplusdata,
  dealerReviews: dealerReviews,
  getWeather: getWeather
};
module.exports = exports["default"];
//# sourceMappingURL=social.js.map
