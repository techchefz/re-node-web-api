'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _socialfeeds = require('../models/socialfeeds');

var _socialfeeds2 = _interopRequireDefault(_socialfeeds);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var util = require('util');

function insert(req, res, next) {
	var flag = true;
	var arr = [];

	var _iteratorNormalCompletion = true;
	var _didIteratorError = false;
	var _iteratorError = undefined;

	try {
		for (var _iterator = req.body.socialFeeds[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
			var item = _step.value;

			if (util.isArray(item.Metatag)) {
				arr.push({
					HashTag: item.HashTag, ListOfTags: item.ListOfTags, UserName: item.UserName,
					UserId: item.UserId, Metatag: item.Metatag, Text: item.Text, SocialFeedImage: item.SocialFeedImage,
					Likes: item.Likes, ReTweets: item.ReTweets, SocialLinks: item.SocialLinks,
					SocialSource: item.SocialSource, isApproved: item.isApproved,
					isBrandPost: item.isBrandPost,
					moderationReference: item.HashTag + "_" + Math.floor(Math.random() * 9 + 1) + Math.floor(Math.random() * 9 + 1) + Math.floor(Math.random() * 9 + 1) + Math.floor(Math.random() * 9 + 1) + Math.floor(Math.random() * 9 + 1)
				});
			} else {
				flag = false;
			}
		}
	} catch (err) {
		_didIteratorError = true;
		_iteratorError = err;
	} finally {
		try {
			if (!_iteratorNormalCompletion && _iterator.return) {
				_iterator.return();
			}
		} finally {
			if (_didIteratorError) {
				throw _iteratorError;
			}
		}
	}

	_socialfeeds2.default.insertMany(arr).then(function (doc) {
		if (flag == true) {
			res.status(200).send('Data inserted Successfully');
		} else {
			res.status(500).send('fields inappropriate');
		}
	}).catch(function (err) {
		console.log(err);
	});
}

exports.default = { insert: insert };
module.exports = exports['default'];
//# sourceMappingURL=locobuzzfeeds.js.map
