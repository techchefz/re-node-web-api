"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ssh = require("ssh2");

var _ssh2 = _interopRequireDefault(_ssh);

var _env = require("../../config/env");

var _env2 = _interopRequireDefault(_env);

var _fs = require("fs");

var _fs2 = _interopRequireDefault(_fs);

var _csvtojson = require("csvtojson");

var _csvtojson2 = _interopRequireDefault(_csvtojson);

var _dealers = require("../models/dealers");

var _dealers2 = _interopRequireDefault(_dealers);

var _dealerSingleInteface = require("../models/dealerSingleInteface");

var _dealerSingleInteface2 = _interopRequireDefault(_dealerSingleInteface);

var _dealerBranchMaster = require("../models/dealerBranchMaster");

var _dealerBranchMaster2 = _interopRequireDefault(_dealerBranchMaster);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var connection = new _ssh2.default();
var dummyArray = [];
var headersarr = [];
var headersarr2 = [];

var dealersCsvArr1 = ["BranchCode", "StoreManagerDesignation", "Country", "StoreManagerPhoneNo", "StoreManagerEmailID", "StoreEmailID", "WeeklyOff", "BusinessHours", "Latitude", "Longitude", "StoreManagerName", "DealerMobileNo", "AlternateStoreNo", "MainPhoneNo", "GooglePlaceID", "Status"];

var dealersCsvArr2 = ["DealerID", "DealerName", "BranchID", "BranchCode", "BranchName", "Actual_Address1", "Actual_Address2", "Actual_Address3", "Actual_City", "Actual_State", "CityID", "StateID", "Actual_Pincode", "BranchTypeIdentifier", "IsServiceApplicable", "IsSalesApplicable", "IsActive", "StartTime", "EndTime", "CityType", "ASMContactCode", "TSMContactCode", "RMContactCode", "ZMContactCode", "RSMContactCode", "TPMContactCode", "GearASMContactCode", "GearRMContactCode", "DealerPrincipalName", "StoreManagerName", "StoreManagerDesignation", "StoreManagerPhoneNo", "StoreManagerEmailID", "WeeklyOff", "BusinessHours", "StatusCode", "Status", "Dealer_Source"];

var flag = false;
var fileName = void 0;

function getSingleInterfaceFromSftp(req, res) {
  res.send("Single Interface Done");
  connection.on("ready", function () {
    connection.sftp(function (err, sftp) {
      if (err) throw err;

      sftp.readdir("DMS-Web3/Testing/Dealer/single_interface", function (err, list) {
        if (err) throw err;
        if (list.length != 0) {
          fileName = list[0].filename;
          var sourceFile = sftp.createReadStream("DMS-Web3/Testing/Dealer/single_interface/" + fileName);

          sourceFile.on("data", function (fileData) {
            dummyArray.push(fileData.toString("utf8"));
          });

          sourceFile.on("end", function (ends) {
            var splData = dummyArray[0].split("\n");
            headersarr = splData[0].split("|");

            for (var i = 0; i < headersarr.length; i++) {
              for (var j = 0; j < dealersCsvArr1.length; j++) {
                if (headersarr[i] == dealersCsvArr1[j]) {
                  flag = true;
                } else {
                  flag = false;
                }
              }
            }

            console.log("Inside End");
            if (flag == true) {
              console.log("inside flag");

              var destinationFile = sftp.createWriteStream("DMS-Web3/Testing/archiveDealers/" + fileName);

              //creatig a temp file to fecth data

              _fs2.default.appendFile("csvFolder/" + fileName, dummyArray, function () {
                console.log("=============appendFile=======================");
                console.log("csvFolder/" + fileName);
                console.log("====================================");
              });

              //extract json from file and save it to temp collection

              (0, _csvtojson2.default)({ delimiter: "|" }).fromFile("csvFolder/" + fileName).then(function (jsonArray) {
                // console.log('================jsonArray====================');
                // console.log(jsonArray);
                // console.log('==============jsonArray======================');
                _dealerSingleInteface2.default.insertMany(jsonArray, function (insertedValue) {
                  var returnObj = {
                    success: true,
                    message: "Successfully inserted " + jsonArray.length
                  };
                  _fs2.default.unlink("csvFolder/" + fileName);
                  console.log(returnObj);
                  //sending file to archive folder sftp
                  sftp.appendFile("DMS-Web3/Testing/archiveDealers/" + fileName, dummyArray, function () {
                    console.log("====================================");
                    console.log("File Saved Successfully inside DMS-Web3/Testing/archiveDealers/" + fileName);
                    console.log("====================================");
                    sourceFile.pipe(destinationFile);

                    destinationFile.on("finish", function () {
                      //================================Code to delete the file==============================

                      sftp.unlink("DMS-Web3/Testing/Dealer/single_interface/" + fileName, function (err) {
                        if (err) {
                          connection.end();

                          console.log("Error, problem starting SFTP: %s", err);
                        } else {
                          connection.end();

                          console.log("file unlinked");
                        }
                      });

                      //================================Code to delete the file==============================
                    });
                  });
                });
              });
            } else {
              connection.end();
              console.log("====================================");
              console.log("File Headers Did`t Matched");
              console.log("====================================");
            }
          });
        } else {
          connection.end();

          console.log("====================================");
          console.log("No Single Inteface File found");
          console.log("====================================");
        }
      });
    });
  });

  connection.on("error", function (err) {
    console.log(err);
  });

  connection.connect({
    // debug: function (s) {
    //     console.log(s)
    // },
    host: "ftpL.royalenfield.com",
    port: 22,
    username: "interface",
    password: "Interface@re",
    privateKey: _fs2.default.readFileSync("config/id_rsa.ppk"),
    passphrase: "Interface@re",
    readyTimeout: 99999
  });
}

function getBranchMasterFromSftp(req, res) {
  res.send("Done");
  console.log("Branch Master Done");
  connection.on("ready", function () {
    connection.sftp(function (err, sftp) {
      if (err) throw err;
      sftp.readdir("DMS-Web3/Testing/Dealer/branch_master", function (err, list) {
        if (err) throw err;
        if (list.length != 0) {
          fileName = list[0].filename;
          var sourceFile = sftp.createReadStream("DMS-Web3/Testing/Dealer/branch_master/" + fileName);

          sourceFile.on("data", function (fileData) {
            dummyArray.push(fileData.toString("utf8"));
          });

          sourceFile.on("end", function (ends) {
            var splData = dummyArray[0].split("\r\n");
            var temp = splData[0].split("\n");
            headersarr2 = temp[0].split("|");
            for (var i = 0; i < headersarr2.length; i++) {
              for (var j = 0; j < dealersCsvArr2.length; j++) {
                if (headersarr2[i] == dealersCsvArr2[j]) {
                  flag = true;
                } else {
                  flag = false;
                }
              }
            }
            console.log("Inside End");
            if (flag == true) {
              console.log("inside flag");
              var destinationFile = sftp.createWriteStream("DMS-Web3/Testing/archiveDealers/" + fileName);

              //creatig a temp file to fecth data

              _fs2.default.appendFile("csvFolder/" + fileName, dummyArray, function () {
                console.log("=============appendFile=======================");
                console.log("csvFolder/" + fileName);
                console.log("====================================");
                //sending file to archive folder sftp
                sftp.appendFile("DMS-Web3/Testing/archiveDealers/" + fileName, dummyArray, function () {
                  //extract json from file and save it to temp collection

                  (0, _csvtojson2.default)({ delimiter: "|" }).fromFile("csvFolder/" + fileName).then(function (jsonArray) {
                    _dealerBranchMaster2.default.insertMany(jsonArray, function (insertedValue) {
                      var returnObj = {
                        success: true,
                        message: "Successfully inserted " + jsonArray.length
                      };
                      _fs2.default.unlink("csvFolder/" + fileName);
                      console.log(returnObj);
                      // res.send(returnObj);
                      sourceFile.pipe(destinationFile);

                      destinationFile.on("finish", function () {
                        //================================Code to delete the file==============================

                        sftp.unlink("DMS-Web3/Testing/Dealer/branch_master/" + fileName, function (err) {
                          if (err) {
                            connection.end();

                            console.log("Error, problem starting SFTP: %s", err);
                          } else {
                            connection.end();

                            console.log("file unlinked");
                          }
                        });

                        //================================Code to delete the file==============================
                      });
                    });
                  });
                });
              });
            } else {
              connection.end();

              res.send("File Headers Did`t Matched");
            }
          });
        } else {
          connection.end();
          console.log("====================================");
          console.log("No Branch Master File found");
          console.log("====================================");
        }
      });
    });
  });

  connection.on("error", function (err) {
    console.log(err);
  });

  connection.connect({
    // debug: function (s) {
    //     console.log(s)
    // },
    host: "ftpL.royalenfield.com",
    port: 22,
    username: "interface",
    password: "Interface@re",
    privateKey: _fs2.default.readFileSync("config/id_rsa.ppk"),
    passphrase: "Interface@re",
    readyTimeout: 99999
  });
}

function dealerDataToDB(tag, file_Name) {
  // console.log('================dealerDataToDB====================');
  // console.log(tag);
  // console.log(file_Name);
  // console.log('====================================');
  // if (tag == "single") {
  //     var json = CsvToJson.formatValueByType().fieldDelimiter('|').getJsonFromCsv(`csvFolder/VW_SingleInterface__03082018_154148.csv`);
  //     console.log('=============json=======================');
  //     console.log(json);
  //     console.log('==============json======================');
  // } else {
  //     var json = CsvToJson.formatValueByType().fieldDelimiter('|').getJsonFromCsv(`csvFolder/${file_Name}`);
  //     console.log('=========else====json=======================');
  //     console.log(json);
  //     console.log('==============json======================');
  // }
}

function mergeDealerData(req, res) {
  res.send("Data Inserted");
  var tempData1 = [];
  var tempData2 = [];
  var tempDealerObj = {};
  var finalArr = [];

  _dealerSingleInteface2.default.find().then(function (foundData1) {
    tempData1 = foundData1;
    if (tempData1.length == 0) {
      res.send("Nothing Found in Single Interface Model");
    } else {
      _dealerBranchMaster2.default.find().then(function (foundData) {
        tempData2 = foundData;
        if (tempData2.length == 0) {
          res.send("Nothing Found in Single Interface Model");
        } else {
          for (var i = 0; i < tempData1.length; i++) {
            for (var j = 0; j < tempData2.length; j++) {
              if (tempData1[i].BranchCode == tempData2[j].BranchCode) {
                var tempOneSingle = {
                  Status: tempData1[i].Status,
                  GooglePlaceID: tempData1[i].GooglePlaceID,
                  MainPhoneNo: tempData1[i].MainPhoneNo,
                  AlternateStoreNo: tempData1[i].AlternateStoreNo,
                  DealerMobileNo: tempData1[i].DealerMobileNo,
                  StoreManagerName: tempData1[i].StoreManagerName,
                  Longitude: parseFloat(tempData1[i].Longitude),
                  Latitude: parseFloat(tempData1[i].Latitude),
                  BusinessHours: tempData1[i].BusinessHours,
                  WeeklyOff: tempData1[i].WeeklyOff,
                  StoreEmailID: tempData1[i].StoreEmailID,
                  StoreManagerEmailID: tempData1[i].StoreManagerEmailID,
                  StoreManagerPhoneNo: tempData1[i].StoreManagerPhoneNo,
                  Country: tempData1[i].Country,
                  StoreManagerDesignation: tempData1[i].StoreManagerDesignation,
                  BranchCode: tempData1[i].BranchCode
                };

                var tempTwoMaster = {
                  Dealer_Source: tempData2[j].Dealer_Source,
                  StatusCode: tempData2[j].StatusCode,
                  StoreManagerName: tempData2[j].StoreManagerName,
                  DealerPrincipalName: tempData2[j].DealerPrincipalName,
                  GearRMContactCode: tempData2[j].GearRMContactCode,
                  GearASMContactCode: tempData2[j].GearASMContactCode,
                  TPMContactCode: tempData2[j].TPMContactCode,
                  RSMContactCode: tempData2[j].RSMContactCode,
                  ZMContactCode: tempData2[j].ZMContactCode,
                  RMContactCode: tempData2[j].RMContactCode,
                  TSMContactCode: tempData2[j].TSMContactCode,
                  ASMContactCode: tempData2[j].ASMContactCode,
                  CityType: tempData2[j].CityType,
                  EndTime: tempData2[j].EndTime,
                  StartTime: tempData2[j].StartTime,
                  IsActive: tempData2[j].IsActive,
                  IsSalesApplicable: tempData2[j].IsSalesApplicable,
                  IsServiceApplicable: tempData2[j].IsServiceApplicable,
                  BranchTypeIdentifier: tempData2[j].BranchTypeIdentifier,
                  Actual_Pincode: tempData2[j].Actual_Pincode,
                  StateID: tempData2[j].StateID,
                  CityID: tempData2[j].CityID,
                  Actual_State: tempData2[j].Actual_State,
                  Actual_City: tempData2[j].Actual_City,
                  Actual_Address3: tempData2[j].Actual_Address3,
                  Actual_Address2: tempData2[j].Actual_Address2,
                  Actual_Address1: tempData2[j].Actual_Address1,
                  BranchName: tempData2[j].BranchName,
                  BranchID: tempData2[j].BranchID,
                  DealerName: tempData2[j].DealerName,
                  DealerID: tempData2[j].DealerID
                };

                tempDealerObj = Object.assign(tempOneSingle, tempTwoMaster);

                // finalArr.push(tempDealerObj);

                if (tempDealerObj.Status == "New") {
                  inserteNewDealer(tempDealerObj);
                } else if (tempDealerObj.Status == "Update") {
                  updateDealer(tempDealerObj);
                } else if (tempDealerObj.Status == "Remove") {
                  removeDealer(tempDealerObj);
                }
              }
            }
          }
          //
          // Dealer.insertMany(finalArr, (insertedValue) => {
          //     let returnObj = {
          //         success: true,
          //         message: "All Dealers Added to DB",
          //         data: finalArr
          //     }
          //     console.log('====================================');
          //     console.log(finalArr.length + " data inserted");
          //     console.log('====================================');
          // });
        }
      });
    }
  });
}

function inserteNewDealer(newDealerData) {
  var newDealer = new _dealers2.default(newDealerData);
  newDealer.save().then(function (savedDealer) {
    // console.log('==============savedDealer======================');
    // console.log(savedDealer);
    // console.log('================savedDealer====================');
  });
}

function updateDealer(updatedData) {
  _dealers2.default.findOneAndUpdate({ DealerID: updatedData.DealerID }, { $set: { updatedData: updatedData } }, { new: true }).then(function (updatedDealer) {
    console.log("====================================");
    console.log(updatedDealer);
    console.log("====================================");
  });
}

function removeDealer(dealerToBeRemoved) {
  _dealers2.default.findOneAndRemove({ DealerID: dealerToBeRemoved.DealerID }).then(function (deletedData) {
    console.log("====================================");
    console.log(deletedData);
    console.log("====================================");
  });
}

exports.default = {
  getBranchMasterFromSftp: getBranchMasterFromSftp,
  getSingleInterfaceFromSftp: getSingleInterfaceFromSftp,
  mergeDealerData: mergeDealerData,
  dealerDataToDB: dealerDataToDB
};
module.exports = exports["default"];
//# sourceMappingURL=dealersCsvToJson.js.map
