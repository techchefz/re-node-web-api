"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _httpStatus = require("http-status");

var _httpStatus2 = _interopRequireDefault(_httpStatus);

var _formidable = require("formidable");

var _formidable2 = _interopRequireDefault(_formidable);

var _jsonwebtoken = require("jsonwebtoken");

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

var _APIError = require("../helpers/APIError");

var _APIError2 = _interopRequireDefault(_APIError);

var _env = require("../../config/env");

var _env2 = _interopRequireDefault(_env);

var _user2 = require("../models/user");

var _user3 = _interopRequireDefault(_user2);

var _emailApi = require("../service/emailApi");

var _emailApi2 = _interopRequireDefault(_emailApi);

var _smsApi = require("../service/smsApi");

var _smsApi2 = _interopRequireDefault(_smsApi);

var _axios = require("axios");

var _axios2 = _interopRequireDefault(_axios);

var _request = require("request");

var _request2 = _interopRequireDefault(_request);

var _moment = require("moment");

var _moment2 = _interopRequireDefault(_moment);

var _base64Img = require("base64-img");

var _base64Img2 = _interopRequireDefault(_base64Img);

var _nodemailer = require("nodemailer");

var _nodemailer2 = _interopRequireDefault(_nodemailer);

var _path = require("path");

var _path2 = _interopRequireDefault(_path);

var _tripStory = require("../models/tripStory");

var _tripStory2 = _interopRequireDefault(_tripStory);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; } //import bcrypt from 'bcrypt';


var geoip = require("geoip-lite");

function testingroute(req, res, next) {
  console.log(req.headers["x-custom-language"]);
  console.log(req.headers);
  res.send(req.headers);
  var ip;
  if (req.headers["x-forwarded-for"]) {
    console.log("1");
    ip = req.headers["x-forwarded-for"].split(",")[0];
  } else if (req.connection && req.connection.remoteAddress) {
    console.log("2");
    console.log(req.connection);
    ip = req.connection.remoteAddress;
  } else {
    console.log("3");
    ip = req.ip;
  }
  console.log("client IP is *********************" + ip);
  console.log(ip);
  var geo = geoip.lookup(ip);
  console.log(geo);
  console.log("========================================================");
}

/**
 * Create new user
 * @property {string} req.body.username - The username of user.
 * @property {string} req.body.mobileNumber - The mobileNumber of user.
 * @returns {User}
 */

function create(req, res, next) {
  // returnObj defination
  var returnObj = {
    success: true,
    message: "",
    data: {}
  };

  // find User if exists and update
  _user3.default.findOneAsync({
    $or: [{ $and: [{ email: req.body.email }, { phoneNo: req.body.phoneNo }] }, { $or: [{ email: req.body.email }, { phoneNo: req.body.phoneNo }] }]
  }).then(function (foundUser) {
    if (foundUser !== null && foundUser.userType === req.body.userType) {
      _user3.default.findOneAndUpdateAsync({ _id: foundUser._id }, { $set: { loginStatus: true } }, { new: true }).then(function (updateUserObj) {
        if (updateUserObj) {
          var jwtAccessToken = _jsonwebtoken2.default.sign(updateUserObj, _env2.default.jwtSecret);
          returnObj.message = "User Already Exists !";
          returnObj.success = false;
          return res.send(returnObj);
        }
      }).error(function (e) {
        var err = new _APIError2.default("error in updating user details while login " + e, _httpStatus2.default.INTERNAL_SERVER_ERROR);
        next(err);
      });
    } else {
      var otpValue = Math.floor(100000 + Math.random() * 900000); //eslint-disable-line
      var emailToken = Math.floor(700000000000 + Math.random() * 900000); //eslint-disable-line
      var bikeId = req.body.bikeOwned ? [req.body.bikeOwned[0].split("_")[1]] : [];
      var _user = new _user3.default({
        email: req.body.email,
        password: req.body.password,
        userType: req.body.userType,
        fname: req.body.fname,
        lname: req.body.lname,
        bikeOwned: bikeId,
        ownBike: req.body.moto,
        phoneNo: req.body.phoneNo,
        otp: otpValue,
        locale: {
          language: req.headers['x-custom-language'],
          country: req.headers['x-custom-country']
        },
        emailToken: emailToken,
        loginStatus: true,
        gender: req.body.gender,
        addressInfo: {
          city: req.body.city
        },
        dob: req.body.dob
      });
      _user.saveAsync().then(function (savedUser) {
        var username = "admin";
        var password = "admin";
        var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
        _request2.default.post({
          headers: {
            "content-type": "application/x-www-form-urlencoded",
            Authorization: auth
          },
          url: _env2.default.aemAuthorUrl + "/bin/createPage",
          form: {
            entity: "user",
            pageId: savedUser._id.toString(),
            title: savedUser.fname.toString(),
            country: savedUser.locale.country,
            language: savedUser.locale.language
          }
        }, function (error, response, dataReturned) {
          var data = JSON.parse(dataReturned);
          if (error) {
            console.log("==============error=================");
            console.log(error);
            console.log("==============error=================");
            returnObj.data.story = null;
            returnObj.message = "Error Creating User";
            returnObj.success = false;
            res.send(returnObj);
          } else {
            console.log("================response===================");
            console.log(response.body);
            console.log("================response===================");
            var _returnObj = {
              success: true,
              message: "",
              data: {}
            };
            _user3.default.findOne({ _id: savedUser._id }).then(function (updatedUser) {
              updatedUser.userUrl = data.pagePath;
              updatedUser.save();
              var jwtAccessToken = _jsonwebtoken2.default.sign(savedUser, _env2.default.jwtSecret);
              _returnObj.data.jwtAccessToken = "JWT " + jwtAccessToken;
              var obj = {
                email: savedUser.email,
                profilePicture: { srcPath: savedUser.profilePicture },
                userId: savedUser._id,
                phone: savedUser.phoneNo,
                firstName: savedUser.fname,
                lastName: savedUser.lname
              };
              _returnObj.data.user = obj;
              _returnObj.message = "user created successfully";
              res.send(_returnObj);
            }).catch(function (e) {
              return console.log(e);
            });
          }
        });
      }).error(function (e) {
        return next(e);
      });
    }
  }).catch(function (e) {
    return console.log(e);
  });
}

function sendSms1(smsText, phoneNo) {
  var num = phoneNo.slice(-10);
  _axios2.default.get("http://www.sms.wstechno.com/ComposeSMS.aspx?username=Isetsol-170841&password=Wsc@4141@&sender=WSTECH&&to=" + num + "&message=Please Use this OTP for resetting the password for your Royal Enfield Account.OTP is :" + smsText + "&priority=1&dnd=1&unicode=0").then(function (response) {
    return response;
  }).catch(function (error) {
    return error;
  });
}

function sendSms(smsText, phoneNo) {
  var num = phoneNo.slice(-10);
  (0, _request2.default)("http://103.247.98.91/API/SendMsg.aspx?uname=20171501&pass=Raytheorysms1&send=PRPSCN&dest=" + phoneNo + "&msg=Hi%20:User,%20Welcome%20to%20Forget%20OTP%20!%20Your%20OTP%20Verification%20Code%20is%20" + smsText).then(function (response) {}).catch(function (err) {
    console.log(err);
  });
}

function expireOtp(userid) {
  _user3.default.findOne({ _id: userid }).then(function (founduser) {
    user.otp = null;
    user.save();
  });
}

function forgot(req, res, next) {
  var returnObj = {
    success: true,
    message: "",
    data: {}
  };
  var phoneNo = req.body.phoneNo;
  if (phoneNo) {
    _user3.default.findOne({ phoneNo: phoneNo }).then(function (user) {
      if (user) {
        var otpValue = Math.floor(100000 + Math.random() * 900000);
        user.otp = otpValue;
        user.save();
        var sendsms = sendSms(otpValue, phoneNo);
        returnObj.success = true;
        returnObj.message = "otp has been sent";
        res.send(returnObj);
      } else {
        returnObj.success = false;
        returnObj.message = "user Not found";
        res.status(404).send(returnObj);
      }
    });
  } else {
    res.status(406).send("please enter the phone no.");
  }
  // }
}

function sample(req, res, next) {
  if (req.body.name && req.body.email && req.body.query) {
    res.send("form submitted successfully with these details" + req.body);
  } else {
    res.send("error submitting form");
  }
}

/**
 * Update existing user
 * @property {Object} req.body.user - user object containing all fields.
 * @returns {User}
 */
function update(req, res, next) {
  _user3.default.findOne({ _id: req.body.userId }).then(function (user) {
    if (user) {
      var oldPassword = req.body.password;
      user.comparePassword(oldPassword, function (err, isMatch) {
        if (err) throw err;
        if (isMatch === true) {
          user.fname = req.body.fname ? req.body.fname : user.fname;
          user.lname = req.body.lname ? req.body.lname : user.lname;
          user.email = req.body.email ? req.body.email : user.email;
          user.dob = req.body.dob ? req.body.dob : user.dob;
          user.gender = req.body.gender ? req.body.gender : user.gender;
          user.phoneNo = req.body.phoneNo ? req.body.phoneNo : user.phoneNo;
          user.bikename = req.body.bikename ? req.body.bikename : user.bikename;
          user.city = req.body.city ? req.body.city : user.city;
          user.aboutMe = req.body.aboutMe ? req.body.aboutMe : user.aboutMe;
          user.ownBike = req.body.moto ? req.body.moto : user.ownBike;
          user.saveAsync().then(function (savedUser) {
            var retObj = {};
            retObj.success = true;
            retObj.message = "Profile Successfully Updated";
            retObj.data = savedUser;
            res.send(retObj);
          }).catch(function (e) {
            return console.log(e);
          });
        } else {
          var retObj = {};
          retObj.message = "Password doesnt match";
          retObj.success = false;
          retObj.data = {};
          res.send(retObj);
        }
      });
    } else {
      var retObj = {};
      retObj.message = "User doesnt exist";
      retObj.success = false;
      retObj.data = {};
      res.send(retObj);
    }
  }).catch(function (e) {
    return console.log(e);
  });
}

/**
 * Delete user.
 * @returns {User}
 */
function remove(req, res, next) {
  var user = req.user;
  user.removeAsync().then(function (deletedUser) {
    var returnObj = {
      success: true,
      message: "user deleted successfully",
      data: deletedUser
    };
    res.send(returnObj);
  }).error(function (e) {
    return next(e);
  });
}

/**
 * Load user and append to req.
 */
function load(req, res, next, id) {
  _user3.default.get(id).then(function (user) {
    req.user = user; // eslint-disable-line no-param-reassign
    return next();
  }).error(function (e) {
    return next(e);
  });
}

function hashed(password) {
  return new Promise(function (resolve, reject) {
    bcrypt.genSalt(10, function (err, salt) {
      if (err) {
        reject(err);
      }
      bcrypt.hash(password, salt, function (hashErr, hash) {
        if (hashErr) {
          reject(hashErr);
        }
        resolve(hash);
      });
    });
  });
}

function forgotReset(req, res, next) {
  var phoneNo = req.body.phoneNo;
  _user3.default.findOne({ phoneNo: req.body.phoneNo }).then(function (user) {
    if (user) {
      var password = req.body.password;
      user.password = password;
      user.save().then(function (doc) {
        res.send("password Updated");
      });
    } else {
      res.send("User doesnt exist");
    }
  });
}

function reset(req, res, next) {
  var userId = req.body.userId;
  _user3.default.get(userId).then(function (user) {
    if (user) {
      var password = req.body.password;
      user.password = req.body.password;
      user.save().then(function (doc) {
        res.send("password Updated");
      }).catch(function (e) {
        return console.log(e);
      });
    } else {
      res.send("User doesnt exist");
    }
  });
}

function changepassword(req, res, next) {
  var userId = req.body.userId;
  // var jwtAccessToken = req.body.jwtAccessToken;
  _user3.default.get(userId).then(function (user) {
    if (user) {
      var oldPassword = req.body.OldPassword;
      var newPassword = req.body.newPassword;
      user.comparePassword(oldPassword, function (err, isMatch) {
        if (err) throw err;
        if (isMatch === true) {
          user.password = newPassword;
          user.save().then(function (doc) {
            res.send("password Updated");
          });
        } else {
          res.send("password didn't match");
        }
      });
    } else {
      res.send("User doesnt exist");
    }
  }).error(function (e) {
    return next(e);
  });
  next();
}

function timeStampToTime(date) {
  var result = (0, _moment2.default)(date).fromNow();
  return result;
}

function getUserDetails(req, res, next) {
  var id = req.query.jsonString;
  var tripImageLength = 0;
  var arr = [];
  _user3.default.findOne({ _id: id }).populate("ridesCreated tripStoriesCreated discussionJoined").then(function (doc) {
    var _obj;

    var usrRides = [];
    for (var i = 0; i < doc.ridesCreated.length; i++) {
      usrRides.push({
        rideName: doc.ridesCreated[i].rideName,
        rideTypeText: doc.ridesCreated[i].rideCategory,
        rideDetails: doc.ridesCreated[i].rideDetails,
        pageUrl: doc.ridesCreated[i].ridePageUrl,
        startPoint: { name: doc.ridesCreated[i].startPoint.name },
        endPoint: { name: doc.ridesCreated[i].endPoint.name },
        startDate: "2018-07-01",
        durationInDays: doc.ridesCreated[i].durationInDays
      });
    }
    var bikeArr = [];
    for (var i = 0; i < doc.ownedBikeInfo.length; i++) {
      bikeArr.push({
        bikeId: doc.ownedBikeInfo[i].bikeId,
        vehicleNo: doc.ownedBikeInfo[i].vehicleNo
      });
    }

    var tripStoriesSend = [];
    for (var i = 0; i < doc.tripStoriesCreated.length; i++) {
      tripStoriesSend.push({
        title: doc.tripStoriesCreated[i].storyTitle,
        summary: doc.tripStoriesCreated[i].storySummary,
        pageUrl: doc.tripStoriesCreated[i].storyUrl,
        thumbnailImagePath: doc.tripStoriesCreated[i].tripStoryImages.length != 0 ? doc.tripStoriesCreated[i].tripStoryImages[0].srcPath : ""
      });
      tripImageLength += doc.tripStoriesCreated[i].tripStoryImages.length;
    }

    for (var i = 0; i < doc.discussionJoined.length; i++) {
      var time = (0, _moment2.default)(doc.discussionJoined[i].postedOn).fromNow();
      doc.discussionJoined[i].timeAgo = time;
    }

    var obj = (_obj = {
      firstName: doc.fname ? doc.fname : "",
      address: { addressAsText: doc.addressInfo.city ? doc.addressInfo.city : "" },
      lastName: doc.lname ? doc.lname : "",
      coverImage: { srcPath: doc.coverImage ? doc.coverImage : "" },
      profilePicture: { srcPath: doc.profilePicture ? doc.profilePicture : "" },
      ownerBikeDetails: doc.bikeName ? doc.bikeName : "",
      aboutMe: doc.aboutMe ? doc.aboutMe : "",
      listofTags: doc.listofTags ? doc.listofTags : [],
      ridesCreated: doc.ridesCreated.length ? doc.ridesCreated.length : 0,
      ridesByUser: usrRides,
      tripStoriesCreated: doc.tripStoriesCreated ? doc.tripStoriesCreated.length : 0,
      imagesUploaded: tripImageLength ? tripImageLength : 0,
      gender: doc.gender ? doc.gender : ""
    }, _defineProperty(_obj, "address", { city: doc.addressInfo.city ? doc.addressInfo.city : "" }), _defineProperty(_obj, "dob", (0, _moment2.default)().format(doc.dob).toString()), _defineProperty(_obj, "email", doc.email), _defineProperty(_obj, "ownBike", doc.ownBike ? doc.ownBike : ""), _defineProperty(_obj, "discussionJoined", doc.discussionJoined.length != 0 ? doc.discussionJoined : []), _defineProperty(_obj, "phone", doc.phoneNo), _defineProperty(_obj, "tripStories", tripStoriesSend), _defineProperty(_obj, "motorcyclesOwned", bikeArr), _obj);

    res.send(obj);
  }).catch(function (e) {
    console.log(e);
    res.send(e);
  });
}

function userInterests(req, res, next) {
  var id = req.body.userId;
  _user3.default.findOneAndUpdate({ _id: id }, { $set: { listofTags: [] } }, { new: true }).then(function (docs) {
    console.log('====================================');
    console.log(doc);
    console.log('====================================');
    var i;
    for (i = 0; i < req.body.interests.length; i++) {
      docs.listofTags.push({ userTags: req.body.interests[i] });
    }
    docs.save().then(function (user) {
      res.send("Updated User Interests");
    });
  }).catch(function (e) {
    return console.log(e);
  });
}

function updateCoverImage(req, res, next) {
  _user3.default.findOne({ _id: req.body.userId }).then(function (doc) {
    var image = req.body.image;
    var path = "./node/assets/User/coverImage";
    _base64Img2.default.img(image, path, doc._id + doc.fname, function (err, filepath) {
      var returnObj = {
        success: true,
        message: "",
        data: {}
      };
      if (err) {
        returnObj.success = false;
        returnObj.message = "error updating image";
        returnObj.data = err;
        res.send(returnObj);
      } else {
        _user3.default.findByIdAndUpdate({ _id: req.body.userId }, { $set: { coverImage: "/" + filepath } }).then(function () {
          returnObj.message = "updated Successfully";
          res.send(returnObj);
        }, function (err) {
          returnObj.success = false;
          returnObj.message = "error updating image";
          returnObj.data = err;
          res.send(returnObj);
        });
      }
    });
  }).catch(function (e) {
    return console.log(e);
  });
}

function updateProfileImage(req, res, next) {
  _user3.default.findOne({ _id: req.body.userId }).then(function (doc) {
    var image = req.body.image;
    var path = "./node/assets/User/ProfileImage";
    _base64Img2.default.img(image, path, doc._id + doc.fname, function (err, filepath) {
      var returnObj = {
        success: true,
        message: "",
        data: {}
      };
      if (err) {
        returnObj.success = false;
        returnObj.message = "error updating image";
        returnObj.data = err;
        res.send(returnObj);
      } else {
        _user3.default.findByIdAndUpdate({ _id: req.body.userId }, { $set: { profilePicture: "/" + filepath } }).then(function () {
          returnObj.message = "updated Successfully";
          res.send(returnObj);
        }, function (err) {
          returnObj.success = false;
          returnObj.message = "error updating image";
          returnObj.data = err;
          res.send(returnObj);
        });
      }
    });
  });
}

function deleteProfileImage(req, res, next) {
  var dummyProfileImage = "/node/assets/User/ProfileImage/profile_image_dummy.jpg";
  var returnObj = {
    success: true,
    message: "",
    data: {}
  };

  _user3.default.findByIdAndUpdate({ _id: req.body.userId }, { $set: { profilePicture: dummyProfileImage } }).then(function () {
    returnObj.message = "deleted Successfully";
    returnObj.data = { dummyImage: dummyProfileImage };
    res.send(returnObj);
  }, function (err) {
    returnObj.success = false;
    returnObj.message = "deletion failed";
    res.status(204).send(returnObj);
  });
}

function deleteCoverImage(req, res, next) {
  var dummyCoverImage = "/node/assets/User/coverImage/cover-image.jpg";
  var returnObj = {
    success: true,
    message: "",
    data: {}
  };

  _user3.default.findByIdAndUpdate({ _id: req.body.userId }, { $set: { coverImage: dummyCoverImage } }).then(function () {
    returnObj.message = "deleted Successfully";
    returnObj.data = { dummyImage: dummyCoverImage };
    console.log("all fine");
    res.send(returnObj);
  }, function (err) {
    returnObj.success = false;
    returnObj.message = " deletion failed";
    res.status(204).send("all good");
  });
}

function suggestedTripStories(req, res, next) {
  var userInterestArr = [];
  var matched = [];
  var userId = req.query.jsonString;
  _user3.default.findOne({ _id: userId }).then(function (userFound) {
    for (var i = 0; i < userFound.listofTags.length; i++) {
      userInterestArr.push(userFound.listofTags[i].userTags);
    }
    _tripStory2.default.find().then(function (tripStoryarr) {
      var tripStories = tripStoryarr;
      for (var i = 0; i < userInterestArr.length; i++) {
        for (var j = 0; j < tripStories.length; j++) {
          for (var k = 0; k < tripStories[j].storyTags.length; k++) {
            if (userInterestArr[i] == tripStories[j].storyTags[k]) {
              matched.push(tripStories[j]);
            }
          }
        }
      }

      var unique = [];
      if (matched.length != 0) {
        unique = [].concat(_toConsumableArray(new Set(matched.map(function (item) {
          return item._id;
        }))));
      }

      var finalResponse = [];
      for (var i = 0; i < tripStories.length; i++) {
        for (var j = 0; j < unique.length; j++) {
          if (unique[j] == tripStories[i]._id) {
            finalResponse.push({
              thumbnailImagePath: tripStories[i].tripStoryImages.length != 0 ? tripStories[i].tripStoryImages[0].srcPath : "",
              title: tripStories[i].storyTitle,
              summary: tripStories[i].storySummary,
              pageUrl: tripStories[i].storyUrl
            });
          }
        }
      }
      res.send({ tripStories: finalResponse });
    });
  }).catch(function (e) {
    return console.log(e);
  });
}

function upcomingRides(req, res, next) {
  Rides.find().then(function (doc) {
    for (var i = 0; i < doc.length; i++) {
      var year = doc[i].startDate[6] + doc[i].startDate[7] + doc[i].startDate[8] + doc[i].startDate[9];
      var date = doc[i].startDate[0] + doc[i].startDate[1];
      var month = doc[i].startDate[3] + doc[i].startDate[4];
      var myDate = new Date(year, month, date);
      if (myDate > Date.now()) {
        console.log(doc[i]);
      }
    }
  });
}

function reviewForUser(req, res, next) {
  var arr = [];
  _user3.default.findOne({ _id: req.query.jsonString }).populate({
    path: "reviewCreated",
    options: { sort: "-reviewDateText" }
  }).then(function (doc) {
    for (var i = 0; i < doc.reviewCreated.length; i++) {
      arr.push({
        reviewText: doc.reviewCreated[0].reviewText,
        averageRatingPercentage: doc.reviewCreated[i].averageRatingPercentage,
        reviewByUser: {
          firstName: doc.fname,
          profilePageUrl: doc.userUrl,
          profilePicture: { srcPath: doc.profilePicture }
        },
        reviewCriterias: doc.reviewCreated[i].reviewCriterias,
        averageRating: doc.reviewCreated[i].averageRating,
        reviewDescription: doc.reviewCreated[i].reviewDescription,
        reviewDateText: (0, _moment2.default)(doc.reviewCreated[i].reviewDateText).format("YYYY-MM-DD")
      });
    }
    res.send(arr[0]);
  });
}

exports.default = {
  testingroute: testingroute,
  reviewForUser: reviewForUser,
  suggestedTripStories: suggestedTripStories,
  load: load,
  create: create,
  update: update,
  remove: remove,
  forgot: forgot,
  changepassword: changepassword,
  reset: reset,
  getUserDetails: getUserDetails,
  userInterests: userInterests,
  forgotReset: forgotReset,
  updateProfileImage: updateProfileImage,
  updateCoverImage: updateCoverImage,
  deleteCoverImage: deleteCoverImage,
  deleteProfileImage: deleteProfileImage,
  sample: sample,
  upcomingRides: upcomingRides
};
module.exports = exports["default"];
//# sourceMappingURL=user.js.map
