'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _solrClient = require('solr-client');

var _solrClient2 = _interopRequireDefault(_solrClient);

var _env = require('../../config/env');

var _env2 = _interopRequireDefault(_env);

var _request = require('request');

var _request2 = _interopRequireDefault(_request);

var _forums = require('../models/forums');

var _forums2 = _interopRequireDefault(_forums);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var indexer = _solrClient2.default.createClient({
    host: _env2.default.solr,
    port: 8983,
    path: "/solr",
    core: "new_core",
    solrVersion: 721
});

function searchSolr(req, res, next) {
    var query2 = indexer.createQuery().q('body: ' + req.query.query + ' OR title: ' + req.query.query).start(0).rows(100).fl('url,title,body,summary,entity,rideDescription');
    var request = indexer.search(query2, function (err, obj) {
        if (err) {
            res.send(err);
        } else {
            res.send(obj.response.docs);
        }
    });
}

function searchSolrCategory(req, res, next) {

    var query2;
    if (req.body.category == 'rides') {
        query2 = indexer.createQuery().q('entity :user-ride OR entity:ride-out').start(0).rows(100).fl('url,title,body,summary,entity,rideDescription');
    } else {
        query2 = indexer.createQuery().q('body: ' + req.query.query + ' OR title: ' + req.query.query).matchFilter('entity', req.body.category).start(0).rows(100).fl('url,title,body,summary,entity,rideDescription');
    }
    var request = indexer.search(query2, function (err, obj) {
        if (err) {
            res.send(err);
        } else {
            res.send(obj.response.docs);
        }
    });
}

function sendSms(req, res, next) {
    var phoneNo = 9599914762;
    var verifyUrl = 'https://push3.maccesssmspush.com/servlet/com.aclwireless.pushconnectivity.listeners.TextListener?userId=refildalt&pass=refildalt13&appid=refildalt&subappid=refildalt&contenttype=1&to=918800782438&from=RECARE&text=Thank you for your interest in Royal Enfield. Your registered store My Store will contact you soon. For queries, give us a missed call at 9599914762&selfid=true&alert=1&dlrreq=true&intflag=fasle';

    //var verifyUrl = "https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=8800782438&sender=RECARE&msgtext=Dear user your one time password is xxx. Please don't share this with any one. Thanks RE team.&intflag=fasle" 
    console.log("=====================verifyurl======================");
    console.log(verifyUrl);
    console.log("======================verifyurl=====================");
    (0, _request2.default)(verifyUrl, function (err, response, body) {
        res.send(body);
    });
}

function searchRide(req, res, next) {
    var resultFromSolr = [];
    var queryForSearch = "";
    if (req.body.placeName === '') {
        queryForSearch = 'startDate:' + req.body.date;
    } else if (req.body.date === "") {
        queryForSearch = 'startPointPlaceName:' + req.body.placeName;
    } else if (req.body.placeName !== '' && req.body.date !== '') {
        queryForSearch = 'startDate:' + req.body.date + ' OR startPointPlaceName:' + req.body.placeName;
    }
    var query2 = indexer.createQuery().q(queryForSearch).matchFilter('entity', req.body.category).start(0).rows(15).fl('duration,startPointPlaceName,endPointPlaceName,title,author,thumbnailImagePath,rideDescription,url,startDate');
    var request = indexer.search(query2, function (err, obj) {
        if (err) {
            res.send(err);
        } else {
            for (var i = 0; i < obj.response.docs.length; i++) {
                resultFromSolr.push({ durationInDays: obj.response.docs[i].duration, startPoint: { name: obj.response.docs[i].startPointPlaceName }, endPoint: { name: obj.response.docs[i].endPointPlaceName }, startDate: obj.response.docs[i].startDate, rideusername: obj.response.docs[i].author[0], rideName: obj.response.docs[i].title[0], url: obj.response.docs[i].url[0], rideDetails: obj.response.docs[i].rideDescription[0], rideImage: obj.response.docs[i].thumbnailImagePath[0] });
            }
            res.send({ object: resultFromSolr });
        }
    });
}

function tripStorySearchCount(req, res, next) {

    var categoryObj = [];
    var resultFromSolr = [];
    var query2 = indexer.createQuery().q('entity:trip-story').matchFilter('country', req.headers['x-custom-country']).matchFilter('language', req.headers['x-custom-language']).start(0).rows(100).fl('category,title,author,thumbnailImagePath,summary,url').facet({ field: 'cat' });

    var request = indexer.search(query2, function (err, obj) {
        if (err) {
            res.send(err);
        } else {
            for (var i = 0; i < obj.facet_counts.facet_fields.cat.length; i++) {
                categoryObj.push({
                    name: obj.facet_counts.facet_fields.cat[i],
                    value: obj.facet_counts.facet_fields.cat[i += 1]
                });
            }
            res.send({ categories: categoryObj });
        }
    });
}

function tripStorySearch(req, res, next) {
    var resultFromSolr = [];
    var dataSent = "";
    if (req.body.category) {
        if (req.body.category == "*:*") {
            dataSent = "*:*";
        } else {
            dataSent = 'category:' + req.body.category;
        }
    } else if (req.body.title) {
        dataSent = 'title:' + req.body.title + ' OR author:' + req.body.title + ' OR summary : ' + req.body.title;
    }
    console.log('=============tripStorySearch=== headers====================');
    console.log(req.headers['x-custom-language']);
    console.log('==============tripStorySearch======================');

    var query2 = indexer.createQuery().q(dataSent).matchFilter('entity', 'trip-story').matchFilter('country', req.headers['x-custom-country']).matchFilter('language', req.headers['x-custom-language']).start(0).rows(100).sort({ dateSort: 'desc' }).fl('category,title,author,thumbnailImagePath,summary,url');
    var request = indexer.search(query2, function (err, obj) {
        if (err) {
            res.send(err);
        } else {
            res.send({ objects: obj.response.docs, count: obj.response.numFound });
        }
    });
}

function tripStorySearchOnClick(req, res, next) {
    var i = 0;
    var resultFromSolr = [];
    var query2 = indexer.createQuery().q('category:' + req.body.category).matchFilter('entity', 'trip-story').matchFilter('country', req.headers['x-custom-country']).matchFilter('language', req.headers['x-custom-language']).start(0).rows(10).sort({ dateSort: 'desc' }).fl('category,title,summary,thumbnailImagePath,author,url');
    var request = indexer.search(query2, function (err, obj) {
        if (err) {
            res.send(err);
        } else {
            res.send({ objects: obj.response.docs, count: obj.response.numFound });
        }
    });
}

function forumTopicSearchOnClick(req, res, next) {
    var i = 0;
    var categoryObj = [];
    var resultFromSolr = [];

    var query2 = indexer.createQuery().q('*:*').matchFilter('entity', 'forum-topic').facet({ field: 'cat' }).start(0).rows(10).fl('category,views,summary,author,url,postedOn,replies').facet({ field: 'cat' });
    var request = indexer.search(query2, function (err, obj) {
        if (err) {
            res.send(err);
        } else {
            for (var i = 0; i < obj.facet_counts.facet_fields.cat.length; i++) {
                categoryObj.push({
                    name: obj.facet_counts.facet_fields.cat[i],
                    value: obj.facet_counts.facet_fields.cat[i += 1]
                });
            }
            res.send({ categories: categoryObj });
        }
    });
}

function forumTopicSearch(req, res, next) {
    var i = 0;
    var resultFromSolr = [];

    var dataSent = '';
    var obj = {};
    if (req.body.category) {
        obj = { categoryName: { $regex: new RegExp(req.body.category), $options: 'i' } };
    } else if (req.body.title) {
        obj = {
            $and: {
                locale: { country: req.headers['x-custom-country'], language: req.headers['x-custom-language'] },
                $or: [{ forumTitle: { $regex: new RegExp(req.body.title), $options: 'i' } }, { forumBody: { $regex: new RegExp(req.body.title), $options: 'i' } }, { forumAuthor: { $regex: new RegExp(req.body.title), $options: 'i' } }]
            }
        };
    }
    _forums2.default.find(obj).populate({
        path: 'postedBy comment'

    }).sort({ postedOn: "desc" }).then(function (docs) {
        for (i = 0; i < docs.length; i++) {
            resultFromSolr.push({ author: docs[i].postedBy.fname ? docs[i].postedBy.fname : '', userUrl: docs[i].postedBy.userUrl, title: docs[i].forumTitle, category: docs[i].categoryName, postedOn: docs[i].postedOn, summary: docs[i].forumBody, views: docs[i].views, replyCount: docs[i].comment.length ? docs[i].comment.length : 0, url: docs[i].forumUrl });
        }
        res.send({ objects: resultFromSolr });
    }).catch(function (err) {
        res.status(500).send("Error");
    });
}

function NewsSearch(req, res, next) {
    var resultFromSolr = [];
    var dataSent = '';
    if (req.body.category) {
        dataSent = 'category:' + req.body.category;
    } else if (req.body.title) {
        dataSent = 'title:' + req.body.title + ' OR author:' + req.body.title + ' OR summary:' + req.body.title;
    }
    var query2 = indexer.createQuery().q(dataSent).matchFilter('entity', 'news').start(0).rows(10).sort({ dateSort: 'desc' }).fl('category,title,author,thumbnailImagePath,summary,url');

    var request = indexer.search(query2, function (err, obj) {
        if (err) {
            res.send(err);
        } else {
            for (var i = 0; i < obj.response.docs.length; i++) {
                resultFromSolr.push({ createdBy: obj.response.docs[i].author[0], category: obj.response.docs[i].category[0], title: obj.response.docs[i].title[0], url: obj.response.docs[i].url[0], summary: obj.response.docs[i].summary[0], newsImage: obj.response.docs[i].thumbnailImagePath[0] });
            }
            res.send({ objects: resultFromSolr, count: obj.response.numFound });
        }
    });
}

function TestingSearch(req, res, next) {

    var resultFromSolr = [];
    var dataSent = '';

    var query2 = indexer.createQuery().q('*:*').matchFilter('entity', 'user-ride').rangeFilter({ field: 'startDate', start: '30-05-2018', end: '31-05-2018' }).start(0).rows(10).fl('category,title,author,thumbnailImagePath,summary,url');

    var request = indexer.search(query2, function (err, obj) {
        if (err) {
            res.send(err);
        } else {
            for (var i = 0; i < obj.response.docs.length; i++) {

                resultFromSolr.push({ createdBy: obj.response.docs[i].author[0], category: obj.response.docs[i].category[0], title: obj.response.docs[i].title[0], url: obj.response.docs[i].url[0], summary: obj.response.docs[i].summary[0], newsImage: obj.response.docs[i].thumbnailImagePath[0] });
            }
            res.send({ objects: resultFromSolr });
        }
    });
}

exports.default = {
    sendSms: sendSms,
    searchSolr: searchSolr,
    searchRide: searchRide,
    tripStorySearch: tripStorySearch,
    tripStorySearchOnClick: tripStorySearchOnClick,
    forumTopicSearchOnClick: forumTopicSearchOnClick,
    tripStorySearchCount: tripStorySearchCount,
    forumTopicSearch: forumTopicSearch,
    NewsSearch: NewsSearch,
    searchSolrCategory: searchSolrCategory
};
module.exports = exports['default'];
//# sourceMappingURL=solr.js.map
