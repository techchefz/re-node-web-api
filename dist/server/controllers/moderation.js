"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _socialfeeds = require("../models/socialfeeds");

var _socialfeeds2 = _interopRequireDefault(_socialfeeds);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function getTwitterData(req, res, next) {
	_socialfeeds2.default.find().then(function (doc) {
		res.send(doc);
	});
}

function moderateTwitterData(req, res, next) {
	_socialfeeds2.default.find({ isApproved: true }).then(function (docs) {
		for (var i = 0; i < docs.length; i++) {
			if (docs[i]._id == req.body.docIds) {
				_socialfeeds2.default.find({ _id: req.body.docIds }).remove().then(function (results) {
					res.send(results.msg = "Document Rejected");
				});
			}
		}
	}).catch(function (err) {
		return res.send(err);
	});
}

exports.default = { getTwitterData: getTwitterData, moderateTwitterData: moderateTwitterData };
module.exports = exports["default"];
//# sourceMappingURL=moderation.js.map
