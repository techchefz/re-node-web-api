'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _dealers = require('../models/dealers');

var _dealers2 = _interopRequireDefault(_dealers);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function get(req, res) {
  // connect to mongodb and find list of dealers and return the response
  return res.send({ success: true, message: 'dealer found', data: req.dealers });
}

function create(req, res, next) {
  _dealers2.default.findOneAsync({
    $and: [{ dealerName: req.body.dealerName }, { placeId: req.body.placeId }]
  }).then(function (foundDealer) {
    if (foundDealer !== null) {
      var returnObj = {
        success: true,
        message: '',
        data: {}
      };
      returnObj.message = 'dealer already exist';
      returnObj.success = false;
      return res.send(returnObj);
    } else {
      var dealer = new _dealers2.default({

        dealer_id: req.body.dealer_id,
        DealerName: req.body.DealerName,
        BranchName: req.body.BranchName,
        address: req.body.address,
        contact_number: req.body.contact_number,
        isServiceApplicable: req.body.isServiceApplicable,
        isSalesApplicable: req.body.isSalesApplicable,
        StartTime: req.body.StartTime,
        EndTime: req.body.EndTime,
        Week_Off: req.body.Week_Off,
        image: req.body.image,
        email: req.body.email,
        review: req.body.review
      });
      dealer.saveAsync().then(function (savedDealer) {
        var returnObj = {
          success: true,
          message: '',
          data: {}
        };
        returnObj.data.dealer = savedDealer;
        returnObj.message = 'dealer created successfully';
        res.send(returnObj);
      }).error(function (e) {
        return next(e);
      });
    }
  });
}
function update(req, res, next) {
  //code to update dealer details goes here ..
}

function remove(req, res, next) {
  // code to remove dealer goes here..

}

function getBusinessHours(str) {
  var d = new Date();
  var n = d.getDay();
  var finalDate;
  var spl = str.split(";");
  for (var i = 0; i < spl.length; i++) {
    var stringify = JSON.stringify(spl[i]);
    var parsed = JSON.parse(stringify);
    if (parsed[0] == n) {
      finalDate = parsed.split(":");
      var from = (0, _moment2.default)(finalDate[1] + ":" + finalDate[2], ["HH:mm"]).format("hh:mm A");
      var to = (0, _moment2.default)(finalDate[3] + ":" + finalDate[4], ["HH:mm"]).format("hh:mm A");
      console.log(from + "-" + to);
      var time = from + "-" + to;
      return time;
    }
  }
}

function getDealerDetails(req, res, next) {
  var d = new Date();
  var day = d.getDay();
  var finalDate;
  var from;
  var to;
  var obj;
  _dealers2.default.findOne({ _id: req.query.obj }).then(function (dealers) {
    var weeklyOfff;
    switch (JSON.parse(dealers.WeeklyOff)) {
      case 0:
        weeklyOfff = "Sunday";
        break;
      case 1:
        weeklyOfff = "Monday";
        break;
      case 2:
        weeklyOfff = "Tuesday";
        break;
      case 3:
        weeklyOfff = "Wednesday";
        break;
      case 4:
        weeklyOfff = "Thursday";
        break;
      case 5:
        weeklyOfff = "Friday";
        break;
      case 6:
        weeklyOfff = "Saturday";
    }
    var businessHours = getBusinessHours(dealers.BusinessHours);
    obj = {
      openingTime: businessHours.split("-")[0],
      closingTime: businessHours.split("-")[1],
      weeklyOff: weeklyOfff
    };
    console.log(obj);
    res.send(obj);
  });
}

exports.default = {
  get: get,
  create: create,
  update: update,
  remove: remove,
  getDealerDetails: getDealerDetails
};
module.exports = exports['default'];
//# sourceMappingURL=dealers.js.map
