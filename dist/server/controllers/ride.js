'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ride = require('../models/ride');

var _ride2 = _interopRequireDefault(_ride);

var _aemcontroller = require('./aemcontroller');

var _request = require('request');

var _request2 = _interopRequireDefault(_request);

var _env = require('../../config/env');

var _env2 = _interopRequireDefault(_env);

var _sortJsonArray = require('sort-json-array');

var _sortJsonArray2 = _interopRequireDefault(_sortJsonArray);

var _geoLib = require('geo-lib');

var _geoLib2 = _interopRequireDefault(_geoLib);

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _user = require('../models/user');

var _user2 = _interopRequireDefault(_user);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_mongoose2.default.Promise = global.Promise;

function create(req, res, next) {
  var waypts = [];
  var imagePath, imageName;
  var arrayFilePath = [];
  var returnObj = {
    success: true,
    message: '',
    data: {}
  };

  req.files.forEach(function (file) {
    arrayFilePath.push({ srcPath: "/" + file.path });
  });
  var ride;

  if (req.body.waypoints) {
    waypts = JSON.parse(req.body.waypoints);
    ride = new _ride2.default({
      rideStartDateIso: (0, _moment2.default)(req.body.startDate).toISOString(),
      rideEndDateIso: (0, _moment2.default)(req.body.endDate).toISOString(),
      rideName: req.body.rideName,
      locale: {
        language: req.headers['x-custom-language'],
        country: req.headers['x-custom-country']
      },
      startPoint: {
        name: req.body.startPoint[1],
        latitude: req.body.startLatitude,
        longitude: req.body.startLongitude
      },
      endPoint: {
        name: req.body.destination,
        latitude: req.body.destinationlatitude,
        longitude: req.body.destinationlongitude
      },
      durationInDays: req.body.durationInDays,
      rideDetails: req.body.rideDetails,
      startDate: req.body.startDate,
      endDate: req.body.endDate,
      terrain: req.body.terrain,
      createdByUser: req.body.userId,
      startTime: '' + (req.body.startTimeHours + ':' + req.body.startTimeMins + ':' + req.body.startTimeZone),
      totalDistance: req.body.totalDistance,
      rideImages: arrayFilePath,
      personalInfo: {
        fName: req.body.fname,
        lName: req.body.lname,
        gender: req.body.gender,
        email: req.body.email,
        password: req.body.password,
        isRoyalEnfieldOwner: req.body.moto,
        dob: req.body.dob,
        city: req.body.city,
        bikeName: req.body.bikeOwned
      },
      waypoints: waypts,
      rideCategory: req.body.rideCategory,

      geo: { type: 'Point', coordinates: [req.body.startLongitude, req.body.startLatitude] }
      //  geometry : {type : 'Point',coordinates :[req.body.startLongitude,req.body.startLatitude]}
    });
  } else {
    ride = new _ride2.default({
      rideStartDateIso: (0, _moment2.default)(req.body.startDate).toISOString(),
      rideEndDateIso: (0, _moment2.default)(req.body.endDate).toISOString(),
      rideName: req.body.rideName,
      startPoint: {
        name: req.body.startPoint[1],
        latitude: req.body.startLatitude,
        longitude: req.body.startLongitude
      },
      endPoint: {
        name: req.body.destination,
        latitude: req.body.destinationlatitude,
        longitude: req.body.destinationlongitude
      },
      locale: {
        language: req.headers['x-custom-language'],
        country: req.headers['x-custom-country']
      },
      durationInDays: req.body.durationInDays,
      rideDetails: req.body.rideDetails,
      startDate: req.body.startDate,
      endDate: req.body.endDate,
      terrain: req.body.terrain,
      createdByUser: req.body.userId,
      startTime: '' + (req.body.startTimeHours + ':' + req.body.startTimeMins + ':' + req.body.startTimeZone),
      totalDistance: req.body.totalDistance,
      rideImages: arrayFilePath,
      personalInfo: {
        fName: req.body.fname,
        lName: req.body.lname,
        gender: req.body.gender,
        email: req.body.email,
        password: req.body.password,
        isRoyalEnfieldOwner: req.body.moto,
        dob: req.body.dob,
        city: req.body.city,
        bikeName: req.body.bikeOwned
      },
      rideCategory: req.body.rideCategory,
      geo: { type: 'Point', coordinates: [req.body.startLongitude, req.body.startLatitude] }

      //  geometry : {type : 'Point',coordinates :[req.body.startLongitude,req.body.startLatitude]}


    });
  }
  ride.saveAsync().then(function (savedRide) {
    _user2.default.findOne({ _id: savedRide.createdByUser }).then(function (userFound) {
      userFound.ridesCreated.push(savedRide._id);
      userFound.save();
    }).catch(function (e) {
      return console.log(e);
    });
    var waypointsData = "";
    for (var i = 0; i < savedRide.waypoints.length; i++) {
      var waypointsname = savedRide.waypoints[i].name;
      var waypointslat = savedRide.waypoints[i].latitude;
      var waypointslng = savedRide.waypoints[i].longitude;
      waypointsData += waypointsname + "," + waypointslat + "," + waypointslng + "|";
    }
    var username = 'admin';
    var password = 'admin';
    var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
    _request2.default.post({
      headers: {
        'content-type': 'application/x-www-form-urlencoded',
        "Authorization": auth
      },
      url: _env2.default.aemAuthorUrl + '/bin/createPage',
      form: {
        entity: savedRide.rideCategory,
        pageId: savedRide._id.toString(),
        title: savedRide.rideName,
        country: savedRide.locale.country,
        language: savedRide.locale.language,
        pageProperties: JSON.stringify({
          "rideEndDateIso": savedRide.rideEndDateIso,
          "rideStartDateIso": savedRide.rideStartDateIso,
          "startPointPlaceName": savedRide.startPoint.name,
          "startPointLongitude": req.body.startLongitude,
          "startPointLatitude": req.body.startLatitude,
          "endPointPlaceName": savedRide.endPoint.name,
          "endPointLongitude": req.body.destinationlongitude,
          "endPointLatitude": req.body.destinationlatitude,
          "startDate": req.body.startDate,
          "endDate": req.body.endDate,
          "terrain": req.body.terrain,
          "duration": req.body.durationInDays,
          "thumbnailImagePath": arrayFilePath[0].srcPath,
          "author": req.body.fname + " " + req.body.lname,
          "totalDistance": req.body.totalDistance,
          "rideDescription": req.body.rideDetails,
          "wayPoints": waypointsData,
          "dateSort": (0, _moment2.default)().toISOString()
        })
      }
    }, function (error, response, data) {

      var resData = JSON.parse(data);
      if (error) {
        returnObj.data.ride = null;
        returnObj.message = 'Error Creating ride';
        returnObj.success = false;
        res.send(returnObj);
      } else {
        _ride2.default.findOneAndUpdate({ _id: savedRide._id }, { $set: { ridePageUrl: resData.pagePath } }).then(function (updatedride) {
          returnObj.data.ride = savedRide;
          returnObj.data.ride.ridePageUrl = resData.pagePath.toString();
          returnObj.message = 'ride created successfully';
          res.send(returnObj);
        });
      }
    });
  }).catch(function (e) {
    return console.log(e);
  });
}

function update(req, res, next) {}

function remove(req, res, next) {
  // code to remove ride goes here..
  _ride2.default.find({ _id: req.body.rideId }).remove().then(function (removedDoc) {
    res.send("ride removed successfully");
  }).catch(function (e) {
    return console.log(e);
  });
}

/**
 * Get Rides list.
 * @property {number} req.body.skip - Number of rides to be skipped.
 * @property {number} req.body.limit - Limit number of rides to be returned.
 * @returns {User[]}
 */

function getRide(req, res, next) {
  _ride2.default.findOne({ _id: req.query.jsonString }).populate({
    path: 'createdByUser',
    model: 'User'
  }).then(function (ride) {
    var waypointsData = "";
    if (ride.waypoints) {
      waypointsData = "";
      for (var i = 0; i < ride.waypoints.length; i++) {
        var waypointsname = ride.waypoints[i].name;
        var waypointslat = ride.waypoints[i].latitude;
        var waypointslng = ride.waypoints[i].longitude;
        waypointsData += waypointsname + "," + waypointslat + "," + waypointslng + "|";
      }
    }

    var obj = {
      rideId: ride._id,
      rideName: ride.rideName,
      startPoint: {
        name: ride.startPoint.name,
        longitude: ride.startPoint.longitude,
        latitude: ride.startPoint.latitude
      },
      endPoint: {
        name: ride.endPoint.name,
        longitude: ride.endPoint.longitude,
        latitude: ride.endPoint.latitude
      },
      durationInDays: ride.durationInDays,
      distance: ride.totalDistance,
      terrain: ride.terrain,
      startDate: ride.startDate,
      endDate: ride.endDate,
      createdBy: {
        fullName: ride.personalInfo.fName + " " + ride.personalInfo.lName,
        phone: ride.createdByUser.phoneNo,
        email: ride.personalInfo.email
      },
      waypoints: waypointsData,
      thumbnailImagePath: ride.rideImages.length != 0 ? ride.rideImages[0].srcPath : ''

    };
    res.send(obj);
  }).catch(function (err) {
    console.log(err);
  });
}

function getRides(req, res, next) {
  _ride2.default.find({ _id: req.body.rideId }).then(function (rides) {
    res.send(rides);
  });
}

function getRidesAroundMeBackup(req, res, next) {
  //   Ride.aggregate([
  //    {
  //      $geoNear: {
  //         near: { type: "Point", coordinates: [ req.body.longitude , req.body.latitude ] },
  //         distanceField: "dist.calculated",
  //         query: { type: "public" },
  //         includeLocs: "dist.location",
  //         num: 5,
  //         spherical: true
  //      }
  //    }
  // ])
  _ride2.default.geoNear([req.body.latitude, req.body.longitude], {
    distanceField: "dist.calculated",
    spherical: true
  }).then(function (doc) {
    var result = {
      object: doc
    };
    res.send(result);
  });
}

function getRidesAroundMe(req, res, next) {
  var Userlat = parseFloat(req.body.latitude);
  var Userlong = parseFloat(req.body.longitude);
  var category = req.body.category;
  if (req.body.category === '' || req.body.category === null) {
    category = "user-ride";
  }
  _ride2.default.find({
    geo: {
      $near: {
        $geometry: {
          type: 'Point',
          coordinates: [req.body.longitude, req.body.latitude]
        }

      }
    }

  }).limit(parseInt(req.body.limit)).then(function (doc) {
    var arrrides = [];
    for (var i = 0; i < doc.length; i++) {
      var verDate = (0, _moment2.default)(doc[i].startDate, "DD-MM-YYYY").format('YYYY-MM-DD');
      if ((0, _moment2.default)(verDate).isAfter((0, _moment2.default)().format('YYYY-MM-DD')) && doc[i].rideCategory == category) {
        arrrides.push(doc[i]);
      }
    }

    res.send({ object: arrrides });
  }).catch(function (e) {
    return console.log(e);
  });
}

exports.default = {
  getRides: getRides,
  getRide: getRide,
  create: create,
  update: update,
  remove: remove,
  getRidesAroundMe: getRidesAroundMe
};
module.exports = exports['default'];
//# sourceMappingURL=ride.js.map
