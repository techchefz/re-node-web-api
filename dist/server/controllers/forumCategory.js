"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _forumCategory = require("../models/forumCategory");

var _forumCategory2 = _interopRequireDefault(_forumCategory);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function create(req, res, next) {
    var forumCategory = new _forumCategory2.default({
        categoryName: req.body.categoryName,
        categorySubHeading: req.body.categorySubHeading
    });
    forumCategory.save().then(function (savedCategory) {
        res.send("Category Created");
    }, function (err) {
        console.log(e);
        res.send("Error Creating Category");
    });
}

function getCategoryDetials(req, res, next) {
    _forumCategory2.default.findOne({ categoryName: req.body.categoryName }).then(function (categoryDetails) {
        var returnObj = {
            categoryName: categoryDetails.categoryName,
            categorySubHeading: categoryDetails.categorySubHeading,
            subscribersCount: categoryDetails.subscribedUsers.length ? categoryDetails.subscribedUsers.length : 0,
            repliesCount: categoryDetails.totalRepliesCount ? categoryDetails.totalRepliesCount : 0,
            viewsCount: categoryDetails.totalViewsCount ? categoryDetails.totalViewsCount : 0,
            topicsCount: categoryDetails.totalRepliesCount ? categoryDetails.totalRepliesCount : 0
        };
        res.send(returnObj);
    }, function (err) {
        res.send(err);
    });
}

function subscribeToForumCategory(req, res, next) {
    _forumCategory2.default.findOneAndUpdate({ categoryName: req.body.categoryName }, { $push: { subscribedUsers: { userId: req.body.userId, email: req.body.email } } }).then(function (updatedForumCategory) {
        res.send("User Subscribed Successfully");
    }, function (err) {
        res.send("Error Subscribing");
    });
}

exports.default = {
    create: create,
    getCategoryDetials: getCategoryDetials,
    subscribeToForumCategory: subscribeToForumCategory
};
module.exports = exports["default"];
//# sourceMappingURL=forumCategory.js.map
