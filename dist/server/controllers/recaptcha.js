'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _recaptcha = require('../models/recaptcha');

var _recaptcha2 = _interopRequireDefault(_recaptcha);

var _request = require('request');

var _request2 = _interopRequireDefault(_request);

var _env = require('../../config/env');

var _env2 = _interopRequireDefault(_env);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function recaptchastore(req, res, next) {
  req.on("data", function (buffer) {
    var obj = JSON.parse(buffer);
    var randomnumber = obj.randomnumber;
    var publickey = obj.publickey;
    var privatekey = obj.privatekey;

    var recaptcha = new _recaptcha2.default({
      Randomnumber: randomnumber,
      publickey: publickey,
      privatekey: privatekey
    });

    recaptcha.save().then(function (doc) {}, function (e) {
      console.log(e);
    });

    res.send("recaptchastore");
  });
}

function recaptchaverify(req, res, next) {
  if (req.body.captcha === undefined || req.body.captcha === '' || req.body.captcha === null) {
    return res.json({ "success": false, "msg": "Please select captcha" });
  }
  //Secret Key
  var secretKey = _env2.default.recaptchaSecretKey;

  // Verify URL
  var verifyUrl = 'https://google.com/recaptcha/api/siteverify?secret=' + secretKey + '&response=' + req.body.captcha + '&remoteip=' + req.connection.remoteAddress;

  // Make Request To VerifyURL
  (0, _request2.default)(verifyUrl, function (err, response, body) {
    body = JSON.parse(body);
    res.send({ msg: "Verified" });
    // If Not Successful
    if (body.success !== undefined && !body.success) {
      res.status(500).send({ msg: "Recaptha not Verified" });
    }
  });
}

exports.default = {

  recaptchaverify: recaptchaverify,
  recaptchastore: recaptchastore

};
module.exports = exports['default'];
//# sourceMappingURL=recaptcha.js.map
