'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _xlsxToJsonLc = require('xlsx-to-json-lc');

var _xlsxToJsonLc2 = _interopRequireDefault(_xlsxToJsonLc);

var _dealers = require('../models/dealers');

var _dealers2 = _interopRequireDefault(_dealers);

var _citiesStates = require('../models/citiesStates');

var _citiesStates2 = _interopRequireDefault(_citiesStates);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function syncCityStates(req, res) {
    (0, _xlsxToJsonLc2.default)({
        input: "./sample-data/state_city.xls",
        output: null,
        lowerCaseHeaders: false
    }, function (err, result) {
        if (err) {
            console.error(err);
        } else {
            _citiesStates2.default.insertMany(result, function () {
                res.send("Inserted Successfully");
            });
        }
    });
}

function syncDealers(req, res) {
    (0, _xlsxToJsonLc2.default)({
        input: "./sample-data/dealers_master_with_latlong.xlsx",
        output: null,
        lowerCaseHeaders: false
    }, function (err, result) {
        if (err) {
            console.error(err);
        } else {
            _dealers2.default.insertMany(result, function () {
                res.send("Inserted Successfully");
            });
        }
    });
}

exports.default = {
    syncDealers: syncDealers, syncCityStates: syncCityStates
};
module.exports = exports['default'];
//# sourceMappingURL=syncSampleData.js.map
