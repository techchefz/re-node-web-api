import BikeSchema from '../models/bike';
import News from '../models/news';

import mongoose from 'mongoose';

function generateId(req, res, next) {

    var category = req.body.requestContentJSON;
    if (category == 'news') {
        const news = new News({
        });
        news
            .save()
            .then((savedNews) => {
                var obj = { pageId: savedNews._id }
                res.send({ pageId: savedNews._id.toString() });
            })
            .catch((error) => {
                console.log(error);
            });


    }
    else {
        var arr = category.split('_');
        const bike = BikeSchema({
            bikeName: arr[1]
        });
        bike.save().then((savedBike) => {
            res.send({ pageId: arr[0] + '_' + savedBike._id });
        })


    }


}
export default { generateId };

