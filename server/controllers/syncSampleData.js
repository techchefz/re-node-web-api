import exceltoJson from 'xlsx-to-json-lc';
import Dealer from '../models/dealers';
import CityStates from '../models/citiesStates';


function syncCityStates(req, res) {        
    exceltoJson({
        input: "./sample-data/state_city.xls",
        output: null,
        lowerCaseHeaders: false
    }, function (err, result) {
        if (err) {
            console.error(err);
        } else {
            CityStates.insertMany(result, () => { res.send("Inserted Successfully") });
        }
    });
}

function syncDealers(req, res) {
    exceltoJson({
        input: "./sample-data/dealers_master_with_latlong.xlsx",
        output: null,
        lowerCaseHeaders: false
    }, function (err, result) {
        if (err) {
            console.error(err);
        } else {        
            Dealer.insertMany(result, () => { res.send("Inserted Successfully") });
        }
    });
}

export default {
    syncDealers, syncCityStates
}