import UserSchema from '../models/user';
import Story from '../models/tripStory';
import multer from 'multer';
import request from "request";
import config from '../../config/env';
import moment from 'moment';
import youtubeThumbnail from 'youtube-thumbnail';


function getStory(req, res, next) {
console.log(req.body)
console.log("======================================req===");
  var storyId;
  var nextStory = {
    imagePath: null,
    title: null,
  };
  var previousStory = {
    imagePath: null,
    title: null
  };

  if (req.body.requestContentJSON) { storyId = req.body.requestContentJSON };
console.log("sdasd" + req.body)
  //To find previos Story
  Story.findOne({ '_id': { '$lt': storyId } }, 'tripStoryImages storyTitle storyUrl locale')
    .sort({ '_id': -1 })
    .limit(1)
    .then((data) => {
      if (data != null) {
        if (data.tripStoryImages.length > 0 && data.locale.country === req.headers['x-custom-country'] && data.locale.language === req.headers['x-custom-language']) {
          previousStory = {
            thumbnailImagePath: data.tripStoryImages[0].srcPath,
            title: data.storyTitle,
            pageUrl: data.storyUrl,
          }
        }
      }

    }).catch ((err) => {
      res.status(500).send({ message: "Error", success: "False" });
    });



  //To find next Story
  Story.findOne({ '_id': { '$gt': storyId } }, 'tripStoryImages storyTitle storyUrl')
    .sort('_id')
    .limit(1)
    .then((data) => {
console.log("in next story");
console.log(data)
        if (data != null) {
        if (data.tripStoryImages.length > 0 && data.locale.country === req.headers['x-custom-country'] && data.locale.language === req.headers['x-custom-language']) {
          nextStory = {
            thumbnailImagePath: data.tripStoryImages[0].srcPath,
            title: data.storyTitle,
            pageUrl: data.storyUrl
          }
        }
      }
    }, (err) => {
      console.log(err);
    });
console.log("==================================================================")
console.log(storyId);
console.log("===================================================================");
  Story.findOne({ _id: storyId })
    .populate('postedBy', 'fname lname profilePicture')
    .then((doc) => {
console.log("=-=================================================")
console.log(doc)
console.log("===================================================")
      var videoArr = [];
      var videoUrl = [];
      videoUrl = doc.videoThumbnail

      for (var i = 0; i < doc.tripStoryImages.length; i++) {
        videoArr.push(doc.tripStoryImages[i])
      }
      var arr = [];
      var returnObj =
      {
        tripStoryId: doc._id,
        title: doc.storyTitle,
        tripStoryBody: doc.storyBody,
        summary: doc.storySummary,
        postedOn: doc.postedOn,
        coverImage: { srcPath: doc.coverImage },
        postedByUser: {
          firstName: doc.postedBy.fname,
          lastName: doc.postedBy.lname,
          profilePicture: { srcPath: doc.postedBy.profilePicture }
        },
        tripImages: doc.tripStoryImages,
        videoUrl: doc.videoThumbnail,
        categories: { category: doc.categoryName },
      };
      returnObj.previousStory = previousStory;
      returnObj.nextStory = nextStory;
      res.send(returnObj);
    }, (err) => {
      res.send({ message: err, success: "False" });
    });
}



function create(req, res, next) {
  var imagePath, imageName;
  var arrayFilePath = [];
  const returnObj = {
    success: true,
    message: '',
    data: {},
  };
  const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, './node/assets/TripStory');
    },
    filename: function (req, file, cb) {
      imageName = Date.now() + file.originalname;
      cb(null, imageName);
      imagePath = './node/assets/TripStory' + imageName;
    }
  });

  const uploadObj = multer({ storage: storage }).array('tripImages', 10);
  uploadObj(req, res, (err) => {
	console.log("=============================req.body========================")
	console.log(req.body)
	console.log("=============================req.body========================")
	console.log("inside upload obj")
    console.log('==============files======================');
    console.log(req.files);
    console.log('================files====================');
    var videoData = [];
    for (var i = 0; i < req.body.field_name.length; i++) {
      videoData.push(req.body.field_name[i].replace('watch?v=', 'embed/'))
    }
	console.log("after video");
    if (err) {
      returnObj.data.story = null;
      returnObj.message = 'Error Creating Story';
      returnObj.success = false;
      returnObj.data.error = err;
      res.send(returnObj);
    } else {
	console.log("inside else");
      req.files
        .forEach(file => {
          arrayFilePath.push({ srcPath: "/" + file.path });
        });

      if (arrayFilePath.length == 0) {
	console.log("inside array file path if")
        arrayFilePath.push({ srcPath: "https://content3.jdmagicbox.com/comp/ernakulam/i4/0484px484.x484.170825114532.p1i4/catalogue/tripstory-kakkanad-west-ernakulam-tour-operators-3c2c1f5.jpg" });
      }
console.log("here")
      const story = new Story({
        storyTitle: req.body.storyTitle,
        storyBody: req.body.editordata,
        storySummary: req.body.storySummary,
        locale: {
          language: req.headers['x-custom-language'],
          country: req.headers['x-custom-country']
        },
        postedBy: req.body.postedBy,
        storyTags: JSON.parse(req.body.storyTags),
        categoryName: req.body.storyCategory,
        tripStoryImages: arrayFilePath,
        videoThumbnail: videoData
      });
      story.saveAsync()
        .then((savedStory) => {
	console.log("==================saved story==================")
	console.log(savedStory)
	console.log("==================saved story===================")
	console.log("story is saved here");
          UserSchema.find({ _id: savedStory.postedBy }).then((userFound) => {
            userFound[0].tripStoriesCreated.push(savedStory._id);
            userFound[0].save();
          });

          var username = 'admin';
          var password = 'admin';
          var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
          request.post({
            headers: {
              'content-type': 'application/x-www-form-urlencoded',
              "Authorization": auth
            },
            url: `${config.aemAuthorUrl}/bin/createPage`,
            form: {
              entity: 'trip-story',
              pageId: savedStory._id.toString(),
              title: savedStory.storyTitle,
              category: savedStory.categoryName,
              country: savedStory.locale.country,
              language: savedStory.locale.language,
              pageProperties: JSON.stringify({
                "title": req.body.storyTitle,
                "author": req.body.postedByUserName,
                "category": req.body.storyCategory,
                "summary": req.body.storySummary,
                "storyTags": req.body.storyTags ? JSON.parse(req.body.storyTags) : '',
                "thumbnailImagePath": arrayFilePath.length != 0 ? arrayFilePath[0].srcPath : '',
                "postedBy": req.body.postedBy,
                "thumbnailImagePathAltText": req.body.storyTitle,
                "dateSort": moment().toISOString(),
                "videoThumbnail": videoData.length !== 0 ? videoData[0] : ''
              })
            }
          }, function (error, response, data) {
		console.log("================body")
		console.log(response.body)
		console.log("=============body====")
            var data = JSON.parse(data);
            if (error) {
              returnObj.data.story = null;
              returnObj.message = 'Error Creating Story';
              returnObj.success = false;
              res.status(400).send(returnObj);
            } else {
              Story
                .findOneAndUpdate(
                  { _id: savedStory._id },
                  { $set: { storyUrl: data.pagePath } }
                )
                .then((updatedStory) => {
                  returnObj.data.story = savedStory;
                  if (data.pagePath) {
                    returnObj.data.story = savedStory;
                    returnObj.data.story.storyUrl = data.pagePath;
                    returnObj.message = 'story created successfully';
                    res.send(returnObj)
                  } else {
                    res.status(400).send("Error");
                  }
                })
                .catch(err => res.status(400).send("Error"))
            }
          });
        })
        .error(e => next(e));
    }
  })
}

function update(req, res, next) {
  Story
    .findOneAndUpdate({ _id: req.body._id }, {
      $set: {
        storyTitle: req.body.storyTitle,
        storyBody: req.body.storyBody,
        tripImages: req.body.tripImages,
        storyUrl: req.body.storyUrl
      }
    })
    .then((savedStory) => {
      const returnObj = {
        success: true,
        message: 'Story updated successfully',
        data: savedStory,
      };
      res.send(returnObj);
    })
    .error(e => next(e));
}


function remove(req, res, next) {
  // code to delete story goes here...
  Story
    .findOne({ _id: req.body._id })
    .then((doc) => {
      if (doc.postedBy == req.body.userid) {
        doc.remove();
      }

      else {
        res.send("invalid user");
      }
    })
}

function getStories(req, res, next) {
  Story
    .getStories(parseInt(req.body.skip), parseInt(req.body.limit))
    .then((stories) => {
      const returnObj = {
        success: true,
        message: '',
        data: {},
      };
      returnObj.data.stories = stories;
      returnObj.message = 'stories retrieved successfully';
      res.send(returnObj);
    })
    .error((e) => next(e));
}


function getmystory(req, res, next) {
  var id = req.body.tripstoryid;
  Story
    .find({ _id: id }).populate(
      {
        path: 'comment',
        model: 'comments',
        populate: {
          path: 'Replyid userdetailscomments',
          populate: { path: 'userdetailsreplies' }
        },
      })
    .then((tripstory) => {
      res.send(tripstory[0]);
    })
    .catch(e => res.send(err))
}




export default {
  getStories,
  getStory,
  create,
  update,
  remove,
  getmystory
};
