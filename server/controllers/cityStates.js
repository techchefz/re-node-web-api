import CityStates from '../models/citiesStates';
import Dealers from '../models/dealers';
import request from 'request';

var headers = {
    'Content-Type': 'text/plain'
};

var fetchCityStatesData = {
    "ResourceIdentifier": 206,
    "EntityType": "9021",
    "EntityID": 45,
    "CardType": 34,
    RequestKey: "rgfvrgkjwkdjsddcawdwhcb",
}

var fetchCityStatesString = JSON.stringify(fetchCityStatesData);

var fetchCityStatesOptions = {
    url: 'http://refineb2cdvp.excelloncloud.com/exapis//api/portalentity',
    method: 'POST',
    headers: headers,
    body: fetchCityStatesString
}

function fetchCityStates(req, res, next) {

    request(fetchCityStatesOptions, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            // Print out the response body
            res.send(body);
            const cityStatesData = JSON.parse(body);
            CityStates.insertMany(cityStatesData, (insertedDoc) => {
            res.send(insertedDoc);
            });
        } else {
            console.log(error);
        }
    });

}

function getcity(req, res, next) {
    CityStates
        .find({ StateName: req.body.stateName })
        .then((foundCity) => {
            let citiesArr = [];
            foundCity.map(a => {
                citiesArr.push({ cityName: a.CityName, cityId: a.CityId })
            });
            res.send(citiesArr);
        })
        .catch((err) => {
            res.send(err);
        })

}

function getState(req, res, next) {
    CityStates
        .distinct('StateName')
        .then((foundState) => {
            res.send(foundState);
        });

}

function getDealers(req, res, next) {
    Dealers
        .find({ City: req.body.city })
        .then((foundDealer) => {
            var dealersArr = [];
            foundDealer.map(dealer => {
                let dealerObj = {
                    "DealerName": dealer.DealerName,
                    "BranchName": dealer.BranchName,
                    "address": dealer.AddressLine1 + " " + dealer.AddressLine2 + " " + dealer.AddressLine3,
                    "dealerID": dealer.DealerID,
                }
                dealersArr.push(dealerObj);
            })
            res.send(dealersArr);
        })
        .catch(err => res.send(err))
}
export default {
    getState,
    getcity,
    getDealers,
    fetchCityStates
}


