import SocialFeeds from "../models/socialfeeds";

function getTwitterData(req, res, next) {
	SocialFeeds
		.find()
		.then((doc) => {
			res.send(doc);
		});
}


function moderateTwitterData(req, res, next) {
	SocialFeeds
		.find({ isApproved: true })
		.then((docs) => {
			for (var i = 0; i < docs.length; i++) {
				if (docs[i]._id == req.body.docIds) {
					SocialFeeds
						.find({ _id: req.body.docIds }).remove().then((results) => {
							res.send(results.msg = "Document Rejected");
						});
				}
			}
		})
		.catch(err => res.send(err))
}


export default { getTwitterData, moderateTwitterData };
