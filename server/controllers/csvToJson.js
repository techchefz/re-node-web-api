import ssh2 from "ssh2";
import config from "../../config/env";
import moment from "moment";
import fs from "fs";
// import csvToJson from "../../convert-csv-to-json";
import isNumber from "is-number";
import csv from "csvtojson";
import BikeCsvSchema from "../models/bikeCsv";
import exceltoJson from "xlsx-to-json-lc";
import Dealer from "../models/dealers";
import RideOutSchema from "../models/ride";

const connection = new ssh2();
let resObj = {};
let riderOutHeader = [
  "Name",
  "StartPoint",
  "Destination",
  "TotalDistance",
  "StartDate",
  "EndDate",
  "RideID",
  "Remarks",
  "Source",
  "DocName",
  "DocDate",
  "CompanyID",
  "CompanyCode",
  "CompanyName",
  "TotalParticipantsAllowed",
  "RideSourceLatitude",
  "RideSourceLongitude",
  "RideDestinationLatitude",
  "RideDestinationLongitude",
  "TerrainType",
  "Difficultylevel",
  "PillionFriendly"
];
let rideOutHeadersArray = [];
let bikeCsvArr = [
  "motorcycle_id",
  "motorcycle_name",
  "motorcycle_code",
  "product_active",
  "ModelGroupCode",
  "ModelGroupName",
  "Status"
];
let dummyArray = [];
let headersarr = [];
let finalData = [];
let flag = false;
let fileName;

function connectSftpforbikescsv(req, res) {
  connection.on("ready", function () {
    console.log("Client :: ready");
    connection.sftp(function (err, sftp) {
      if (err) throw err;
      sftp.readdir("DMS-Web3/Testing/Model", function (err, list) {
        if (err) throw err;
        fileName = list[0].filename;
        let sourceFile = sftp.createReadStream(`DMS-Web3/Testing/Model/${fileName}`);

        sourceFile.on("data", fileData => {
          dummyArray.push(fileData.toString("utf8"));
          // console.log("Data");
          var strData = fileData.toString("utf8");
          var splData = strData.split("\r\n");
          headersarr = splData[0].split("|");
        });

        sourceFile.on("end", ends => {
          console.log("Inside End");
          for (var i = 0; i < headersarr.length; i++) {
            for (var j = 0; j < bikeCsvArr.length; j++) {
              if (headersarr[i] == bikeCsvArr[j]) {
                flag = true;
              } else {
                flag = false;
              }
            }
          }

          if (flag == true) {
            let destinationFile = sftp.createWriteStream(`DMS-Web3/Testing/archiveBike/${fileName}`);
            sourceFile.pipe(destinationFile);
            //====================code to write new file at new destination =======================
            fs.appendFile("csvFolder/bike.csv", dummyArray, () => {
              console.log("====================================");
              console.log(`csvFolder/bike.csv`);
              console.log("====================================");
            });
            sftp.appendFile(`DMS-Web3/Testing/archiveBike/${fileName}`, dummyArray, () => {
              console.log("====================================");
              console.log(`File Saved Successfully inside DMS-Web3/Testing/archiveBike/${fileName}`);
              console.log("====================================");
            }
            );
            //====================code to write new file at new destination =======================
            destinationFile.on("finish", () => {
              console.log("destination finished");
              bikeCsvToJson(fileName);
              //================================Code to delete the file==============================

              sftp.unlink(`DMS-Web3/Testing/Model/${fileName}`, function (err) {
                if (err) {
                  console.log("Error, problem starting SFTP: %s", err);
                } else {
                  console.log("file unlinked");
                }
              });

              //================================Code to delete the file==============================
            });
          } else {
            console.log("====================================");
            console.log("File Headers Did`t Matched");
            console.log("====================================");
          }
        });
      });
    });
  });

  connection.on("error", err => {
    console.log(err);
  });

  connection.connect({
    debug: function (s) {
      // console.log(s)
    },
    host: "ftpL.royalenfield.com",
    port: 22,
    username: "interface",
    password: "Interface@re",
    privateKey: fs.readFileSync("config/id_rsa.ppk"),
    passphrase: "Interface@re",
    readyTimeout: 99999
  });
}

function bikeCsvToJson(dummyArray) {
  // var json = csvToJson.formatValueByType().fieldDelimiter("|").getJsonFromCsv("csvFolder/bike.csv");

  csv({ delimiter: "|" }).fromFile(`csvFolder/bike.csv`).then((bikes) => {
    // console.log("================mahh json====================");
    // console.log(bikes);
    // console.log("====================================");
    var finalBikeArray = [];

    bikes.forEach(bike => {
      finalBikeArray.push({
        familyName: isNumber(bike.ModelGroupName) ? bike.ModelGroupName.toString() : bike.ModelGroupName,
        familyCode: isNumber(bike.ModelGroupCode) ? bike.ModelGroupCode.toString() : bike.ModelGroupCode,
        bikeModelCode: isNumber(bike.motorcycle_code) ? bike.motorcycle_code.toString() : bike.motorcycle_code,
        bikeName: isNumber(bike.motorcycle_name) ? bike.motorcycle_name.toString() : bike.motorcycle_name,
        bikeStatus: isNumber(bike.Status) ? bike.Status.toString() : bike.Status
      });
    });
    BikeCsvSchema.insertMany(finalBikeArray).then((docs) => {
      // console.log('============docs========================');
      // console.log(docs);
      // console.log('====================================');
      if (docs) {
        resObj.success = true;
        resObj.message = "inserted successfully";
        resObj.data = {};
        return resObj;
      } else {
        resObj.success = false;
        resObj.message = "error inserting docs";
        resObj.data = err;
        return resObj;
      }
    });
  })


}
function dealerDataToDB(req, res, next) {
  var arr = [];
  exceltoJson(
    { input: "csvFolder/dealer.xlsx", output: null, lowerCaseHeaders: false },
    function (err, result) {
      if (err) {
        console.error(err);
      } else {
        for (var i = 0; i < result.length; i++) {
          arr.push({
            BranchCode: result[i].BranchCode,
            AddressLine1: result[i].AddressLine1,
            AddressLine2: result[i].AddressLine2,
            AddressLine3: result[i].AddressLine3,
            Pincode: result[i].Pincode,
            City: result[i].City,
            State: result[i].State,
            Country: result[i].Country,
            MainPhoneNo: result[i].MainPhoneNo,
            StoreEmailId: result[i].StoreEmailId,
            WeeklyOff: result[i].WeeklyOff,
            StoreManagerName: result[i].StoreManagerName,
            BusinessHours: result[i].BusinessHours,
            Longitude: result[i].Longitude,
            Latitude: result[i].Latitude,
            GooglePlaceID: result[i].GooglePlaceId,
            DealerID: result[i].DealerID,
            BranchName: result[i].BranchName,
            CityID: result[i].CityID,
            StateID: result[i].StateID,
            BranchTypeIdenitifier: result[i].BranchTypeIdenitifier,
            DealerPrincipalName: result[i].DealerPrincipalName,
            Status: result[i].Status,
            Dealer_Source: result[i].Dealer_Source,
            Cover_image: result[i].Cover_image
              ? ` /node/assets/Dealer/CoverImage/${result[i].Cover_image}`
              : "/node/assets/Dealer/CoverImage/Cover_image_placeholder.jpg",
            Cover_image_mobile: result[i].Cover_image_mobile
              ? `/node/assets/Dealer/CoverImage/mobile/${
              result[i].Cover_image_mobile
              }`
              : "/node/assets/Dealer/CoverImage/mobile/Cover_image_mobile_placeholder.jpg",
            Thumbnail_Image: result[i].Thumbnail_Image
              ? `/node/assets/Dealers/ThumbnailImages/${
              result[i].Thumbnail_Image
              }`
              : "/node/assets/Dealer/ThumbnailImage/Thumbnail_Image_placeholder.jpg",
            PrimaryImage: result[i].PrimaryImage
              ? `/node/assetss/Dealers/PrimaryImages/${result[i].PrimaryImage}`
              : "/node/assets/Dealer/PrimaryImage/PrimaryImage_placeholder.jpg",
            PrimaryImage_mobile: result[i].PrimaryImage_mobile
              ? `/node/assetss/Dealers/PrimaryImages/mobiles/${
              result[i].PrimaryImage_mobile
              }`
              : "/node/assets/Dealer/PrimaryImage/mobile/PrimaryImage_mobile_placeholder.jpg",
            DealerImage: result[i].DealerImage
              ? `/node/assetss/Dealers/DealerImages/${result[i].DealerImage}`
              : "/node/assets/Dealer/DealerImage/DealerImage_placeholder.jpg",
            DealerName: result[i].DealerName,
            DealerOneLiner: result[i].DealerOneLiner,
            DealerDescription: result[i].DealerDescription,
            Gallery: result[i].Gallery
          });
        }
        if (err) {
          console.error(err);
        } else {
          console.log("============result========================");
          console.log(arr);
          console.log("=============result=======================");
          Dealer.insertMany(arr, () => {
            res.send("Inserted Successfully");
          });
        }
      }
    }
  );
}

function rideOutCsvToJson(req, res) {
  console.log("Branch Master Done");
  // res.send("finalData.length")
  connection.end();

  connection.on("ready", function () {
    connection.sftp(function (err, sftp) {
      if (err) throw err;
      sftp.readdir("DMS-Web3/Testing/RideList", (err, list) => {
        if (err) throw err;
        if (list.length != 0) {
          fileName = list[0].filename;
          let sourceFile = sftp.createReadStream(`DMS-Web3/Testing/RideList/${fileName}`);

          sourceFile.on("data", fileData => {
            dummyArray.push(fileData.toString("utf8"));
          });

          sourceFile.on("end", ends => {
            console.log("=============dummyArray=======================");
            console.log(dummyArray);
            console.log("==============dummyArray======================");
            var splData = dummyArray[0].split("\r\n");
            var temp = splData[0].split("\n");
            rideOutHeadersArray = temp[0].split("|");
            for (var i = 0; i < rideOutHeadersArray.length; i++) {
              for (var j = 0; j < riderOutHeader.length; j++) {
                if (rideOutHeadersArray[i] == riderOutHeader[j]) {
                  flag = true;
                } else {
                  flag = false;
                }
              }
            }
            console.log("Inside End");
            if (flag == true) {
              console.log("inside flag");
              let destinationFile = sftp.createWriteStream(`DMS-Web3/Testing/archiveRideOut/${fileName}`);

              //creatig a temp file to fecth data

              fs.appendFile(`csvFolder/${fileName}`, dummyArray, () => {
                console.log("=============appendFile=======================");
                console.log(`csvFolder/${fileName}`);
                console.log("====================================");
                //sending file to archive folder sftp
                sftp.appendFile(`DMS-Web3/Testing/archiveRideOut/${fileName}`, dummyArray, function () {
                  //extract json from file and save it to temp collection

                  csv({ delimiter: "|" }).fromFile(`csvFolder/${fileName}`)
                    .then(jsonArray => {
                      res.send(`${jsonArray.length}`);
                      jsonArray.forEach(element => {
                        let newObj = {
                          rideCategory: "ride-outs",
                          rideName: element.Name,
                          startPoint: {
                            name: element.StartPoint,
                            latitude: parseFloat(element.RideSourceLatitude),
                            longitude: parseFloat(element.RideSourceLongitude)
                          },
                          endPoint: {
                            name: element.Destination,
                            latitude: parseFloat(element.RideDestinationLatitude),
                            longitude: parseFloat(element.RideDestinationLongitude)
                          },
                          rideStartDateIso: moment(element.StartDate, "YYYYMMDD").toISOString(),
                          rideEndDateIso: moment(element.EndDate, "YYYYMMDD").toISOString(),
                          RideId: parseInt(element.RideID),
                          createdOn: moment(element.DocDate, "YYYYMMDD").toISOString(),
                          startDate: moment(element.StartDate, "YYYYMMDD").format("DD-MM-YYYY"),
                          endDate: moment(element.EndDate, "YYYYMMDD").format("DD-MM-YYYY"),
                          terrain: element.TerrainType,
                          totalDistance: parseInt(element.TotalDistance),
                          geo: {
                            type: "Point",
                            coordinates: [
                              parseFloat(element.RideDestinationLongitude),
                              parseFloat(element.RideDestinationLatitude)
                            ]
                          },

                          //===================To Be Added TO Officle Ride Schema=====================

                          BranchCode: element.CompanyCode,
                          CountryCode: element.CountryCode,
                          CompanyName: element.CompanyName
                        };
                        finalData.push(newObj);
                      });

                      RideOutSchema.insertMany(finalData).then(uploaded => {
                        console.log("=============finalData=======================");
                        console.log(`${finalData.length}`);
                        console.log(`${uploaded.length}`);
                        // finalData = [];
                        console.log("================uploaded====================");
                        fs.unlink(`csvFolder/${fileName}`, function (err) {
                          if (err) {
                            connection.end();

                            console.log("Error, problem starting SFTP: %s", err);
                          } else {
                            connection.end();

                            console.log("Local file unlinked");
                          }
                        })
                      });

                      sourceFile.pipe(destinationFile);
                      destinationFile.on("finish", () => {
                        //================================Code to delete the file==============================

                        sftp.unlink(`DMS-Web3/Testing/RideList/${fileName}`, function (err) {
                          if (err) {
                            connection.end();

                            console.log("Error, problem starting SFTP: %s", err);
                          } else {
                            connection.end();

                            console.log("file unlinked");
                          }
                        }
                        );

                        //================================Code to delete the file==============================
                      });
                    });
                }
                );
              });
            } else {
              connection.end();
              let destinationFile = sftp.createWriteStream(`DMS-Web3/Testing/archiveRideOut/${fileName}`);
              sftp.appendFile(`DMS-Web3/Testing/archiveRideOut/${fileName}`, dummyArray, function () {
                sourceFile.pipe(destinationFile);
                destinationFile.on("finish", () => {
                  //================================Code to delete the file==============================

                  sftp.unlink(
                    `DMS-Web3/Testing/RideList/${fileName}`,
                    function (err) {
                      if (err) {
                        connection.end();

                        console.log("Error, problem starting SFTP: %s", err);
                      } else {
                        connection.end();

                        console.log("file unlinked");
                      }
                    }
                  );

                  //================================Code to delete the file==============================
                });
              }
              );
              console.log("====================================");
              console.log("File Headers Did`t Matched");
              console.log(rideOutHeadersArray);
              console.log(riderOutHeader);

              console.log(`${fileName}`);
              console.log("====================================");
            }
          });
        } else {
          connection.end();
          console.log("====================================");
          console.log("No Ride List File found");
          console.log("====================================");
        }
      });
    });
  });

  connection.on("error", err => {
    console.log(err);
  });

  connection.connect({
    // debug: function (s) {
    //     console.log(s)
    // },
    host: "ftpL.royalenfield.com",
    port: 22,
    username: "interface",
    password: "Interface@re",
    privateKey: fs.readFileSync("config/id_rsa.ppk"),
    passphrase: "Interface@re",
    readyTimeout: 99999
  });
}

function mapDealersToRodeOuts(req, res) {
  res.send("It`s Done");
  //   res.send('sadsad');
  //   Ride.find().then((rides) => {
  //     var foo = asyncc(function () {
  //       for (const item of rides) {
  //         awaitt(savingFunction(item))
  //       }
  //     })
  //     foo();
  //   });

  // }

  // var savingFunction = function (foundRide) {
  //   return new Promise((resolve, reject) => {

  //     Dealer.find({ BranchCode: foundRide.branchcode }).then((foundDealer) => {
  //       if (foundDealer) {
  //         foundDealer.rideOutId = foundRide._id
  //         foundDealer.save().then((savedDealer) => {
  //           foundRide.dealerId = savedDealer._id
  //           foundRide.save();
  //           resolve(foundRide);
  //         })
  //       }
  //       else {
  //         reject(foundRide);
  //       }

  //     })


  //   })

}

export default {
  connectSftpforbikescsv,
  bikeCsvToJson,
  dealerDataToDB,
  rideOutCsvToJson,
  mapDealersToRodeOuts
};
