import Forum from '../models/forums';
import ForumCategory from "../models/forumCategory";
import request from "request";
import config from '../../config/env';
import mongoose from 'mongoose';
import moment from "moment";
import User from '../models/user';

function timeStampToTime(date) {
    var result = moment(date).fromNow();
    var tempDate = result.split(" ");
    if (tempDate[1] === 'hour' || tempDate[1] === 'year' || tempDate[1] === 'days') {
        result = moment(date).format("DD MMM YYYY");
    }
    return result;
}

function incrementTopicViews(req, res, next) {
    Forum
        .findOneAndUpdate({ _id: req.body.forumId }, { $inc: { "views": 1 } }, { new: true })
        .then((updatedForumPost) => {
            ForumCategory.findOneAndUpdate({ categoryName: req.body.categoryName }, { $inc: { "totalViewsCount": 1 } }).then((doc) => {
                res.send(updatedForumPost.views.toString());
            }, (err) => {
                console.log(err);
            })
        }, (err) => {
            console.log(err);
        })
}

function incrementCategoryTopicsCounter(topicCategoryName) {
    ForumCategory
        .findOneAndUpdate({ categoryName: topicCategoryName }, { $inc: { "totalTopicsCount": 1 } })
        .then((doc) => {
            return true;
        }, (err) => {
            console.log(err);
        })
}

function getFourmDetail(req, res, next) {
    var comments = [];
    Forum
        .findOne({ _id: req.body.obj.pageId })
        .populate(
            {
                path: 'postedBy comment',
                populate: { path: 'userdetailscomments' }

            }).then((foundForum) => {
                foundForum.comment.forEach(comment => {
                    var singleComment = {
                        "commentText": comment.commentBody,
                        "userInfo": {
                            "profilePicture": {
                                "srcPath": comment.userdetailscomments.profilePicture,
                            },
                            "fullName": comment.userdetailscomments.fname + ' ' + comment.userdetailscomments.lname,
                        },
                        "commentTime": moment(comment.date).format("YYYY-MM-DD"),
                    }
                    comments.push(singleComment);
                });
                if (comments.length > 0) {
                    var returnObj =
                    {
                        topicViews: foundForum.views,
                        replyCount: foundForum.comment.length ? foundForum.comment.length : 0,
                        forumTopicId: foundForum._id,
                        heading: foundForum.forumTitle,
                        content: foundForum.forumBody,
                        categoryName: foundForum.categoryName,
                        postedOn: timeStampToTime(foundForum.postedOn),
                        postedByUser: {
                            fullName: foundForum.postedBy.fname + " " + foundForum.postedBy.lname,
                            profilePicture: { srcPath: foundForum.postedBy.profilePicture }
                        },
                        comments: comments,
                    };
                } else {
                    var returnObj =
                    {
                        topicViews: foundForum.views,
                        replyCount: foundForum.comment.length ? foundForum.comment.length : 0,
                        forumTopicId: foundForum._id.toString(),
                        heading: foundForum.forumTitle,
                        content: foundForum.forumBody,
                        categoryName: foundForum.categoryName,
                        postedOn: timeStampToTime(foundForum.postedOn),
                        postedByUser: {
                            fullName: foundForum.postedBy.fname + " " + foundForum.postedBy.lname,
                            profilePicture: { srcPath: foundForum.postedBy.profilePicture },
                            userUrl: foundForum.postedBy.userUrl,
                        }
                    };
                }
                res.send(returnObj);
            }, (err) => {
                res.send(err);
            });
}

function update(req, res, next) {
    var responseObj = {};
    Forum
        .findOne({ _id: req.body.id })
        .then((foundDoc) => {
            foundDoc.forumTitle = req.body.forumTitle ? req.body.forumTitle : foundDoc.forumTitle,
                foundDoc.forumBody = req.body.forumBody ? req.body.forumBody : foundDoc.forumBody,
                foundDoc.categoryName = req.body.categoryName ? req.body.categoryName : foundDoc.categoryName
        })
        .catch((err) => {
            console.log(err);
        })
    Forum
        .saveAsync()
        .then((updatedForum) => {
            res.send(updatedForum);
        })
        .catch((err) => {
            responseObj.success = false;
            responseObj.message = "error updating";
            responseObj.data = err
        })
}


function create(req, res, next) {
    var country = req.headers['x-custom-country'];
    var language = req.headers['x-custom-language'];
    const returnObj = {
        success: true,
        message: '',
        data: {},
    };
    const forum = new Forum({
        forumTitle: req.body.title,
        forumBody: req.body.Content,
        categoryName: req.body.category,
        postedOn: Date.now(),
        postedBy: req.body.postedBy,
        forumAuthor: req.body.postedByUserName,
        locale : {country : country,
        language : language}
    });
    forum
        .saveAsync()
        .then((savedPost) => {
	console.log(savedPost)
            User
                .findOne({ _id: req.body.postedBy })
                .then((foundUser) => {
                    foundUser.discussionJoined.push(savedPost._id);
                    foundUser.save();
                })
            incrementCategoryTopicsCounter(req.body.category);
            var username = 'admin';
            var password = 'admin';
            var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");

            request.post({
                headers: {
                    'content-type': 'application/x-www-form-urlencoded',
                    "Authorization": auth
                },
                url: `${config.aemAuthorUrl}/bin/createPage`,
                form: {
                    entity: 'forum-topic',
                    pageId: savedPost._id.toString(),
                    title: savedPost.forumTitle,
                    category: savedPost.categoryName,
                    country : req.headers['x-custom-country'],
                    language: req.headers['x-custom-language'],
                    pageProperties: JSON.stringify({
                        "author": req.body.postedByUserName,
                        "category": savedPost.categoryName,
                        "summary": req.body.Content,
                        "postedBy": req.body.postedBy,
                        "views": savedPost.views,
                        "replyCount": savedPost.comment.length,
                        "postedOn": moment(savedPost.postedOn).format("DD MMM YYYY"),
                        "dateSort": moment().toISOString()
                    })
                }
            }, function (error, response, data) {
		console.log(response.body)
                var data = JSON.parse(data);
                if (error) {
                    returnObj.data.forumPost = null;
                    returnObj.message = 'Error Creating Post';
                    returnObj.success = false;
                    res.send(returnObj);
                } else {
                    Forum
                        .findOneAndUpdate(
                            { _id: savedPost._id },
                            { $set: { forumUrl: data.pagePath } }
                        )
                        .then((updatedForumPost) => {
                            returnObj.data.forumPost = updatedForumPost;
                            returnObj.data.forumPost.forumUrl = data.pagePath;
                            returnObj.message = 'Post created successfully';
                            res.send(returnObj);
                        })
                }
            });
        })
        .error(e => next(e));
}


function getForumPosts(req, res, next) {
    Forum
        .getForumPosts(parseInt(req.body.skip), parseInt(req.body.limit))
        .then((posts) => {
            const returnObj = {
                success: true,
                message: '',
                data: {},
            };
            returnObj.data.post = posts;
            returnObj.message = 'Forum-Posts retrieved successfully';
            res.send(returnObj);
        })
        .error((e) => next(e));
}

export default {
    getFourmDetail,
    getForumPosts,
    create,
    incrementTopicViews,
};


// node name is sent in summary listing
