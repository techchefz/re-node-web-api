import httpStatus from 'http-status';
import jwt from 'jsonwebtoken';
import APIError from '../helpers/APIError';
import config from '../../config/env';
import UserSchema from '../models/user';

const crypto = require('crypto');
const { OAuth2Client } = require('google-auth-library');
var google = require('googleapis');
var request = require('request');
var twitterAPI = require('node-twitter-api');

/**
 * Returns jwt token  and user object if valid email and password is provided
 * @param req (email, password, userType)
 * @param res
 * @param next
 * @returns {jwtAccessToken, user}
 */
function login(req, res, next) {
  var retObj = {
    success: true,
    message: '',
    data: {},
  };
  const userObj = {
    email: req.body.email,
    userType: req.body.userType,
  };
  const phoneObj = {
    phoneNo: req.body.email,
    userType: req.body.userType,
  };

  UserSchema
    .findOneAsync({ $or: [userObj, phoneObj] }, '+password')
    .then((user) => {
      //eslint-disable-line
      if (!user) {
        const err = new APIError('User not found with the given email id', httpStatus.NOT_FOUND);
        return next(err);
      } else {
        user.comparePassword(req.body.password, (passwordError, isMatch) => {
          //eslint-disable-line
          if (passwordError || !isMatch) {
            retObj.success = false;
            retObj.message = 'Incorrect Password';
            res.json(retObj);
          } else {
            user.loginStatus = true;
            const token = jwt.sign(user, config.jwtSecret);
            UserSchema.findOneAndUpdateAsync({ _id: user._id }, { $set: user }, { new: true }) //eslint-disable-line
              .then((updatedUser) => {
                var userObj = { email: updatedUser.email, profilePicture: { srcPath: updatedUser.profilePicture }, userId: updatedUser._id, phone: updatedUser.phoneNo, firstName: updatedUser.fname, lastName: updatedUser.lname };
                retObj.success = true;
                retObj.message = 'user successfully logged in';
                retObj.data.jwtAccessToken = `JWT ${token}`;
                retObj.data.user = userObj;
                res.json(retObj);
              })
              .catch((error) => {
                const err = new APIError(`error in updating user details while login ${error}`, httpStatus.INTERNAL_SERVER_ERROR);
                next(err);
              });
          }
        });

      }
    })
    .catch((e) => {
      console.log(e);
      const err = new APIError(`erro while finding user ${e}`, httpStatus.INTERNAL_SERVER_ERROR);
      next(err);
    });
}
/** This is a protected route. Change login status to false and send success message.
 * @param req
 * @param res
 * @param next
 * @returns success message
 */

function checkUser(req, res) {
  UserSchema.findOneAsync({ email: req.body.email })
    .then((foundUser) => {
      if (foundUser !== null) {
        const jwtAccessToken = jwt.sign(foundUser, config.jwtSecret);
        const returnObj = {
          success: true,
          message: 'User Exist',
          data: {},
        };
        returnObj.data = {
          user: foundUser,
          jwtAccessToken: `JWT ${jwtAccessToken}`,
        };
        return res.send(returnObj);
      } else {
        const returnObj = {
          success: true,
          message: 'New User',
        };
        return res.send(returnObj);
      }
    })
    .catch((error) => {
    });
}



function facebookSignup(req, res, next) {
  var retObj = {
    status: false,
    userObj: null,
    message: null
  };

  var accessToken = req.body.AccessToken;
  var clientSecret = config.facebookclientsecret;
  var appsecret_proof = crypto.createHmac('sha256', clientSecret).update(accessToken).digest('hex');
  var verifyUrl = `https://graph.facebook.com/v2.12/me?fields=id,email,first_name,last_name,picture,gender&access_token=${accessToken}&appsecret_proof=${appsecret_proof}`;
  request(verifyUrl, (err, response, body) => {
    var responseBody = JSON.parse(body);
    UserSchema.findOne({ email: responseBody.email }) //eslint-disable-line
      // eslint-disable-next-line
      .then((UserObj) => {
        if (UserObj == null) {
          retObj.status = true,
            retObj.userObj = responseBody,
            retObj.message = null;
          res.send(retObj);
        } else if (UserObj.email === responseBody.email) {
          retObj.status = false;
          retObj.userObj = null;
          retObj.message = "User Already Exists !";
          res.send(retObj);
        } else {
          retObj.status = false;
          retObj.userObj = null;
          retObj.message = "Error Creating User, Please Try again !";
        }
      }, (err) => {
        console.log(err);
      })
  });
}


function googleSignup(req, res, next) {
  var retObj = {
    status: false,
    userObj: null,
    message: null
  };
  const ClientId = config.googleclientid;
  const ClientSecret = config.googleclientsecret;
  function getOAuthClient() {
    return new OAuth2Client(ClientId, ClientSecret, config.googleredirecturi);
  }

  var oauth2Client = getOAuthClient();
  var code = req.body.authCode;
  oauth2Client.getToken(code, function (err, tokens) {
    // Now tokens contains an access_token and an optional refresh_token. Save them.
    if (!err) {
      oauth2Client.setCredentials(tokens);
      var token = tokens.id_token;
      var verifyUrl = `https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=${token}`;
      request(verifyUrl, (err, response, body) => {
        var responseBody = JSON.parse(body);
        UserSchema.findOne({ email: responseBody.email }) //eslint-disable-line
          // eslint-disable-next-line
          .then((UserObj) => {
            if (UserObj == null) {
              retObj.status = true,
                retObj.userObj = responseBody,
                retObj.message = null;
              res.send(retObj);
            } else if (UserObj.email === responseBody.email) {
              retObj.status = false;
              retObj.userObj = null;
              retObj.message = "User Already Exists !";
              res.send(retObj);
            } else {
              retObj.status = false;
              retObj.userObj = null;
              retObj.message = "Error Creating User, Please Try again !";
              res.send(retObj);
            }
          }, (err) => {
            console.log(err);
          })
      });
    }
    else {
      console.log(err);
    }
  });
}

function facebookLogin(req, res, next) {
  var retObj = {
    status: true,
    message: '',
    data: {},
  };
  var accessToken = req.body.obj.accessToken;
  var clientSecret = config.facebookclientsecret;
  var appsecret_proof = crypto.createHmac('sha256', clientSecret).update(accessToken).digest('hex');
  var verifyUrl = `https://graph.facebook.com/v2.12/me?fields=id,email,first_name,last_name,picture,gender&access_token=${accessToken}&appsecret_proof=${appsecret_proof}`;
  request(verifyUrl, (err, response, body) => {
    var responseBody = JSON.parse(body);
    UserSchema.findOne({ email: responseBody.email }) //eslint-disable-line
      // eslint-disable-next-line
      .then((UserObj) => {
        if (UserObj == null) {
          retObj.status = false;
          retObj.user = null;
          retObj.message = "User Doesn't Exists !";
          res.send(retObj);
        } else if (UserObj.email === responseBody.email) {
          retObj.status = true;
          retObj.message = "Logged in Successfully";
          const jwtAccessToken = jwt.sign(UserObj, config.jwtSecret);
          retObj.data.jwtAccessToken = `JWT ${jwtAccessToken}`;
          var obj = { email: UserObj.email, profilePicture: { srcPath: UserObj.profilePicture }, userId: UserObj._id, phone: UserObj.phoneNo, firstName: UserObj.fname, lastName: UserObj.lname };
          retObj.data.user = obj;
          res.send(retObj);
        } else {
          retObj.status = false;
          retObj.user = null;
          retObj.message = "Error !, Please Try again !";
          res.send(retObj);
        }
      }, (err) => {
        console.log(err);
      })
  });
}


function googleLogin(req, res, next) {
  var retObj = {
    success: true,
    message: '',
    data: {},
  };

  const ClientId = config.googleclientid;
  const ClientSecret = config.googleclientsecret;
  function getOAuthClient() {
    return new OAuth2Client(ClientId, ClientSecret, config.googleredirecturi);

  }
  var oauth2Client = getOAuthClient();
  var code = req.body.obj.authCode;
  oauth2Client.getToken(code, function (err, tokens) {
    // Now tokens contains an access_token and an optional refresh_token. Save them.
    if (!err) {
      oauth2Client.setCredentials(tokens);
      var token = tokens.id_token;
      var verifyUrl = `https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=${token}`;
      request(verifyUrl, (err, response, body) => {
        var responseBody = JSON.parse(body);
        UserSchema.findOne({ email: responseBody.email }) //eslint-disable-line
          // eslint-disable-next-line
          .then((UserObj) => {
            if (UserObj == null) {
              retObj.status = false;
              retObj.user = null;
              retObj.message = "User Doesn't Exists !";
              res.send(retObj);
            } else if (UserObj.email === responseBody.email) {
              retObj.status = true;
              retObj.message = "Logged in Successfully";
              const jwtAccessToken = jwt.sign(UserObj, config.jwtSecret);
              retObj.data.jwtAccessToken = `JWT ${jwtAccessToken}`;
              var obj = { email: UserObj.email, profilePicture: { srcPath: UserObj.profilePicture }, userId: UserObj._id, phone: UserObj.phoneNo, firstName: UserObj.fname, lastName: UserObj.lname };
              retObj.data.user = obj;
              res.send(retObj);
            } else {
              retObj.status = false;
              retObj.user = null;
              retObj.message = "Error !, Please Try again !";
              res.send(retObj);
            }
          }, (err) => {
            console.log(err);
          })
      });
    }
    else {
      console.log(err);
    }
  });
}


var twitter = new twitterAPI({
  //Kunal
  consumerKey: config.twitterconfig.consumerKey,
  consumerSecret: config.twitterconfig.consumerSecret,
  callback: config.twitterconfig.callbacksignup

  //Royal-Enfield
  // consumerKey: 'mlG280kRbMSmmtWVoFmQCA4Yz',
  // consumerSecret: 'NH7rj8Z6IkA7fCFqSqqshI0Q5GZXwdzafmMd1aoag2n5B1svQ9',
  // callback: 'http://127.0.0.1/twitter/callback.html'
});
var _requestSecret;
function getTwitterRequestToken(req, res, next) {
  twitter.getRequestToken(function (error, requestToken, requestTokenSecret, results) {
    if (error) {
      console.log("Error getting OAuth request token : " + error);
      res.send(error);
    } else {
      _requestSecret = requestTokenSecret;
      res.redirect("https://api.twitter.com/oauth/authenticate?oauth_token=" + requestToken);
      //store token and tokenSecret somewhere, you'll need them later; redirect user
    }
  });
}

function getTwitterAccessToken(req, res, next) {
  var token = req.query.oauth_token;
  var verifier = req.query.oauth_verifier;
  twitter.getAccessToken(token, _requestSecret, verifier, function (error, accessToken, accessTokenSecret, data) {
    if (error) {
      console.log(error);
    } else {
      //store accessToken and accessTokenSecret somewhere (associated to the user)
      //Step 4: Verify Credentials belongs here
      twitter.verifyCredentials(accessToken, accessTokenSecret, { include_email: true, skip_status: true }, function (error, user, response) {
        if (error) {
          //something was wrong with either accessToken or accessTokenSecret
          //start over with Step 1
          console.log(error);
        } else {
          UserSchema.findOne({ email: user.email }) //eslint-disable-line
            // eslint-disable-next-line
            .then((UserObj) => {
              var retObj = {};
              if (UserObj == null) {
                retObj.status = true,
                  retObj.userObj = user,
                  retObj.message = null;
                res.send(user);
              } else if (UserObj.email === user.email) {
                retObj.status = false;
                retObj.userObj = null;
                retObj.message = "User Already Exists !";
                res.send(retObj);
              } else {
                retObj.status = false;
                retObj.userObj = null;
                retObj.message = "Error Creating User, Please Try again !";
              }
            }, (err) => {
              console.log(err);
            })

          //accessToken and accessTokenSecret can now be used to make api-calls (not yet implemented)
          //data contains the user-data described in the official Twitter-API-docs
          //you could e.g. display his screen_name
          // res.send(user)
        }
      });
    }
  });
}

var login1 = 'login';
var twitter2 = new twitterAPI({
  consumerKey: config.twitterconfig.consumerKey,
  consumerSecret: config.twitterconfig.consumerSecret,
  callback: config.twitterconfig.callbackLogin
});
var _requestSecret1;


function getTwitterRequestTokenLogIn(req, res, next) {
  twitter2.getRequestToken(function (error, requestToken, requestTokenSecret, results) {
    if (error) {
      console.log("Error getting OAuth request token : " + error);
      res.send(error);
    } else {
      _requestSecret1 = requestTokenSecret;
      res.redirect("https://api.twitter.com/oauth/authenticate?oauth_token=" + requestToken);
      //store token and tokenSecret somewhere, you'll need them later; redirect user
    }
  });
}

function getTwitterAccessTokenLogIn(req, res, next) {
  var token = req.query.oauth_token;
  var verifier = req.query.oauth_verifier;
  twitter2.getAccessToken(token, _requestSecret1, verifier, function (error, accessToken, accessTokenSecret, data) {
    if (error) {
      console.log(error);
    } else {
      //store accessToken and accessTokenSecret somewhere (associated to the user)
      //Step 4: Verify Credentials belongs here
      twitter2.verifyCredentials(accessToken, accessTokenSecret, { include_email: true, skip_status: true }, function (error, user, response) {
        if (error) {
          //something was wrong with either accessToken or accessTokenSecret
          //start over with Step 1
          console.log("Verification Error");
          console.log(error);
        } else {
          UserSchema.findOne({ email: user.email }) //eslint-disable-line
            // eslint-disable-next-line
            .then((UserObj) => {
              var retObj = {};
              retObj.data = {};
              if (UserObj == null) {
                retObj.status = false;
                retObj.user = null;
                retObj.message = "User Doesn't Exists !";
                res.send(retObj);
              } else if (UserObj.email === user.email) {
                console.log('sda');
                retObj.status = true;
                retObj.message = "Logged in Successfully";
                const jwtAccessToken = jwt.sign(UserObj, config.jwtSecret);
                retObj.data.jwtAccessToken = `JWT ${jwtAccessToken}`;
                var obj = { email: UserObj.email, profilePicture: { srcPath: UserObj.profilePicture }, userId: UserObj._id, phone: UserObj.phoneNo, firstName: UserObj.fname, lastName: UserObj.lname };
                retObj.data.user = obj;
                res.send(retObj);
              } else {
                retObj.status = false;
                retObj.user = null;
                retObj.message = "Error !, Please Try again !";
                res.send(retObj);
              }
            }, (err) => {
              console.log(err);
            })
        }
      });
    }
  });
}

export default
  {
    getTwitterRequestTokenLogIn,
    getTwitterAccessTokenLogIn,
    login,
    checkUser,
    facebookSignup,
    googleSignup,
    googleLogin,
    facebookLogin,
    getTwitterRequestToken,
    getTwitterAccessToken
  };

