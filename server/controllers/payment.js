import http from "http";
import fs from "fs";
import qs from "querystring";
import crypto from "crypto";

function ccavRequestHandler(req, res, next) {
console.log("In Request Handler");   
console.log(req.body); 
var body = '',
        workingKey = '100C402E4A9AC6A06B187B9D9E2A9A53',	//Put in the 32-Bit key shared by CCAvenues.
        accessCode = 'AVIE01FC67BO11EIOB',			//Put in the Access Code shared by CCAvenues.
        encRequest = '',
        formbody = '';

      
        body = "merchant_id=10292&order_id=6598765432&currency=INR&amount=1.00&redirect_url=http%3A%2F%2Fdev3.royalenfield.com%2Fnode%2Fapi%2FccavResponseHandler&cancel_url=http%3A%2Fdev3.royalenfield.com%2Fnode%2Fapi%2FccavResponseHandler&language=EN&billing_name=Peter&billing_address=Santacruz&billing_city=Mumbai&billing_state=MH&billing_zip=400054&billing_country=India&billing_tel=9876543210&billing_email=testing%40domain.com&delivery_name=Sam&delivery_address=Vile+Parle&delivery_city=Mumbai&delivery_state=Maharashtra&delivery_zip=400038&delivery_country=India&delivery_tel=0123456789&merchant_param1=additional+Info.&merchant_param2=additional+Info.&merchant_param3=additional+Info.&merchant_param4=additional+Info.&merchant_param5=additional+Info.&promo_code=&customer_identifier=";
        encRequest = encrypt(body, workingKey);
        formbody = '<form id="nonseamless" method="post" name="redirect" action="https://test.ccavenue.com/transaction/transaction.do?command=initiateTransaction"/> <input type="hidden" id="encRequest" name="encRequest" value="' + encRequest + '"><input type="hidden" name="access_code" id="access_code" value="' + accessCode + '"><script language="javascript">document.redirect.submit();</script></form>';
console.log(formbody);
        res.writeHeader(200, { "Content-Type": "text/html" });
        res.write(formbody);
        res.end();

    return;
}

function ccavResponseHandler(req, res, next) {
console.log("In Response Handler");    
var ccavEncResponse = '',
        ccavResponse = '',
        workingKey = '100C402E4A9AC6A06B187B9D9E2A9A53',	//Put in the 32-Bit key shared by CCAvenues.
        ccavPOST = '';

        ccavEncResponse += req.data;
        ccavPOST = qs.parse(ccavEncResponse);
        var encryption = ccavPOST.encResp;
        ccavResponse = decrypt(encryption, workingKey);

        var pData = '';
        pData = '<table border=1 cellspacing=2 cellpadding=2><tr><td>'
        pData = pData + ccavResponse.replace(/=/gi, '</td><td>')
        pData = pData.replace(/&/gi, '</td></tr><tr><td>')
        pData = pData + '</td></tr></table>'
        htmlcode = '<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><title>Response Handler</title></head><body><center><font size="4" color="blue"><b>Response Page</b></font><br>' + pData + '</center><br></body></html>';
        response.writeHeader(200, { "Content-Type": "text/html" });
        res.write(htmlcode);
        res.end();
}

function encrypt(plainText, workingKey) {
    var m = crypto.createHash('md5');
    m.update(workingKey);
    var key = m.digest('binary');
    var iv = '\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f';
    var keyBuffer = new Buffer(key, 'binary');
    var cipher = crypto.createCipheriv('aes-128-cbc', keyBuffer, iv);
    var encoded = cipher.update(plainText, 'utf8', 'hex');
    encoded += cipher.final('hex');
    return encoded;
}

function decrypt(encText, workingKey) {
    var m = crypto.createHash('md5');
    m.update(workingKey)
    var key = m.digest('binary');
    var iv = '\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f';
    var keyBuffer = new Buffer(key, 'binary');
    var decipher = crypto.createDecipheriv('aes-128-cbc', keyBuffer, iv);
    var decoded = decipher.update(encText, 'hex', 'utf8');
    decoded += decipher.final('utf8');
    return decoded;
}

export default { ccavRequestHandler, ccavResponseHandler };
