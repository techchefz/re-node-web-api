import OwnersManual from '../models/owners-manual';

function create(req, res, next) {

    const ownersManual = new OwnersManual({

        name: req.body.name,
        email: req.body.email,
        phone: req.body.phone,
        city: req.body.city

    });

    ownersManual
        .saveAsync()
        .then((doc) => {
            res.send('saved successfully');
        })
        .catch(err => res.send(err));
}

export default {
    create
}