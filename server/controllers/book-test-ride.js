import BookTestRide from '../models/book-test-ride';
import BookTestRidefunc from '../service/dmsApi';
import queryString from 'query-string';
import request from 'request';

function create(req, res, next) {
    const bookTestRideVar = new BookTestRide({
        fName: req.body.fName,
        lName: req.body.lName,
        email: req.body.email,
        bikeName: req.body.bikeName,
        countryName: req.body.countryName,
        stateName: req.body.stateName,
        cityName: req.body.cityName,
        dealerName: req.body.dealerName,
        dealerCode: req.body.dealerCode,
        Date: req.body.Date,
        mobile: req.body.mobile,
        buyPlanDate: req.body.buyPlanDate,
        time: req.body.time
    });
    bookTestRideVar
        .saveAsync()
        .then((savedRide) => {
            var testRideDatavar = {
                'Name': savedRide.fName + savedRide.lName,
                'mobile': savedRide.mobile,
                'EMail': savedRide.email,
                'Country': 'india',
                'State': savedRide.stateName,
                'City': savedRide.cityName,
                'Motorcycle': savedRide.bikeName,
                'Dealer_Name': savedRide.dealerName,
                'When_do_plan_to_buy': savedRide.buyPlanDate,
                'Web_link': 'https://royalenfield.com',
                'Tocken': 'ayz4EbIZLT403KVQ7dffhJ1Ffzv0yiSui1i1VXmAssesLjYODiy5dHIdfd2W11Q9QH6oHTuTWyIi7t5GsfGYk4qWisQdAP1X',
                'Dealer_Code': savedRide.dealerCode,
                'UTM': '22feet',
                'Test_Ride_Date': savedRide.Date,
                'Test_Ride_Time': savedRide.time,
            };

            var axiosConfig = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                }
            };

            var testRideOptions = {
                url: 'http://crc.cequitycti.com/RoyalEnfield/service.asmx/Organic',
                method: 'POST',
                headers: axiosConfig.headers,
                body: queryString.stringify(testRideDatavar)
            }

            request(testRideOptions, function (error, response, body) {
                if (!error && response.statusCode == 200) {

                    var result = response.statusCode;
                    res.send(result);

                } else {
                    reject(err);
                }

            })

        })

};

export default {
    create
}


