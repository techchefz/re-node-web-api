import Bike from '../models/bike';
import User from '../models/user';
import asyncc from 'asyncawait/async';
import awaitt from 'asyncawait/await';
import BikecsvSchema from '../models/bikeCsv';

function get(req, res) {
    return res.send({ success: true, message: 'bike found', data: req.bike });
}

function create(req, res, next) {
    Bike.findOneAsync({
        $and: [{ bikeName: req.body.bikeName }, { bikeCategory: req.body.bikeCategory }]
    }).then((foundBike) => {
        if (foundBike !== null) {
            const returnObj = {
                success: true,
                message: '',
                data: {},
            };
            returnObj.message = 'bike already exist';
            returnObj.success = false;
            return res.send(returnObj);
        } else {
            const bike = new Bike({
                bikeModel: req.body.bikeModel,
                bikeName: req.body.bikeName,
                bikeCategory: req.body.bikeCategory,
            });
            bike
                .saveAsync()
                .then((savedBike) => {
                    const returnObj = {
                        success: true,
                        message: '',
                        data: {},
                    };
                    returnObj.data.bike = savedBike;
                    returnObj.message = 'bike created successfully';
                    res.send(returnObj);
                })
                .error(e => next(e));
        }
    });
}

function register(req, res, next) {
    res.send("inside bike");
    var bikeId = req.body.obj.motorcycleId.split('_')[1];
    var familyCode = req.body.obj.motorcycleFamilyCode;
    var modelCodes = req.body.obj.motorcycleModelCodes;
    var modelArr = [];
    var responseObj = {};
    var familyName;
    var status;
    for (var i = 0; i < modelCodes.length; i++) {
        BikecsvSchema
            .findOne({ bikeModelCode: modelCodes[i] })
            .then((foundCsvBike) => {
                modelArr.push({
                    bikeModelId: JSON.parse(foundCsvBike.bikeModelCode),
                    bikeModelName: foundCsvBike.bikeName
                })
            }).catch((err) => {
                console.log(err);
            })

    }
    BikecsvSchema
        .findOne({ familyCode: familyCode })
        .then((foundCsvBike) => {
            familyName = foundCsvBike.familyName;
            status = foundCsvBike.bikeStatus;
        })

    Bike
        .findOne({ _id: bikeId })
        .then((foundBike) => {
            foundBike.familyCode = JSON.parse(familyCode);
            foundBike.status = status;
            foundBike.model = modelArr
            foundBike.familyName = familyName
            foundBike
                .save()
                .then((savedBike) => {
                    responseObj.success = true;
                    responseObj.message = "Bike created successfully";
                    responseObj.data = savedBike
                })
                .catch((err) => {
                    responseObj.success = false;
                    responseObj.message = "error creating Bike";
                    responseObj.data = err;
                })

        })
}

function update(req, res, next) {
    //code to update bike details goes here ..
    Bike.findOne({ _id: req.body._id }).then((foundBike) => {
        foundBike.bikeModel = req.body.bikeModel ? req.body.bikeModel : foundBike.bikeModel,
            foundBike.bikeName = req.body.bikeName ? req.body.bikeName : foundBike.bikeName,
            foundBike.bikeCategory = req.body.bikeCategory ? req.body.bikeCategory : foundBike.bikeCategory
    });
    bike
        .saveAsync()
        .then((updatedBike) => {
            const returnObj = {
                success: true,
                message: '',
                data: {},
            };
            returnObj.data.bike = updatedBike;
            returnObj.message = 'bike updated successfully';
            res.send(returnObj);
        })
        .error(e => next(e));
}


function remove(req, res, next) {
    // code to remove bike goes here..
    Bike
        .remove({ bikeName: req.body.bikeName })
        .then((removedBike) => {
            const returnObj = {
                success: true,
                message: '',
                data: {},
            };
            returnObj.data.bike = removedBike;
            returnObj.message = 'bike removed successfully';
            res.send(returnObj);
        })
        .error(e => next(e));
}

function addMotorcycle(req, res, next) {
    const returnObj = {
        success: true,
        message: '',
        data: {},
    };

    var bikeId = req.body.bikeId.split("_")[1];
    var userId = req.body.userId;
    User.findOne({ _id: userId }).then((foundUser) => {
        foundUser.ownedBikeInfo
            .push({ bikeId: bikeId, vehicleNo: req.body.vehicleNo });
        foundUser.save().then((savedUser) => {
            returnObj.message = "bike added Successfully";
            res.status(200).send(returnObj);
        }).catch((err) => {
            returnObj.success = false;
            returnObj.message = "error adding motorcycle";
            res.status(501).send(returnObj);
        })
    })
};

function getMotorcycleModelCodes(req, res) {
    var obj1 = {};
    BikecsvSchema
        .find()
        .then((doc) => {
            for (var i = 0; i < doc.length; i++) {
                obj1[doc[i].bikeModelCode] = doc[i].bikeName;
            }
            res.send(obj1)
        })
        .catch((err) => { console.log(err) })
}

function getMotorcycleFamilyCodes(req, res) {
    var obj1 = {};

    BikecsvSchema
        .distinct("familyCode")
        .then((distinct) => {
            var foo = asyncc(function () {
                for (const item of distinct) {
                    var arr = awaitt(savingFunction(item));
                    awaitt(matchingFunction(arr));
                }
                res.send(obj1);
            })
            foo();
        });

    var savingFunction = ((distinct) => {
        return new Promise((resolve, reject) => {
            BikecsvSchema.aggregate([
                { $match: { familyCode: distinct } },
            ]).then((match) => {
                resolve(match);
            }).catch((err) => {
                console.log(err);
            })
        });
    })
    var matchingFunction = ((match) => {
        return new Promise((resolve, reject) => {
            var fun = asyncc(function () {

                for (const item of match) {
                    awaitt(() => {
                        return new Promise((resolve, reject) => {
                            obj1[item.familyCode] = item.familyName;
                            resolve(obj1);
                        });
                    })
                }
            })
            fun();
            resolve(obj1);

        });
    })

}

export default {
    get,
    create,
    update,
    remove,
    addMotorcycle,
    getMotorcycleModelCodes,
    getMotorcycleFamilyCodes,
    register
};

