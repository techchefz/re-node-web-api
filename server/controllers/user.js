//import bcrypt from 'bcrypt';
import httpStatus from "http-status";
import formidable from "formidable";
import jwt from "jsonwebtoken";
import APIError from "../helpers/APIError";
import config from "../../config/env";
import User from "../models/user";
import sendEmail from "../service/emailApi";
import SendSms from "../service/smsApi";
import axios from "axios";
import request from "request";
import moment from "moment";
import base64 from "base64-img";
import nodemailer from "nodemailer";
import path from "path";
import TripStory from "../models/tripStory";
var geoip = require("geoip-lite");

function testingroute(req, res, next) {
  console.log(req.headers["x-custom-language"]);
  console.log(req.headers);
  res.send(req.headers);
  var ip;
  if (req.headers["x-forwarded-for"]) {
    console.log("1");
    ip = req.headers["x-forwarded-for"].split(",")[0];
  } else if (req.connection && req.connection.remoteAddress) {
    console.log("2");
    console.log(req.connection);
    ip = req.connection.remoteAddress;
  } else {
    console.log("3");
    ip = req.ip;
  }
  console.log("client IP is *********************" + ip);
  console.log(ip);
  var geo = geoip.lookup(ip);
  console.log(geo);
  console.log("========================================================");
}

/**
 * Create new user
 * @property {string} req.body.username - The username of user.
 * @property {string} req.body.mobileNumber - The mobileNumber of user.
 * @returns {User}
 */

function create(req, res, next) {
  // returnObj defination
  const returnObj = {
    success: true,
    message: "",
    data: {}
  };

  // find User if exists and update
  User.findOneAsync({
    $or: [
      { $and: [{ email: req.body.email }, { phoneNo: req.body.phoneNo }] },
      { $or: [{ email: req.body.email }, { phoneNo: req.body.phoneNo }] }
    ]
  })
    .then(foundUser => {
      if (foundUser !== null && foundUser.userType === req.body.userType) {
        User.findOneAndUpdateAsync(
          { _id: foundUser._id },
          { $set: { loginStatus: true } },
          { new: true }
        ).then(updateUserObj => {
          if (updateUserObj) {
            const jwtAccessToken = jwt.sign(updateUserObj, config.jwtSecret);
            returnObj.message = "User Already Exists !";
            returnObj.success = false;
            return res.send(returnObj);
          }
        }).error(e => {
          const err = new APIError(
            `error in updating user details while login ${e}`,
            httpStatus.INTERNAL_SERVER_ERROR
          );
          next(err);
        });
      } else {
        const otpValue = Math.floor(100000 + Math.random() * 900000); //eslint-disable-line
        const emailToken = Math.floor(700000000000 + Math.random() * 900000); //eslint-disable-line
        var bikeId = req.body.bikeOwned
          ? [req.body.bikeOwned[0].split("_")[1]]
          : [];
        const user = new User({
          email: req.body.email,
          password: req.body.password,
          userType: req.body.userType,
          fname: req.body.fname,
          lname: req.body.lname,
          bikeOwned: bikeId,
          ownBike: req.body.moto,
          phoneNo: req.body.phoneNo,
          otp: otpValue,
          locale: {
            language: req.headers['x-custom-language'],
            country: req.headers['x-custom-country']
          },
          emailToken: emailToken,
          loginStatus: true,
          gender: req.body.gender,
          addressInfo: {
            city: req.body.city
          },
          dob: req.body.dob
        });
        user
          .saveAsync()
          .then(savedUser => {
            var username = "admin";
            var password = "admin";
            var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
            request.post(
              {
                headers: {
                  "content-type": "application/x-www-form-urlencoded",
                  Authorization: auth
                },
                url: `${config.aemAuthorUrl}/bin/createPage`,
                form: {
                  entity: "user",
                  pageId: savedUser._id.toString(),
                  title: savedUser.fname.toString(),
                  country: savedUser.locale.country,
                  language: savedUser.locale.language,
                }
              },
              function (error, response, dataReturned) {
                var data = JSON.parse(dataReturned);
                if (error) {
		console.log("==============error=================")
		console.log(error)
		console.log("==============error=================")
                  returnObj.data.story = null;
                  returnObj.message = "Error Creating User";
                  returnObj.success = false;
                  res.send(returnObj);
                } else {
		console.log("================response===================")
		console.log(response.body)
		 console.log("================response===================")
                  const returnObj = {
                    success: true,
                    message: "",
                    data: {}
                  };
                  User.findOne({ _id: savedUser._id })
                    .then(updatedUser => {
                      updatedUser.userUrl = data.pagePath;
                      updatedUser.save();
                      const jwtAccessToken = jwt.sign(
                        savedUser,
                        config.jwtSecret
                      );
                      returnObj.data.jwtAccessToken = `JWT ${jwtAccessToken}`;
                      var obj = {
                        email: savedUser.email,
                        profilePicture: { srcPath: savedUser.profilePicture },
                        userId: savedUser._id,
                        phone: savedUser.phoneNo,
                        firstName: savedUser.fname,
                        lastName: savedUser.lname
                      };
                      returnObj.data.user = obj;
                      returnObj.message = "user created successfully";
                      res.send(returnObj);
                    })
                    .catch(e => console.log(e));
                }
              }
            );
          })
          .error(e => next(e));
      }
    })
    .catch(e => console.log(e));
}

function sendSms1(smsText, phoneNo) {
  var num = phoneNo.slice(-10);
  axios
    .get(
      "http://www.sms.wstechno.com/ComposeSMS.aspx?username=Isetsol-170841&password=Wsc@4141@&sender=WSTECH&&to=" +
      num +
      "&message=Please Use this OTP for resetting the password for your Royal Enfield Account.OTP is :" +
      smsText +
      "&priority=1&dnd=1&unicode=0"
    )
    .then(function (response) {
      return response;
    })
    .catch(function (error) {
      return error;
    });
}

function sendSms(smsText, phoneNo) {
  var num = phoneNo.slice(-10);
  request(
    `http://103.247.98.91/API/SendMsg.aspx?uname=20171501&pass=Raytheorysms1&send=PRPSCN&dest=${phoneNo}&msg=Hi%20:User,%20Welcome%20to%20Forget%20OTP%20!%20Your%20OTP%20Verification%20Code%20is%20${smsText}`
  )
    .then(response => { })
    .catch(err => {
      console.log(err);
    });
}

function expireOtp(userid) {
  User.findOne({ _id: userid }).then(founduser => {
    user.otp = null;
    user.save();
  });
}

function forgot(req, res, next) {
  const returnObj = {
    success: true,
    message: "",
    data: {}
  };
  var phoneNo = req.body.phoneNo;
  if (phoneNo) {
    User.findOne({ phoneNo: phoneNo }).then(user => {
      if (user) {
        const otpValue = Math.floor(100000 + Math.random() * 900000);
        user.otp = otpValue;
        user.save();
        var sendsms = sendSms(otpValue, phoneNo);
        returnObj.success = true;
        returnObj.message = "otp has been sent";
        res.send(returnObj);
      } else {
        returnObj.success = false;
        returnObj.message = "user Not found";
        res.status(404).send(returnObj);
      }
    });
  } else {
    res.status(406).send("please enter the phone no.");
  }
  // }
}

function sample(req, res, next) {
  if (req.body.name && req.body.email && req.body.query) {
    res.send("form submitted successfully with these details" + req.body);
  } else {
    res.send("error submitting form");
  }
}

/**
 * Update existing user
 * @property {Object} req.body.user - user object containing all fields.
 * @returns {User}
 */
function update(req, res, next) {
  User.findOne({ _id: req.body.userId })
    .then(user => {
      if (user) {
        var oldPassword = req.body.password;
        user.comparePassword(oldPassword, function (err, isMatch) {
          if (err) throw err;
          if (isMatch === true) {
            user.fname = req.body.fname ? req.body.fname : user.fname;
            user.lname = req.body.lname ? req.body.lname : user.lname;
            user.email = req.body.email ? req.body.email : user.email;
            user.dob = req.body.dob ? req.body.dob : user.dob;
            user.gender = req.body.gender ? req.body.gender : user.gender;
            user.phoneNo = req.body.phoneNo ? req.body.phoneNo : user.phoneNo;
            user.bikename = req.body.bikename ? req.body.bikename : user.bikename;
            user.city = req.body.city ? req.body.city : user.city;
            user.aboutMe = req.body.aboutMe ? req.body.aboutMe : user.aboutMe;
            user.ownBike = req.body.moto ? req.body.moto : user.ownBike;
            user
              .saveAsync()
              .then(savedUser => {
                var retObj = {};
                retObj.success = true;
                retObj.message = "Profile Successfully Updated";
                retObj.data = savedUser;
                res.send(retObj);
              })
              .catch(e => console.log(e));
          } else {
            var retObj = {};
            retObj.message = "Password doesnt match";
            retObj.success = false;
            retObj.data = {};
            res.send(retObj);
          }
        });
      } else {
        var retObj = {};
        retObj.message = "User doesnt exist";
        retObj.success = false;
        retObj.data = {};
        res.send(retObj);
      }
    })
    .catch(e => console.log(e));
}

/**
 * Delete user.
 * @returns {User}
 */
function remove(req, res, next) {
  const user = req.user;
  user
    .removeAsync()
    .then(deletedUser => {
      const returnObj = {
        success: true,
        message: "user deleted successfully",
        data: deletedUser
      };
      res.send(returnObj);
    })
    .error(e => next(e));
}

/**
 * Load user and append to req.
 */
function load(req, res, next, id) {
  User.get(id)
    .then(user => {
      req.user = user; // eslint-disable-line no-param-reassign
      return next();
    })
    .error(e => next(e));
}

function hashed(password) {
  return new Promise((resolve, reject) => {
    bcrypt.genSalt(10, (err, salt) => {
      if (err) {
        reject(err);
      }
      bcrypt.hash(password, salt, (hashErr, hash) => {
        if (hashErr) {
          reject(hashErr);
        }
        resolve(hash);
      });
    });
  });
}

function forgotReset(req, res, next) {
  var phoneNo = req.body.phoneNo;
  User.findOne({ phoneNo: req.body.phoneNo }).then(user => {
    if (user) {
      var password = req.body.password;
      user.password = password;
      user.save().then(doc => {
        res.send("password Updated");
      });
    } else {
      res.send("User doesnt exist");
    }
  });
}

function reset(req, res, next) {
  var userId = req.body.userId;
  User.get(userId).then(user => {
    if (user) {
      var password = req.body.password;
      user.password = req.body.password;
      user
        .save()
        .then(doc => {
          res.send("password Updated");
        })
        .catch(e => console.log(e));
    } else {
      res.send("User doesnt exist");
    }
  });
}

function changepassword(req, res, next) {
  var userId = req.body.userId;
  // var jwtAccessToken = req.body.jwtAccessToken;
  User.get(userId)
    .then(user => {
      if (user) {
        var oldPassword = req.body.OldPassword;
        var newPassword = req.body.newPassword;
        user.comparePassword(oldPassword, function (err, isMatch) {
          if (err) throw err;
          if (isMatch === true) {
            user.password = newPassword;
            user.save().then(doc => {
              res.send("password Updated");
            });
          } else {
            res.send("password didn't match");
          }
        });
      } else {
        res.send("User doesnt exist");
      }
    })
    .error(e => next(e));
  next();
}

function timeStampToTime(date) {
  var result = moment(date).fromNow();
  return result;
}

function getUserDetails(req, res, next) {
  var id = req.query.jsonString;
  var tripImageLength = 0;
  var arr = [];
  User.findOne({ _id: id })
    .populate("ridesCreated tripStoriesCreated discussionJoined")
    .then((doc) => {
      var usrRides = [];
      for (var i = 0; i < doc.ridesCreated.length; i++) {
        usrRides.push({
          rideName: doc.ridesCreated[i].rideName,
          rideTypeText: doc.ridesCreated[i].rideCategory,
          rideDetails: doc.ridesCreated[i].rideDetails,
          pageUrl: doc.ridesCreated[i].ridePageUrl,
          startPoint: { name: doc.ridesCreated[i].startPoint.name },
          endPoint: { name: doc.ridesCreated[i].endPoint.name },
          startDate: "2018-07-01",
          durationInDays: doc.ridesCreated[i].durationInDays
        });
      }
      var bikeArr = [];
      for (var i = 0; i < doc.ownedBikeInfo.length; i++) {
        bikeArr.push({
          bikeId: doc.ownedBikeInfo[i].bikeId,
          vehicleNo: doc.ownedBikeInfo[i].vehicleNo
        });
      }

      var tripStoriesSend = [];
      for (var i = 0; i < doc.tripStoriesCreated.length; i++) {
        tripStoriesSend.push({
          title: doc.tripStoriesCreated[i].storyTitle,
          summary: doc.tripStoriesCreated[i].storySummary,
          pageUrl: doc.tripStoriesCreated[i].storyUrl,
          thumbnailImagePath: doc.tripStoriesCreated[i].tripStoryImages.length != 0 ? doc.tripStoriesCreated[i].tripStoryImages[0].srcPath : ""
        });
        tripImageLength += doc.tripStoriesCreated[i].tripStoryImages.length;
      }

      for (var i = 0; i < doc.discussionJoined.length; i++) {
        let time = moment(doc.discussionJoined[i].postedOn).fromNow();
        doc.discussionJoined[i].timeAgo = time;
      }

      var obj = {
        firstName: doc.fname ? doc.fname : "",
        address: { addressAsText: doc.addressInfo.city ? doc.addressInfo.city : "" },
        lastName: doc.lname ? doc.lname : "",
        coverImage: { srcPath: doc.coverImage ? doc.coverImage : "" },
        profilePicture: { srcPath: doc.profilePicture ? doc.profilePicture : "" },
        ownerBikeDetails: doc.bikeName ? doc.bikeName : "",
        aboutMe: doc.aboutMe ? doc.aboutMe : "",
        listofTags: doc.listofTags ? doc.listofTags : [],
        ridesCreated: doc.ridesCreated.length ? doc.ridesCreated.length : 0,
        ridesByUser: usrRides,
        tripStoriesCreated: doc.tripStoriesCreated ? doc.tripStoriesCreated.length : 0,
        imagesUploaded: tripImageLength ? tripImageLength : 0,
        gender: doc.gender ? doc.gender : "",
        address: { city: doc.addressInfo.city ? doc.addressInfo.city : "" },
        dob: moment().format(doc.dob).toString(),
        email: doc.email,
        ownBike: doc.ownBike ? doc.ownBike : "",
        discussionJoined: doc.discussionJoined.length != 0 ? doc.discussionJoined : [],
        phone: doc.phoneNo,
        tripStories: tripStoriesSend,
        motorcyclesOwned: bikeArr
      };

      res.send(obj);
    })
    .catch(e => {
      console.log(e);
      res.send(e);
    });
}

function userInterests(req, res, next) {
  var id = req.body.userId;
  User.findOneAndUpdate({ _id: id }, { $set: { listofTags: [] } }, { new: true })
    .then((docs) => {
      console.log('====================================');
      console.log(doc);
      console.log('====================================');
      var i;
      for (i = 0; i < req.body.interests.length; i++) {
        docs.listofTags.push({ userTags: req.body.interests[i] });
      }
      docs.save().then(user => {
        res.send("Updated User Interests");
      });
    })
    .catch(e => console.log(e));
}

function updateCoverImage(req, res, next) {
  User.findOne({ _id: req.body.userId })
    .then(doc => {
      var image = req.body.image;
      var path = "./node/assets/User/coverImage";
      base64.img(image, path, doc._id + doc.fname, function (err, filepath) {
        const returnObj = {
          success: true,
          message: "",
          data: {}
        };
        if (err) {
          returnObj.success = false;
          returnObj.message = "error updating image";
          returnObj.data = err;
          res.send(returnObj);
        } else {
          User.findByIdAndUpdate(
            { _id: req.body.userId },
            { $set: { coverImage: "/" + filepath } }
          ).then(
            () => {
              returnObj.message = "updated Successfully";
              res.send(returnObj);
            },
            err => {
              returnObj.success = false;
              returnObj.message = "error updating image";
              returnObj.data = err;
              res.send(returnObj);
            }
          );
        }
      });
    })
    .catch(e => console.log(e));
}

function updateProfileImage(req, res, next) {
  User.findOne({ _id: req.body.userId }).then(doc => {
    var image = req.body.image;
    var path = "./node/assets/User/ProfileImage";
    base64.img(image, path, doc._id + doc.fname, function (err, filepath) {
      const returnObj = {
        success: true,
        message: "",
        data: {}
      };
      if (err) {
        returnObj.success = false;
        returnObj.message = "error updating image";
        returnObj.data = err;
        res.send(returnObj);
      } else {
        User.findByIdAndUpdate(
          { _id: req.body.userId },
          { $set: { profilePicture: "/" + filepath } }
        ).then(
          () => {
            returnObj.message = "updated Successfully";
            res.send(returnObj);
          },
          err => {
            returnObj.success = false;
            returnObj.message = "error updating image";
            returnObj.data = err;
            res.send(returnObj);
          }
        );
      }
    });
  });
}

function deleteProfileImage(req, res, next) {
  var dummyProfileImage =
    "/node/assets/User/ProfileImage/profile_image_dummy.jpg";
  const returnObj = {
    success: true,
    message: "",
    data: {}
  };

  User.findByIdAndUpdate(
    { _id: req.body.userId },
    { $set: { profilePicture: dummyProfileImage } }
  ).then(
    () => {
      returnObj.message = "deleted Successfully";
      returnObj.data = { dummyImage: dummyProfileImage };
      res.send(returnObj);
    },
    err => {
      returnObj.success = false;
      returnObj.message = "deletion failed";
      res.status(204).send(returnObj);
    }
  );
}

function deleteCoverImage(req, res, next) {
  var dummyCoverImage = "/node/assets/User/coverImage/cover-image.jpg";
  const returnObj = {
    success: true,
    message: "",
    data: {}
  };

  User.findByIdAndUpdate(
    { _id: req.body.userId },
    { $set: { coverImage: dummyCoverImage } }
  ).then(
    () => {
      returnObj.message = "deleted Successfully";
      returnObj.data = { dummyImage: dummyCoverImage };
      console.log("all fine");
      res.send(returnObj);
    },
    err => {
      returnObj.success = false;
      returnObj.message = " deletion failed";
      res.status(204).send("all good");
    }
  );
}

function suggestedTripStories(req, res, next) {
  var userInterestArr = [];
  var matched = [];
  var userId = req.query.jsonString;
  User.findOne({ _id: userId })
    .then(userFound => {
      for (var i = 0; i < userFound.listofTags.length; i++) {
        userInterestArr.push(userFound.listofTags[i].userTags);
      }
      TripStory.find().then(tripStoryarr => {
        var tripStories = tripStoryarr;
        for (var i = 0; i < userInterestArr.length; i++) {
          for (var j = 0; j < tripStories.length; j++) {
            for (var k = 0; k < tripStories[j].storyTags.length; k++) {
              if (userInterestArr[i] == tripStories[j].storyTags[k]) {
                matched.push(tripStories[j]);
              }
            }
          }
        }

        let unique = [];
        if (matched.length != 0) {
          unique = [...new Set(matched.map(item => item._id))];
        }

        var finalResponse = [];
        for (var i = 0; i < tripStories.length; i++) {
          for (var j = 0; j < unique.length; j++) {
            if (unique[j] == tripStories[i]._id) {
              finalResponse.push({
                thumbnailImagePath:
                  tripStories[i].tripStoryImages.length != 0 ? tripStories[i].tripStoryImages[0].srcPath : "",
                title: tripStories[i].storyTitle,
                summary: tripStories[i].storySummary,
                pageUrl: tripStories[i].storyUrl
              });
            }
          }
        }
        res.send({ tripStories: finalResponse });
      });
    })
    .catch(e => console.log(e));
}

function upcomingRides(req, res, next) {
  Rides.find().then(doc => {
    for (var i = 0; i < doc.length; i++) {
      var year =
        doc[i].startDate[6] +
        doc[i].startDate[7] +
        doc[i].startDate[8] +
        doc[i].startDate[9];
      var date = doc[i].startDate[0] + doc[i].startDate[1];
      var month = doc[i].startDate[3] + doc[i].startDate[4];
      var myDate = new Date(year, month, date);
      if (myDate > Date.now()) {
        console.log(doc[i]);
      }
    }
  });
}

function reviewForUser(req, res, next) {
  var arr = [];
  User.findOne({ _id: req.query.jsonString })
    .populate({
      path: "reviewCreated",
      options: { sort: "-reviewDateText" }
    })

    .then(doc => {
      for (var i = 0; i < doc.reviewCreated.length; i++) {
        arr.push({
          reviewText: doc.reviewCreated[0].reviewText,
          averageRatingPercentage: doc.reviewCreated[i].averageRatingPercentage,
          reviewByUser: {
            firstName: doc.fname,
            profilePageUrl: doc.userUrl,
            profilePicture: { srcPath: doc.profilePicture }
          },
          reviewCriterias: doc.reviewCreated[i].reviewCriterias,
          averageRating: doc.reviewCreated[i].averageRating,
          reviewDescription: doc.reviewCreated[i].reviewDescription,
          reviewDateText: moment(doc.reviewCreated[i].reviewDateText).format("YYYY-MM-DD")
        });
      }
      res.send(arr[0]);
    });
}

export default {
  testingroute,
  reviewForUser,
  suggestedTripStories,
  load,
  create,
  update,
  remove,
  forgot,
  changepassword,
  reset,
  getUserDetails,
  userInterests,
  forgotReset,
  updateProfileImage,
  updateCoverImage,
  deleteCoverImage,
  deleteProfileImage,
  sample,
  upcomingRides,
};
