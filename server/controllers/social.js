import Twit from "twitter";
import util from "util";
import request from "request";
import sortJsonArray from "sort-json-array";
import express from "express";
import axios from "axios";
import CircularJSON from "circular-json-es6";
import Socialfeeds from "../models/socialfeeds";
var asyncc = require('asyncawait/async');
var awaitt = require('asyncawait/await');
import config from '../../config/env';



function twitterdata(req, res, next) {
  var arr = [];
  var hashtag = req.body.hashtag;
  Socialfeeds
    .find()
    .then((docs) => {
      var foo = asyncc(function () {
        for (const item of docs) {
          if (item.ListOfTags != null) {
            for (var i = 0; i < item.ListOfTags.length; i++) {
              if (item.ListOfTags[i].toLowerCase() == hashtag.toLowerCase())
                arr.push(awaitt(savingFunction(item)))
            }
          }
        }

        res.send(arr);
      })
      foo();
    })
    .catch((err) => {
      res.status(500).send({ msg: "error" })
    });
}



var savingFunction = ((item) => {
  return new Promise((resolve, reject) => {
    resolve(item);
  })
})


function dealerReviews(req, res, next) {
  var placeid = req.body.obj.googlePlaceId;
  var googlekey = config.googlereviewkey;
  var verifyUrl = `https://maps.googleapis.com/maps/api/place/details/json?placeid=${placeid}&key=${googlekey}`;
  request(verifyUrl, (err, response, body) => {
    var stringbody = JSON.parse(body);
    var avReview = 0;
    for (var i = 0; i < stringbody.result.reviews.length; i++) {

      avReview = avReview + stringbody.result.reviews[i].rating;
    }
    var finalReview = avReview / stringbody.result.reviews.length;
    var reviewPercentage = finalReview / 5;
    var finalPercentage = reviewPercentage * 100;
    var location = stringbody.result.url;
    var obj = {
      ratingPercentage: finalPercentage,
      rating: finalReview,
      directionUrl: location
    }
    res.send(obj);
  });
}

function fetch(req, res, next) {
  var placeid = 'ChIJ7bGcKGThDDkRHLFd_LKUAC4';
  var googlekey = config.googlereviewkey;
  var placeId = req.body.obj.googlePlaceId;
  var verifyUrl = `https://maps.googleapis.com/maps/api/place/details/json?placeid=${placeid}&key=${googlekey}`;

  request(verifyUrl, (err, response, body) => {

    body = JSON.parse(body);
    var Address = body.result.formatted_address;
    var phonenumber = body.result.formatted_phone_number;
    var name = body.result.name;
    var location = body.result.url;
    var ratings = body.result.rating;
    var website = body.result.website;
    var c = body.result.reviews;
    var d = sortJsonArray(c, 'rating', 'des');
    var reviewername = d[0].author_name;
    var authorurl = d[0].author_url;
    var profilephoto = d[0].profile_photo_url;
    var authorating = d[0].rating;
    var text = d[0].text;
    var timedescription = d[0].relative_time_description;
    var lat = body.result.geometry.location.lat;
    var lng = body.result.geometry.location.lng;


    var obj = {
      reviewText: text,
      reviewRating: authorating,
      reviewByUser:
      {
        firstName: reviewername,
        address: { addressAsText: Address },
        profilePicture: { srcPath: profilephoto }
      },
      reviewDateText: timedescription,
      reviewRatingPercentage: authorating * 20
    }

    res.send(obj);
  });
}

function googleplusdata(req, res, next) {
  const apkey = "AIzaSyA3LipyK7I_HaCEgW4jZl59dTlY0-gJvTA";
  var GUrl = `https://www.googleapis.com/plus/v1/activities?query=Royal+Enfield&maxResults=20&orderBy=recent&key=${apkey}`;
  axios
    .get(GUrl)
    .then((response) => {
      var clone = CircularJSON.parse(CircularJSON.stringify(response));
      res.send(clone.data);
    })
    .catch((e) => { console.log(e); }
    );
}

function getWeather(req,res,next){
	var lat = req.query.lat;
	var lng = req.query.lng;
	axios.get("https://api.openweathermap.org/data/2.5/weather?lat="+lat+"&lon="+lng+"&appid=cbf1b58e3415fa3b236b9e52d76e1bc4").then((data)=>{
	res.send(data.data)
	}).catch((err)=>{
		console.log(err)
	})
}

export default {
  twitterdata,
  fetch,
  googleplusdata,
  dealerReviews,
  getWeather
};

