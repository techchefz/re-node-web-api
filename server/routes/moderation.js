import express from 'express';
import validate from 'express-validation';
import httpStatus from 'http-status';
import passport from 'passport';

import APIError from '../helpers/APIError';
import config from '../../config/env';
import paramValidation from '../../config/param-validation';
import moderationCtrl from '../controllers/moderation';

const router = express.Router();

router.route('/twitterdata')
  /** GET /api/moderation/twitterdata - Get twitterdata */
  .get(moderationCtrl.getTwitterData)

router.route('/moderatetwitterdata')
  /** GET /api/moderation/moderatetwitterdata - Moderate twitterdata */
  .post(moderationCtrl.moderateTwitterData)


export default router;
