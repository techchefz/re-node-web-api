import express from 'express';
import payment from "../controllers/payment";


const router = express.Router();

router.route('/ccavRequestHandler')
    .post(payment.ccavRequestHandler);

router.route('/ccavResponseHandler')
    .post(payment.ccavResponseHandler);

export default router;
