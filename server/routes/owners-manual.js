import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import ownersManual from '../controllers/owners-manual';

const router = express.Router();

router.route('/create')
  .post(ownersManual.create);

  export default router;