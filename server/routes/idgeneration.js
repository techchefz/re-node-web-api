import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import idgenerationcontroller from '../controllers/idgeneration'

const router = express.Router();


router.route('/').post(idgenerationcontroller.generateId);


export default router;