import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import repliesCtrl from '../controllers/replies';

const router = express.Router();
/** POST /api/replies/create */

router.route('/create')
    .post(repliesCtrl.createReplies);

export default router;