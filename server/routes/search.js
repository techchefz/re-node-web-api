import express from 'express';
import solrCtrl from '../controllers/solr';

const router = express.Router();

router.route('/')
    .get(solrCtrl.searchSolr)

    .post(solrCtrl.searchSolrCategory);

router.route('/searchride')
    .post(solrCtrl.searchRide);

router.route('/testSms')
    .post(solrCtrl.sendSms);

router.route('/tripStorySearchCount')
    .post(solrCtrl.tripStorySearchCount);

router.route('/tripStorySearch')
    .post(solrCtrl.tripStorySearch);

router.route('/searchTripOnClick')
    .post(solrCtrl.tripStorySearchOnClick);

router.route('/forumTopicSearchOnClick')
    .post(solrCtrl.forumTopicSearchOnClick);

router.route('/forumTopicSearch')
    .post(solrCtrl.forumTopicSearch);

router.route('/newsSearch')
    .post(solrCtrl.NewsSearch);



export default router;
