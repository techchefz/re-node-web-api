import express from 'express';
import validate from 'express-validation';
import passport from 'passport';
import APIError from '../helpers/APIError';
import config from '../../config/env';
import paramValidation from '../../config/param-validation';
import storectrl from '../controllers/storelocator';

const router = express.Router();

// POST /api/storelocator
router.route('/')
    .post(storectrl.locatestores);

export default router;