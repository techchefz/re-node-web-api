import express from 'express';
import validate from 'express-validation';
import httpStatus from 'http-status';
import passport from 'passport';
import APIError from '../helpers/APIError';
import config from '../../config/env';
import paramValidation from '../../config/param-validation';
import userCtrl from '../controllers/user';
import userVerify from '../controllers/verify';

const router = express.Router();

/** POST /api/users/register - create new user and return corresponding user object and token */
router.route('/register')
  .post(validate(paramValidation.createUser), userCtrl.create);

//api/users/verifyOtp
router.route('/verifyOtp')
  .post(validate(paramValidation.verifyOtp), userVerify.verifyOtp);

//api/users/changePassword
router.route('/changePassword')
  .post(validate(paramValidation.changePassword), userCtrl.changepassword);

//api/users/forgot
router.route('/forgot')
  .post(validate(paramValidation.forgotPassword), userCtrl.forgot);

//api/users/resetPassword
router.route('/resetPassword')
  .post(validate(paramValidation.resetPassword), userCtrl.reset);

//api/users/resetPassword using mobile no
router.route('/reset-password-phone')
  .post(validate(paramValidation.forgotReset), userCtrl.forgotReset);

//api/users/userInterests
router.route('/userInterests')
  .post(validate(paramValidation.userInterest), userCtrl.userInterests);

//api/users/updateprofileimage
router.route('/updateprofileimage')
  .post(validate(paramValidation.updateprofileimage),userCtrl.updateProfileImage);

//api/users/updatecoverimage
router.route('/updatecoverimage')
  .post(validate(paramValidation.updatecoverimage),userCtrl.updateCoverImage);

//api/users/deletecoverimage
router.route('/deletecover')
  .post(validate(paramValidation.deletecoverimage),userCtrl.deleteCoverImage);

//api/users/deleteprofileimage
router.route('/deleteprofileimage')
  .post(validate(paramValidation.deleteprofileimage),userCtrl.deleteProfileImage);

router.route('/sample')
  .post(userCtrl.sample);


router.route('/updateProfile')
  .post(validate(paramValidation.updateProfile),userCtrl.update);

router.route('/suggestedTripStories').get(userCtrl.suggestedTripStories);

router.route('/upcoming-rides').post(userCtrl.upcomingRides);

router.route('/reviewForUser').get(validate(paramValidation.reviewForUser),userCtrl.reviewForUser);

router.route('/testingroute').get(userCtrl.testingroute);

/**
  * Middleware for protected routes. All protected routes need token in the header in the form Authorization: JWT token
  */

//router.use((req, res, next) => {
//  passport.authenticate('jwt', config.passportOptions, (error, userDtls, info) => { //eslint-disable-line
//    if (error) {
//      const err = new APIError('token not matched', httpStatus.INTERNAL_SERVER_ERROR);
//      return next(err);
//    } else if (userDtls) {
//      req.user = userDtls;
//      next();
//    } else {
//      const err = new APIError(`token not matched ${info}`, httpStatus.UNAUTHORIZED);
//      return next(err);
//    }
//  })(req, res, next);
//});

router.route('/')
  /** GET /api/users - Get user */
  .get(validate(paramValidation.getUserDetails),userCtrl.getUserDetails)

  /** PUT /api/users - Update user */
  .put(userCtrl.update)

  /** DELETE /api/users - Delete user */
  .delete(userCtrl.remove);

/** Load user when API with userId route parameter is hit */
router.param('userId', userCtrl.load);

export default router;
