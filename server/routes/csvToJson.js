import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import csvToJsonCtrl from '../controllers/csvToJson';
import csvToJsonDealersCtrl from "../controllers/dealersCsvToJson";
const router = express.Router();

router.route('/connect')
    .get(csvToJsonCtrl.connectSftpforbikescsv);

router.route('/bikecsvToJson')
    .get(csvToJsonCtrl.bikeCsvToJson);

router.route('/dealerxlstojson')
    .get(csvToJsonCtrl.dealerDataToDB);

router.route('/getBranchMasterDealers')
    .get(csvToJsonDealersCtrl.getBranchMasterFromSftp);
router.route('/getSingleInterfaceDealers')
    .get(csvToJsonDealersCtrl.getSingleInterfaceFromSftp);


router.route('/getAllDealers')
    .get(csvToJsonDealersCtrl.mergeDealerData);

router.route('/rideOutCsvToJson')
    .get(csvToJsonCtrl.rideOutCsvToJson);

router.route('/mapDealersToRodeOuts')
    .get(csvToJsonCtrl.mapDealersToRodeOuts);

export default router;
