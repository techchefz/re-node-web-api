import express from 'express';
import authRoutes from './auth';
import userRoutes from './user';
import bikeRoutes from './bike';
import tripStoryRoutes from './tripStory';
import rideRoutes from './ride';
import verifyRoutes from './verify';
import uploadAssetRoutes from './uploadAsset';
import socialRoutes from './social';
import subscribeRoutes from './subscribe';
import storelocator from './storelocator';
import recaptchaRoutes from './recaptcha';
import dealerRoutes from './dealers';
import comments from './comments';
import servicebooking from './servicebooking';
import replies from './replies';
import reviews from './reviews';
import searchSolar from './search';
import newsRoutes from './news';
import bookTestRide from './book-test-ride';
import forums from "./forums";
import cityStatesRoutes from './cityStates';
import ownersManual from './owners-manual';
import idgeneration from './idgeneration';
import dmsRoutes from "./dms";
import syncDataRoutes from "./syncSampleData";
import moderation from './moderation';
import csvToJsonRoutes from './csvToJson';
import locobuzzfeeds from './locobuzzfeeds';
import payment from './payment';

//import csvToJson from './csvToJson';


const router = express.Router();

/** GET /health-check - Check service health */
router.get('/health-check', (req, res) =>
  res.send('OK'));

router.get('/', (req, res) =>
  res.send('OK'));

router.use('/csvtojson', csvToJsonRoutes);
// mount user routes at /verify
router.use('/verify', verifyRoutes);

// mount user routes at /users
router.use('/users', userRoutes);

// mount user routes at /users
router.use('/dealers', dealerRoutes);

// mount auth routes at /auth
router.use('/auth', authRoutes);

// mount auth routes at /bikes
router.use('/bikes', bikeRoutes);

// mount stories routes at /stories
router.use('/stories', tripStoryRoutes);

//mount comments routes at /comments
router.use('/comments', comments);
// mount stories routes at /rides
router.use('/rides', rideRoutes);

//mount UploadAsset routes at /uploadAsset
router.use('/uploadAsset', uploadAssetRoutes);

//mount Social routes at /social
router.use('/social', socialRoutes);

//mount recaptcha routes at /recaptcha
router.use('/recaptcha', recaptchaRoutes)

//mount subscription routes at /subscribe
router.use('/subscribe', subscribeRoutes);

//mount store routes at /storelocator
router.use('/storelocator', storelocator);

//mount servicebooking routes at /servicebooking
router.use('/servicebooking', servicebooking);

//mount replies routes at /replies
router.use('/replies', replies);

// mount reviews routes at /reviews
router.use('/reviews', reviews);

//mount search routes at /search
router.use('/search', searchSolar);

//mount news routes at /news
router.use('/news', newsRoutes);

//mount forum routes at /fourms 
router.use('/forums', forums);

//mount forum routes at /fourms 
router.use('/booktestride', bookTestRide);

//mount cityStates at /cityStates
router.use('/cityStates', cityStatesRoutes);

//mount owners-manual at /ownersmanual
router.use('/ownersmanual', ownersManual);

// mount all id genertaion id routes on /pagecreation
router.use('/idgeneration', idgeneration);

router.use('/moderation', moderation);

router.use('/dms', dmsRoutes);

router.use('/sync-data', syncDataRoutes);


router.use('/locobuzzfeeds', locobuzzfeeds);

router.use('/payment', payment);

// router.use('/csvtojson',csvToJson);

export default router;

