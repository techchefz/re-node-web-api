import express from 'express';
import validate from 'express-validation';
import passport from 'passport';
import APIError from '../helpers/APIError';
import config from '../../config/env';
import paramValidation from '../../config/param-validation';
import subscription from '../controllers/subscribe';

const router = express.Router();

// POST /api/susbcribe 
router.route('/')
  .post(subscription.subscribed)

export default router;


