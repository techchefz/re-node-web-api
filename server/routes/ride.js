import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import rideCtrl from '../controllers/ride';
import multer from 'multer';

const router = express.Router();



const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './node/assets/Rides');
  },
  filename: function (req, file, cb) {
    var imageName = Date.now() + file.originalname;
    cb(null, imageName);
    var imagePath = './node/assets/Rides' + imageName;
  }
});

const uploadObj = multer({ storage: storage })
/** POST /api/rides/create - create new Ride and return corresponding ride object */
router.route('/create')
  .post(uploadObj.array('rideImages', 10),validate(paramValidation.createRide), rideCtrl.create);

router.route('/all')
  .post(validate(paramValidation.getRides), rideCtrl.getRides);

router.route('/getridesaroundme')
  .post(validate(paramValidation.getridesaroundme),rideCtrl.getRidesAroundMe);

router.route('/')
  /** POST /api/rides/ - Get ride */
  .post(validate(paramValidation.getRide), rideCtrl.getRide)

  /** PUT /api/rides/ - Update ride */
  .put(rideCtrl.update)

  /** DELETE /api/rides/ - Delete ride */
  .delete(rideCtrl.remove)

  .get(rideCtrl.getRide);

export default router;
