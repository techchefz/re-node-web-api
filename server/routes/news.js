import express from 'express';
import newsCtrl from '../controllers/news';

const router = express.Router();

router.route('/create')
  .post(newsCtrl.create);

router.route('/getNews')
  /** POST /api/stories/ - Get story */
  .post(newsCtrl.getNews)

router.route('/')
  .put(newsCtrl.update);

export default router;
