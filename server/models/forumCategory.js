import mongoose from 'mongoose';

/**
 * Forums Schema
 */

const Schema = mongoose.Schema;

const ForumCategorySchema = new mongoose.Schema({
    categoryName: { type: String, Default: null, unique: true },
    categorySubHeading: { type: String, Default: null },
    totalTopicsCount: { type: Number, Default: 0 },
    totalViewsCount: { type: Number, Default: 0 },
    totalRepliesCount: { type: Number, Default: 0 },
    subscribedUsers: [{
        userId: { type: Schema.Types.ObjectId, ref: 'User', default: null },
        email: { type: String, Default: null }
    }]
});

export default mongoose.model('ForumCategory', ForumCategorySchema);
