import mongoose from 'mongoose';

var dealersingleinterfaceSchema = new mongoose.Schema({
    BranchCode: { type: String, default: Number },
    StoreManagerDesignation: { type: String, default: Number },
    Country: { type: String, default: Number },
    StoreManagerPhoneNo: { type: String, default: Number },
    StoreManagerEmailID: { type: String, default: Number },
    StoreEmailID: { type: String, default: Number },
    WeeklyOff: { type: String, default: Number },
    BusinessHours: { type: String, default: Number },
    Latitude: { type: String, default: Number },
    Longitude: { type: String, default: Number },
    StoreManagerName: { type: String, default: Number },
    DealerMobileNo: { type: String, default: Number },
    AlternateStoreNo: { type: String, default: Number },
    MainPhoneNo: { type: String, default: Number },
    GooglePlaceID: { type: String, default: Number },
    Status: { type: String, default: Number },
});


export default mongoose.model('singleintefacedealer', dealersingleinterfaceSchema);