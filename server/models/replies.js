import mongoose from 'mongoose';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';

import User from './user';

/*** 
comments Schema 
***/

const moment = require('moment');
const Schema = mongoose.Schema;
const ReplySchema = new mongoose.Schema({
    replyBody: { type: String, default: null },
    date: { type: Date, default: Date.now },
    locale: {
        country: { type: String, required: true },
        language: { type: String, required: true }
    },
    userdetailsreplies: { type: mongoose.Schema.Types.ObjectId, ref: 'User', default: null }
});


export default mongoose.model('replies', ReplySchema);
