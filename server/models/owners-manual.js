import mongoose from 'mongoose';

var ownersManualSchema = new mongoose.Schema({

    name: { type: String, default: null },
    email: { type: String, default: null },
    phone: { type: String, default: null },
    city: { type: String, default: null },
    locale: {
        country: { type: String, required: true },
        language: { type: String, required: true }
    },

});

export default mongoose.model('OwnersManual', ownersManualSchema);