import mongoose from 'mongoose';

/**
 * Bike Schema
 */

const Schema = mongoose.Schema;
const BikeSchema = new mongoose.Schema({
    bikeName: { type: String, default: null },
    familyCode: { type: Number, default: null },
    model: [],
    familyName: { type: String, default: null },
    status: { type: String, default: null },
    ReviewId: [{ type: Schema.Types.ObjectId, ref: 'Reviews', default: null }]
});


export default mongoose.model('Bike', BikeSchema);
