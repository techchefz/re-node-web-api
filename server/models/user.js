//import bcrypt from 'bcrypt';
import Promise from 'bluebird';
import mongoose from 'mongoose';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
var crypto = require("crypto");
var md5 = require('js-md5');


const securityKey = 'manju22feet';

md5 = text => {
  return crypto
    .createHash('md5')
    .update(text)
    .digest();
}


/**
 * User Schema
 */
mongoose.Promise = global.Promise;
const Schema = mongoose.Schema;
const UserSchema = new mongoose.Schema({
  locale: {
    country: { type: String, required: true },
    language: { type: String, required: true }
  },
  previousUserId: { type: Number, default: null },
  fname: { type: String, default: null },
  lname: { type: String, default: null },
  gender: { type: String, default: null },
  email: { type: String, required: true, unique: true },
  phoneNo: { type: String, default: null, unique: true },
  password: { type: String, required: true, select: true },
  bikename: { type: String, default: null },
  city: { type: String, default: null },
  aboutMe: { type: String, default: null },
  profilePicture: {
    type: String,
    default:
      '/node/assets/User/ProfileImage/profile_image_dummy.jpg' //Default profile image to be added.
  },
  coverImage: {
    type: String,
    default:
      '/node/assets/User/coverImage/cover-image.jpg' //Default profile image to be added.
  },
  dob: { type: String, default: '12/8/1993' },
  addressInfo: {
    address: { type: String, default: null },
    city: { type: String, default: null },
    state: { type: String, default: null },
    country: { type: String, default: null },
    pinCode: { type: String, default: null }
  },
  ownBike: { type: String, default: null },
  reviewCreated: [
    { type: Schema.Types.ObjectId, ref: 'Reviews' }
  ],
  mobileVerified: { type: Boolean, default: false },
  emailVerified: { type: Boolean, default: false },
  otp: { type: Number, default: null },
  emailToken: { type: Number, default: null },
  isRoyalEnfieldOwner: { type: String, default: false },
  bikeOwned: [{ type: Schema.Types.ObjectId, ref: 'Bike', default: null }],
  ownedBikeInfo: [{
    bikeId: { type: Schema.Types.ObjectId, ref: 'Bike', default: null },
    dmsFamilyId: { type: String, default: null },
    type: { type: String, default: null },

    regNo: { type: String, default: null },
    RC_ownerName: { type: String, default: null },
    vehicleNo: { type: String, default: null },
    regDate: { type: Date, default: '1/1/2018' }
  }],

  ridesCreated: [
    { type: Schema.Types.ObjectId, ref: 'Ride', default: null }
  ],

  tripStoriesCreated: [
    { type: Schema.Types.ObjectId, ref: 'TripStory', default: null }
  ],

  ridesJoined: [
    { type: Schema.Types.ObjectId, ref: 'Ride', default: null }
  ],
  discussionJoined: [
    { type: Schema.Types.ObjectId, ref: 'Forum', default: null }
  ],
  favouriteQuote: {
    type: String, default: null
  },
  favouriteRideExperience: {
    type: String, default: null
  },
  socialNetworkUrls: {
    facebook: { type: String, default: null },
    googlePlus: { type: String, default: null },
    instagram: { type: String, default: null },
    twitter: { type: String, default: null },
  },
  userType: { type: String, default: 'user' },
  loginStatus: { type: Boolean, default: false },
  jwtAccessToken: { type: String, default: null },
  listofTags: [{ userTags: { type: String, default: null } }],
  userInterest: { type: String, default: null },
  userUrl: { type: String, default: null }
});


/**
 * converts the string value of the password to some hashed value
 * - pre-save hooks
 * - validations
 * - virtuals
 */
// eslint-disable-next-line
UserSchema.pre("save", function userSchemaPre(next) {
  const user = this;



  if (this.isModified('password') || this.isNew) {

    // eslint-disable-next-line

    const encrypt = (text, secretKey) => {
      secretKey = md5(secretKey);
      secretKey = Buffer.concat([secretKey, secretKey.slice(0, 8)]);
      const cipher = crypto.createCipheriv('des-ede3', secretKey, '');
      const encrypted = cipher.update(text, 'utf8', 'base64');
      return encrypted + cipher.final('base64');
    };
    const encrypted = encrypt(user.password, securityKey);
    user.password = encrypted;
    next();
  } else {
    return next();
  }
});


/**
 * comapare the stored hashed value of the password with the given value of the password
 * @param pw - password whose value has to be compare
 * @param cb - callback function
 */
UserSchema.methods.comparePassword = function comparePassword(pw, cb) {
  const user = this;
  // eslint-disable-next-line
  // pw is the incoming password 
  // user.password is the old password
  var isMatch;
  const encrypt = (text, secretKey) => {
    secretKey = md5(secretKey);
    secretKey = Buffer.concat([secretKey, secretKey.slice(0, 8)]);
    const cipher = crypto.createCipheriv('des-ede3', secretKey, '');
    const encrypted = cipher.update(text, 'utf8', 'base64');
    return encrypted + cipher.final('base64');
  };
  const encrypted = encrypt(pw, securityKey);

  if (encrypted === user.password) {
    isMatch = true
  }
  else {
    isMatch = false
  }

  cb(null, isMatch);

};




/**
 * Statics
 */
UserSchema.statics = {
  /**
     * Get user
     * @param {ObjectId} id - The objectId of user.
     * @returns {Promise<User, APIError>}
     */
  get(id) {
    return this.findById(id)
      .execAsync()
      .then((user) => {
        if (user) {
          return user;
        }
        const err = new APIError('No such user exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },
  /**
     * List users in descending order of 'createdAt' timestamp.
     * @param {number} skip - Number of users to be skipped.
     * @param {number} limit - Limit number of users to be returned.
     * @returns {Promise<User[]>}
     */
  list({ skip = 0, limit = 20 } = {}) {
    return this.find({ $or: [{ userType: 'user' }, { userType: 'admin' }] })
      .sort({ _id: -1 })
      .select('-__v')
      .skip(skip)
      .limit(limit)
      .execAsync();
  }
};
/**
 * @typedef User
 */
export default mongoose.model('User', UserSchema);
