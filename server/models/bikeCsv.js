import mongoose from 'mongoose';

/**
 * Bike Schema
 */

const Schema = mongoose.Schema;
const BikeSchema = new Schema({
    familyName: { type: String, default: null },
    familyCode: { type: String, default: null },
    bikeModelCode: { type: String, default: null },
    bikeName: { type: String, default: null },
    bikeStatus: { type: String, default: null },
});


export default mongoose.model('BikeCSV', BikeSchema);
