import mongoose from 'mongoose';
import validator from "validator";

var UserSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
    trim: true,
    minlength: 1,
  },
  locale: {
    country: { type: String, required: true },
    language: { type: String, required: true }
  },
});

export default mongoose.model('Subscribe', UserSchema);

