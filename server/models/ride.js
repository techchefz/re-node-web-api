import mongoose from 'mongoose';

/**
 * Ride Schema
 */

const GeoSchema = new mongoose.Schema({
    type: {
        type: String,
        default: "Point"
    },
    coordinates: {
        type: [Number],
        index: '2dsphere'
    }

});

const Schema = mongoose.Schema;
const rideSchema = new mongoose.Schema({


    rideStartDateIso: { type: Date },
    rideEndDateIso: { type: Date },

    BranchCode: { type: String, default: null },
    CountryCode: { type: String, default: null },
    CompanyName: { type: String, default: null },

    prevUserId: { type: Number, default: null },
    RideId: { type: Number, default: null },
    ridePageUrl: { type: String, default: null },
    createdOn: { type: Date, default: null },
    rideName: { type: String, default: null },
    startPoint: {
        latitude: { type: Number, default: 0 },
        longitude: { type: Number, default: 0 },
        name: { type: String, default: null }
    },
    endPoint: {
        latitude: { type: Number, default: 0 },
        longitude: { type: Number, default: 0 },
        name: { type: String, default: null }
    },
    waypoints: [{
        latitude: { type: Number, default: 0 },
        longitude: { type: Number, default: 0 },
        name: { type: String, default: null }
    }],
    durationInHours: { type: Number, default: null },
    calculatedDistance: { type: Number, default: 0 },
    durationInDays: { type: Number, default: null },
    rideDetails: { type: String, default: null },
    startDate: { type: String, default: null },
    endDate: { type: String, default: null },
    terrain: { type: String, default: null },
    createdByUser: { type: Schema.Types.ObjectId, ref: 'user' },
    ridersJoined: [{ type: Schema.Types.ObjectId, default: null }],
    startTime: { type: String, default: null },
    totalDistance: { type: Number, default: null },
    rideImages: [{
        srcPath: { type: String, default: null }
    }],
    rideCategory: { type: String, default: null },
    personalInfo: {
        fName: { type: String, default: null },
        lName: { type: String, default: null },
        gender: { type: String, default: null },
        email: { type: String, default: null },
        isRoyalEnfieldOwner: { type: Boolean, default: null },
        dob: { type: String, default: null },
        phoneNo: { type: Number, default: null },
        city: { type: String, default: null },
        bikeName: { type: String, default: null }
    },

    geo: {
        type: { type: String },
        coordinates: [Number]
    },
    DealerId: { type: Schema.Types.ObjectId, ref: 'dealer', default: null }
    //geometry : GeoSchema
})

//rideSchema.index({ geo: '2dsphere' });



rideSchema.statics = {
    getStories(skip, limit) {
        return this.find()
            .skip(skip)
            .limit(limit)
            .execAsync()
            .then((stories) => {
                if (stories) {
                    return stories;
                }
                const err = new APIError('Error Retrieving stories!', httpStatus.NOT_FOUND);
                return Promise.reject(err);
            });
    }
}

export default mongoose.model('Ride', rideSchema);
