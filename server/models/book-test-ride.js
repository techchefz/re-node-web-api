import mongoose from 'mongoose';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';

/*** 
comments Schema 
***/

const Schema = mongoose.Schema;
const bookTestRideSchema = new mongoose.Schema({
    fName: { type: String, default: null },
    lName: { type: String, default: null },
    email: { type: String, default: null },
    bikeName: { type: String, default: null },
    locale: {
        country: { type: String, required: true },
        language: { type: String, required: true }
    },
    countryName: { type: String, default: null },
    stateName: { type: String, default: null },
    cityName: { type: String, default: null },
    dealerName: { type: String, default: null },
    Date: { type: String, default: null },
    mobile: { type: Number, default: null },
    dealerCode: { type: String, default: null },
    buyPlanDate: { type: String, default: null },
    time: { type: String, default: null }
});

export default mongoose.model('bookTestRide', bookTestRideSchema);