import mongoose from 'mongoose';
var rideOutSchema = new mongoose.Schema({
    Name: { type: String, default: null, trim: true } ,
    StartPoint: { type: String, default: null, trim: true } ,
    Destination: { type: String, default: null, trim: true } ,
    TotalDistance: { type: String, default: null, trim: true } ,
    StartDate: { type: String, default: null, trim: true },
    EndDate: { type: String, default: null, trim: true } ,
    RideID: { type: String, default: null, trim: true } ,
    Remarks: { type: String, default: null, trim: true } ,
    Source: { type: String, default: null, trim: true } ,
    DocName: { type: String, default: null, trim: true } ,
    DocDate: { type: String, default: null, trim: true } ,
    BranchCode: { type: String, default: null, trim: true } ,
    CompanyCode: { type: String, default: null, trim: true } ,
    CompanyName: { type: String, default: null, trim: true },
    TotalParticipantsAllowed: { type: String, default: null, trim: true },
    RideSourceLatitude: { type: String, default: null, trim: true } ,
    RideSourceLongitude: { type: String, default: null, trim: true } ,
    RideDestinationLatitude: { type: String, default: null, trim: true } ,
    RideDestinationLongitude: { type: String, default: null, trim: true } ,
    TerrainType: { type: String, default: null, trim: true } ,
    Difficultylevel: { type: String, default: null, trim: true } ,
    PillionFriendly: { type: String, default: null, trim: true }
});


export default mongoose.model('rideOut', rideOutSchema);
