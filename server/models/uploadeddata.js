import mongoose from 'mongoose';

var uploadeddata = new mongoose.Schema({
    url: { type: String },
    title: { type: String },
    description: { type: String },
    date: { type: String },
    time: { type: Number },
    kilometers: { type: Number },
    
});

export default mongoose.model('uploadeddata', uploadeddata);