import mongoose from 'mongoose';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';


/*** 
comments Schema 
***/
const moment = require('moment');
const Schema = mongoose.Schema;


const ReviewsSchema = new mongoose.Schema({


	reviewEntityId: { type: String, default: null },
	locale: {
		country: { type: String, required: true },
		language: { type: String, required: true }
	},
	reviewByUser: { type: mongoose.Schema.Types.ObjectId, ref: 'User', default: null },
	reviewDescription: { type: String, default: null },
	reviewText: { type: String, default: null },
	reviewDateText: { type: Date, default: Date.now },
	averageRating: { type: Number, default: 0 },
	averageRatingPercentage: { type: Number, default: 0 },
	reviewCriterias: {

		performance:
		{

			reviewCriteria: { type: String, default: null },
			reviewCriteriaLabel: { type: String, default: null },
			reviewRating: { type: Number, default: 0 },
			reviewRatingPercentage: { type: Number, default: 0 }
		},

		handling:
		{

			reviewCriteria: { type: String, default: null },
			reviewCriteriaLabel: { type: String, default: null },
			reviewRating: { type: Number, default: 0 },
			reviewRatingPercentage: { type: Number, default: 0 }
		},

		style:
		{

			reviewCriteria: { type: String, default: null },
			reviewCriteriaLabel: { type: String, default: null },
			reviewRating: { type: Number, default: 0 },
			reviewRatingPercentage: { type: Number, default: 0 }
		},

		versatality:
		{
			reviewCriteria: { type: String, default: null },
			reviewCriteriaLabel: { type: String, default: null },
			reviewRating: { type: Number, default: 0 },
			reviewRatingPercentage: { type: Number, default: 0 }
		}
		,
		ownershipExperience:
		{
			reviewCriteria: { type: String, default: null },
			reviewCriteriaLabel: { type: String, default: null },
			reviewRating: { type: Number, default: 0 },
			reviewRatingPercentage: { type: Number, default: 0 }
		},

	}





});




export default mongoose.model('Reviews', ReviewsSchema);