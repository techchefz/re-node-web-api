import Joi from 'joi';

export default {
  // ================================================= USER =============================================
  //POST /node/api/users/verifyOtp
  verifyOtp: {
    body: {
      phoneNo: Joi.string().required(),
      otp: Joi.number().required()
    }
  },

  //POST /node/api/users/changePassword
  changePassword: {
    body: {
      OldPassword: Joi.string().required(),
      newPassword: Joi.string().required()
    }
  },

  //POST /node/api/users/forgot
  forgotPassword: {
    body: {
      phoneNo: Joi.string().required()
    }
  },

  //POST /node/api/users/reset-password-phone
  forgotReset: {
    body: {
      phoneNo: Joi.string().required(),
      password: Joi.string().required()
    }
  },

  //POST /node/api/users/userInterests
  userInterest: {
    body: {
      userId: Joi.string().required(),
      interests: Joi.array().required()
    }
  },

  //POST /node/api/users/updateprofileimage
  updateprofileimage: {
    body: {
      userId: Joi.string().required(),
      image: Joi.string().required()
    }
  },

  //POST /node/api/users/updatecoverimage
  updatecoverimage: {
    body: {
      userId: Joi.string().required(),
      image: Joi.string().required()
    }
  },

  //POST /node/api/users/resetPassword
  resetPassword: {
    body: {
      password: Joi.string().required()
    }
  },

  //POST /node/api/users/deletecoverimage
  deletecoverimage: {
    body: {
      userId: Joi.string().required()
    }
  },

  //POST /node/api/users/deleteprofileimage
  deleteprofileimage: {
    body: {
      userId: Joi.string().required()
    }
  },

  //GET /node/api/users/suggestedTripStories
  suggestedTripStories: {
    query: {
      jsonString: Joi.string().required()
    }
  },

  //GET /node/api/users/reviewForUser
  reviewForUser: {
    query: {
      jsonString: Joi.string().required()
    }
  },

  //GET /node/api/users/
  getUserDetails: {
    query: {
      jsonString: Joi.string().required()
    }
  },


  //POST /node/api/users/updateProfile
  updateProfile: {
    body: {
      userId: Joi.string().required()
    }
  },

  // POST /node/api/users/register
  createUser: {
    body: {
      email: Joi.string().required(),
      password: Joi.string().required(),
      phoneNo: Joi.string().required(),
      fname: Joi.string().required(),
      lname: Joi.string().required(),
      gender: Joi.string().required(),
      city: Joi.string().required(),
      dob: Joi.string().required(),
      moto: Joi.string().required(),
      
    }
  },
  // ================================================= USER =============================================

  //POST /node/api/booktestride/create
  createTestRide: {
    body: {
      fName: Joi.string().required,
      lName: Joi.string().required,
      email: Joi.string().required,
      bikeName: Joi.string().required,
      countryName: Joi.string().required,
      stateName: Joi.string().required,
      cityName: Joi.string().required,
      dealerName: Joi.string().required,
      Date: Joi.string().required,
      mobile: Joi.number().required,
      Date: Joi.string().required,
      dealerCode: Joi.string().required,
      buyPlanDate: Joi.string().required,
      time: Joi.string().required
    }
  },
  // POST /api/auth/login
  login: {
    body: {
      email: Joi.string().required(),
      password: Joi.string().required(),
      userType: Joi.string().required()
    }
  },

  // POST /api/bikes/register
  createBike: {
    body: {
      bikeModel: Joi.string().required(),
      bikeName: Joi.string().required(),
      bikeCategory: Joi.string().required()
    }
  },

  // POST /api/stories/create
  createTripStory: {
    body: {
    }
  },

  // POST /api/stories/all
  getStories: {
    body: {
      limit: Joi.string().required()
    }
  },

  // POST /api/stories/
  getStory: {
    body: {
    }
  },

  // POST /api/rides/create
  createRide: {
    body: {
      rideName: Joi.string().required(),
      startLatitude: Joi.number().required(),
      startLongitude: Joi.number().required(),
      destination: Joi.string().required(),
      destinationlatitude: Joi.number().required(),
      destinationlongitude: Joi.number().required(),
    //  durationInDays: Joi.number().required(),
      startDate: Joi.string().required(),
      endDate: Joi.string().required(),
      terrain: Joi.string().required(),
      userId: Joi.string().required(),
      startTimeHours: Joi.string().required(),
      startTimeMins: Joi.string().required(),
      startTimeZone: Joi.string().required(),
      totalDistance: Joi.number().required(),
      fname: Joi.string().required(),
      lname: Joi.string().required(),
      gender: Joi.string().required(),
      email: Joi.string().required(),
     // isRoyalEnfieldOwner: Joi.string().required(),
     // dob: Joi.string().required(),
     // city: Joi.string().required(),
     // bikeOwned: Joi.string().required(),
      rideCategory: Joi.string().required()
    }
  },

  // POST /api/rides/getridesaroundme
  getridesaroundme:{
    body:{
      latitude: Joi.number().required(),
      longitude: Joi.number().required(),
    }
  },


  // POST /api/rides/all
  getRides: {
    body: {
      limit: Joi.string().required()
    }
  },

  // POST /api/rides/
  getRide: {
    body: {
      rideId: Joi.string().required()
    }
  },

  sendOtp: {
    body: {
      phoneNo: Joi.string().required()
    }
  },


  getBikeDetail: {
    body: {
      phoneNo: Joi.string().required()
    }
  },

  findState: {
    body: {
      state: Joi.string().required()
    }
  },

  findCity: {
    body: {
      city: Joi.string().required()
    }
  },
};
